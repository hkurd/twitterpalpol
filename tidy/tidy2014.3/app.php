<?php

/**
 * App, Tidy PHP 
 * load application contnet and define the current controller
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class App {

    /**
     * current controller
     * @access Public
     * @static 
     * @var object controller
     * @see AppController
     */
    public static $controller;

    /**
     * imports to the system
     * @access public
     * @static 	 
     * @param string name of import
     * @param string type of import
     * @return void
     */
    public static function import($name, $type) {
        if (is_object(self::$controller)) {
            $currentController = self::$controller;
            $currentModel = $currentController->model;
        }
        switch ($type) {

            case 'controller' : {
                    (file_exists(CONTROLLER_PATH . $name . CONTROLLER_EXT)) ? include_once (CONTROLLER_PATH . $name . CONTROLLER_EXT) : self::throwExp('controller', CONTROLLER_PATH . $name . CONTROLLER_EXT);
                }
                break;

            case 'view' : {

                    $variables = $GLOBALS ['registry']->template->getViewVariables();
                    $renderedAjax = $GLOBALS ['registry']->request->getRequest('renderedAjax');
                    if (isset($variables))
                        foreach ($variables as $key => $value) {
                            $$key = $value;
                        }
                    if (!$currentController->data ['partAjax'])
                        if (function_exists('jsBeforeView'))
                            jsBeforeView();
                    (file_exists(VIEW_PATH . $name . VIEW_EXT)) ? include_once (VIEW_PATH . $name . VIEW_EXT) : self::throwExp('view ', VIEW_PATH . $name . VIEW_EXT);
                }
                break;

            case 'model' : {

                    (file_exists(MODEL_PATH . $name . MODEL_EXT)) ? include_once (MODEL_PATH . $name . MODEL_EXT) : self::throwExp('model ', MODEL_PATH . $name . MODEL_EXT);
                }
                break;

            case 'layout' : {
                    (file_exists(VIEW_PATH . 'layout' . DS . $name . LAYOUT_EXT)) ? include_once (VIEW_PATH . 'layout' . DS . $name . LAYOUT_EXT) : self::throwExp('layout ', VIEW_PATH . $name . LAYOUT_EXT);
                }
                break;

            case 'imports' : {

                    (file_exists(IMPORT_PATH . $name . MODEL_EXT)) ? include_once (IMPORT_PATH . $name . MODEL_EXT) : self::throwExp('imports ', IMPORT_PATH . $name . MODEL_EXT);
                }
                break;

            case 'element' : {
                    $variables = $GLOBALS ['registry']->template->getViewVariables();
                    $renderedAjax = $GLOBALS ['registry']->request->getRequest('renderedAjax');
                    if (isset($variables))
                        foreach ($variables as $key => $value) {
                            $$key = $value;
                        }

                    (file_exists(ELEMENTS_PATH . $name . VIEW_EXT)) ? include (ELEMENTS_PATH . $name . VIEW_EXT) : self::throwExp('element ', ELEMENTS_PATH . $name . VIEW_EXT);
                }
                break;
        }
    }

    /**
     * through exeption if not founded
     * @access privates
     * @static 	 
     * @param string $name of import
     * @param string $path of import
     * @return boolean
     */
    private static function throwExp($name, $path) {
        throw new Exception($name . ' not found in ' . $path);
        return false;
    }

    /**
     * clear all cash and validation files
     * @access public
     * @static 	 
     * @param string $name of import
     * @param string $path of import
     * @return boolean
     */
    public static function clear() {
        try {
            deleteAllDirFiles(CASH_PATH);
            deleteAllDirFiles(JS_PATH . 'validation' . DS);
            deleteAllDirFiles(JS_PATH . 'cash' . DS);
            deleteAllDirFiles(CSS_PATH . 'cash' . DS);
            $GLOBALS ['registry']->response->destroyCookie();
            $ourFileName = LOG_PATH . DS . 'langLog.log';
            $ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
            fclose($ourFileHandle);
        } catch (TidyException $e) {
            _tr($e->getMessage(), TIDY_CONSTANTS::LOG_CRIT);
        }
        //return false;
    }

    /*
     * connect to database
     * @param array $databaseConfig for connection
     * @param int index (for multi instances)
     * @see AppDB
     * @return object AppDB
     */

    public static function dbconnect($databaseConfig, $instanceIndex = 0) {
        $db = DB::createBuilder($databaseConfig, $instanceIndex);
        $db->setDebug(TIDY_CONSTANTS::DEBUG_NO_MODE);
        if ($databaseConfig['driver'] == 'mysql') {
            $db->query("SET CHARACTER SET 'utf8'");
            $db->query("SET NAMES 'utf8'");
        }
        //	$db->setNumberOfRowsPerPage ( NAVIGATOR_PAGE_NUM );
        //	$db->query ( "SET CHARACTER SET 'utf8'" );
        //	$db->query ( "SET NAMES 'utf8'" );
        return $db;
    }

    /*
     * define the current language of the request
     * @global object $registry
     * @return void
     */

    public static function languageDefine() {
        $langGetShort = strtolower($GLOBALS ['registry']->request->getGet('lang'));
        $viewGetLangShort = strtolower($GLOBALS ['registry']->request->getGet('viewLang'));

        $langShort = defineLanguageVariableCookie($langGetShort, 'lang');
        $viewLangShort = defineLanguageVariableCookie($viewGetLangShort, 'viewLang');



        $mainLangKey = array_keys($GLOBALS ['sysLang'], $langShort);
        $mainLangKey = $mainLangKey [0];

        $mainViewLangKey = array_keys($GLOBALS ['sysLang'], $viewLangShort);
        $mainViewLangKey = $mainViewLangKey [0];

        $GLOBALS ['registry']->currentLangKey = $mainLangKey;
        $GLOBALS ['registry']->currentLang = $langShort;

        $GLOBALS ['registry']->currentViewLangKey = $mainViewLangKey;
        $GLOBALS ['registry']->currentViewLang = $viewLangShort;
    }

    /*
     * set defaul language with lang key
     * @param int lang key
     * @global object registry
     * @global array $GLOBALS ['sysLang']
     * @global object translation
     * @global 
     * @return void 
     */

    public static function setDefaultLanguage($langID) {

        if ($langID != null && array_key_exists($langID, $GLOBALS ['sysLang']) &&
                $GLOBALS ['registry']->currentViewLangKey != $langID) {
            $GLOBALS ['registry']->currentViewLangKey = $GLOBALS ['registry']->currentLangKey = $langID;
            $langShort = $GLOBALS ['sysLang'][$langID];
            $GLOBALS ['registry']->currentViewLang = $GLOBALS ['registry']->currentLang = $langShort;
            $GLOBALS ['registry']->tr->addTranslation(
                    array(
                        'content' => LANG_PATH . $langShort . '.mo',
                        'locale' => $langShort
                    )
            );

            $GLOBALS ['registry']->tr->setLocale($langShort);
            @$GLOBALS ['registry']->response->setCookie('lang', $langShort, TTL_REMEMBER);
            @$GLOBALS ['registry']->response->setCookie('viewLang', $langShort, TTL_REMEMBER);
        }
    }

    /*
     * get the cash object
     * @param integer $lifeTime in secodns	 
     * @param string $path	
     * @return void
     */

    public static function getCash($lifeTime, $path) {
        $cash = new Cash($lifeTime, $path);
        return $cash->getCashOb();
    }

    /*
     * perform authentication user
     * @return void
     */

    public static function authenticateUser() {
        //	var_dump($GLOBALS['registry']->session->getSession('formKey'));

        $requestInfo = $GLOBALS ['registry']->request->getRequstInfo();

        $clientIP = (!empty($requestInfo ['REMOTE_ADDR'])) ? $requestInfo['REMOTE_ADDR'] : ((!empty($_ENV ['REMOTE_ADDR'])) ? $_ENV ['REMOTE_ADDR'] : getenv('REMOTE_ADDR'));
        //$userIP = encodeIP ( $clientIP );
        $GLOBALS ['registry']->userIP = $clientIP;
        //@$GLOBALS ['registry']->msg->writeUserLog ( $userIP );				
        //$formKey=($GLOBALS ['registry']->request->getCookie('formKey')!=NULL)?$GLOBALS ['registry']->request->getCookie('formKey'):($GLOBALS['registry']->session->getSession('formKey')!=NULL)?$GLOBALS['registry']->session->getSession('formKey'):'';
        /*

          if(strtolower($requestInfo['REQUEST_METHOD'])=='post'){// formCheck

          $checkID=($GLOBALS ['registry']->request->getPost('currentCheckId')!='')?$GLOBALS ['registry']->request->getPost('currentCheckId'):$GLOBALS ['registry']->request->getGet('currentCheckId');

          if($formKey!=NULL)
          if($formKey!=$checkID)
          {
          //	die(var_dump($GLOBALS['registry']->session->getSession('formKey').' <br />'.$GLOBALS ['registry']->request->getPost('currentCheckId'))); //problem of redirection

          //redirectTo(array('controller'=>DEFAULT_CONTROLLER,'action'=>DEFAULT_ACTION));
          }

          else{
          $GLOBALS['registry']->session->destroySessionKey('formKey');
          $GLOBALS ['registry']->response->deleteCookie('formKey');
          $formKey=NULL;
          }
          }
          elseif(@empty ( $requestInfo ['HTTP_X_REQUESTED_WITH'] ) && @strtolower ( $requestInfo ['HTTP_X_REQUESTED_WITH'] ) != 'xmlhttprequest'){//via ajax


          if($formKey==NULL){

          $formKey=md5(uniqid($GLOBALS ['registry']->rsaKeys[1]));
          $GLOBALS ['registry']->response->deleteCookie('formKey');
          $GLOBALS['registry']->session->destroySessionKey('formKey');


          $GLOBALS['registry']->session->setSession('formKey',$formKey);
          $GLOBALS ['registry']->response->setCookie('formKey', $formKey, 60*60*24);
          }
          //var_dump($GLOBALS['registry']->session->getSession('formKey'));
          }
          $GLOBALS ['registry']->formKey=	$formKey;
         */



        $GLOBALS ['registry']->authSetting = new AuthSetting(/* $loginOption= */array('autoLogin' => false, 'maxLoginTime' => 5, 'sessionLength' => 10800, 'loginRestTime' => 30, 'maxLoginAttempts' => 5), $cookieOptions = array('cookieName' => 'SMS_2_SQL', 'cookiePath' => '/', 'domain' => NULL, 'secure' => NULL), 1, NULL, $clientIP);
        /*
          $GLOBALS['registry']->authentication=new Authentication($GLOBALS['registry']->authSetting);
          $GLOBALS['registry']->userSessionObject=$GLOBALS['registry']->authentication->sessionPageStart($GLOBALS ['registry']->request->requestId, $userIP); */
        /*
          $GLOBALS ['registry']->userSessionObject=$GLOBALS['registry']->session->getSession ( 'userSession', true );
          if(!is_object($GLOBALS ['registry']->userSessionObject))
          $GLOBALS ['registry']->userSessionObject=$GLOBALS['registry']->request->getCookie('userSession',true);
         */
    }

    /*
     * a set of function call , variables to start application
     * @author Hussam El-Kurd
     * */

    public static function startApp() {
        /*
         * define Request Language
         */
        self::languageDefine();
    }

}
