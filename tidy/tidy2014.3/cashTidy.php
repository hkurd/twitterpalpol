<?php
/**
 * cashELIDApps
 * cash apps as compressed version 
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
include 'version.php';
class cashTIDY {
    
    private $_files = array();
    private $_cashSetting = array();

    public function __construct() {
        $this->_cashSetting = array(
            'APP_NAMESPACE'=> 'TIDY',
            'APP_FILE'=> 'tidy.compress',
            'APP_VERSION'=> TIDY_VERSION,
            
            );
    }
    /**
     * files to be cashed
     */
    private function _defineFiles() {

        foreach (glob('*.php') as $tidyFile) {
            if(strstr($tidyFile, basename(__FILE__, '.php')))
                continue;
            elseif (strstr($tidyFile, 'cleaner')){
                 $cleanerFile = $tidyFile;
                 var_dump($cleanerFile);
                 continue;
            }
            elseif (!is_dir($tidyFile))
                $this->_files[] = $tidyFile;
        }
        foreach (glob('db/*.php') as $tidyFile) {
                $this->_files[] = $tidyFile;
        }      
        foreach (glob('db/drivers/*.php') as $tidyFile) {
                $this->_files[] = $tidyFile;
        }              
        foreach (glob('api/*.php') as $tidyFile) {
                $this->_files[] = $tidyFile;
        }  
        foreach (glob('mvc/*.php') as $tidyFile) {
                $this->_files[] = $tidyFile;
        }    
        
        $this->_files[] = $cleanerFile;
    }

    /**
     * clear files from comments && spaces
     * @param string contnet
     * return clean content
     */
    public function compressContent($content) {
        $combressContent = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);
        $combressContent = str_replace(array("\r", "\t", '<?php','?>'), '', $combressContent);
        $combressContent = str_replace(array("\n\n", "\n\n\n", "\n"), "\n", $combressContent);
        $combressContent = str_replace(array('  ', "    "), " ", $combressContent);


        return $combressContent;
    }

    /**
     * cash files 
     * @param array files array
     */
    public function cashAppConent() {
        $this->_defineFiles();
        $aLastModifieds = array();
        if (is_array($this->_files) && sizeof($this->_files) > 0) {
            //files 
            foreach ($this->_files as $sFile) {
                if (file_exists($sFile)) {
                    $aLastModifieds [] = filemtime("$sFile");
                    $sCode .= file_get_contents("$sFile");
                }
            }

            $sCode = $this->compressContent($sCode);
            $appBpFile = $this->_cashSetting['APP_FILE'] . ".php";
            $cashLine = '<?php '."\n".'// ' . $this->_cashSetting['APP_VERSION'] . ' , Date: ' . date('Y/m/d',time()) . "\n" . 'namespace ' . $this->_cashSetting['APP_NAMESPACE'] . ';';
            //clear file
            $this->_writeFileCode($appBpFile, "");
            //write code
            $this->_writeFileCode($appBpFile, $cashLine.$sCode);
            //clean lins code
           
            $cleanCodeCode = '';
            
            $in = fopen($appBpFile, 'r');
            while (!feof($in)) {
                $line = fgets($in);
                $line = $this->cleanLine($line);
                $cleanCodeCode.= $line;
            }
            fclose($in);
          
            //write clean code
            $this->_writeFileCode( $appBpFile,$cashLine.$cleanCodeCode);
             
            echo 'Cashed';
        }
    }

    /**
     * write code to a file
     * @param string path for file 
     * @param string content
     * @return void
     */
    private function _writeFileCode($path, $content) {
        if (is_file($path)) {
            $oFile = fopen($path, 'w');
            if (flock($oFile, LOCK_EX)) {
                fwrite($oFile, $content);
                flock($oFile, LOCK_UN);
            }
            fclose($oFile);
        }
    }

    /**
     * Main cleaning function.
     */
    public function cleanLine($line) {
        static $inComment = FALSE;
        if ($inComment) {
            $inComment = strpos($line, '*/') === FALSE;
            return '';
        }
        // The last condition rules out XPath with //* in them.
        elseif (!$inComment && strpos($line, '/*') !== FALSE && 
                strpos($line, '//*') === FALSE &&
                strpos($line, 'preg') === FALSE &&
                strpos($line, 'replace') === FALSE &&
                strpos($line, 'mb_') === FALSE &&
                strpos($line, 'eregi') === FALSE 
                ) {
            $inComment = TRUE;
            return '';
        } elseif (strpos($line, 'require') === 0) {
            return '';
        } elseif (($p = strpos($line, '// ')) !== FALSE) {
            $line = substr($line, 0, $p);
            $line = trim($line);
            return empty($line) ? '' : $line . PHP_EOL;
        } elseif (strpos($line, '<?php') !== FALSE) {
            return '';
        } elseif (strpos($line, '//') === FALSE && in_array(substr($line, strlen($line) - 2, 1), array('}', ':', ',', ';'))) {
            $line = rtrim($line);
        }
        return ltrim($line);
    }
}

$cashTIDY = new cashTIDY();
$cashTIDY->cashAppConent();