<?php

/**
 * AppCash, Tidy PHP 
 * Cash variables and pages
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @see ZEND_CASH 
 * @filesource
 */
class Cash {

    private $_cash;

    /**
     * constructor define the cash setting
     * @access public
     * @param integer $lifeTime in secodns	 
     * @param string $path	 	 
     * @return void
     */
    function __construct($lifeTime, $path) {

        $frontendOptions = array('lifetime' => $lifeTime, 'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => $path);
        $this->_cash = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
    }

    /**
     * get the cash object
     * @access public 
     * @return object cashs
     */
    public function getCashOb() {
        return $this->_cash;
    }

}
