<?php

//namespace Tidy\DB;

class sqlsrv implements sqlDriversInterface {

    /**
     * @see DB class
     * @access public
     * @var object
     */
    public $db;

    /**
     * connection string
     * @access private
     * @var string
     */
    private $connectionString = '';

    public function __construct($db) {
        $this->db = $db;
        $this->connectionString = "sqlsrv:server=%s;Database=%s;%s";
    }

    public function getSimpleLimitQuery($model, $columns = "*", $condition = NULL, $limit = 1, $otherTablesJoin = NULL) {
        if (is_string($model))
            $model = $model . 'Model';
        $model = is_object($model) ? $model : new $model ();

        $selectResource = $model->tableName;
        $otherTablesJoin = is_array($otherTablesJoin) ? ',' . implode(',', $otherTablesJoin) : NULL;
        $qry = "SELECT TOP $limit $columns FROM $selectResource  $otherTablesJoin WHERE 1=1 ";
        $condition = isset($condition) ? $condition : '';
        $qry .= $condition;
        return $qry;
    }

    public function limitSql($model, $columns, $selectResource, $limit, $condition = NULL, $sortQuery = '' , $otherTablesJoin = NULL) {

        $model = is_string($model) ? $model . 'Model' : $model;
        $model = is_object($model) ? $model : new $model ();
        $modelName = (is_object($model)) ? get_class($model) : $model;
        eval('$sort =' . $modelName . '::$sortColumns;');

        $primary = $model->primaryKeys;

        $condition = $conditionInternal = isset($condition) ? $condition : $this->db->getCondition($model, $debugString);

        $otherTablesJoin = is_array($otherTablesJoin) ? implode(',', $otherTablesJoin) : '';
        
        if($sortQuery == ''){
            $sort = is_array($sort) ? implode(',', $sort) : $sort;

            if (is_array($primary)) {
                $selectPrimary = implode(',', $primary);
                if (!in_array($sort, $primary))
                    $selectPrimary .= ',' . $sort;
                $newfunc = create_function('$a', 'return "CONVERT(NVARCHAR(MAX),$a,1)";');
                $primary = '(' . implode('+', array_map($newfunc, $primary)) . ')';
            } else
                $selectPrimary = $primary;
                if ($sort != NULL) {
                    eval('$sortType =' . $modelName . '::$sortType;');
                }
                else{
                    $sort = $primary;
                    $sortType = 'desc';
                }
                $sortQuery .= " ORDER BY  $sort  $sortType";
            }
            
        $qry = 'SELECT * FROM (SELECT ROW_NUMBER() OVER ('.$sortQuery.') AS Row, ' . $columns . '  FROM ' . $selectResource . ' ' . $otherTablesJoin . '  WHERE 1=1 ' . $condition . ') AS LIMIT_TABLE ';
        $qry.=  'WHERE Row BETWEEN '.$limit [0].' AND '.$limit [1].'';

        return $qry;
    }

    /* (non-PHPdoc)
     * @see sqlDriversInterface::validateColumn()
     */

    public function validateColumn($table, $column, $value) {
        // TODO Auto-generated method stub
    }

    /* (non-PHPdoc)
     * @see sqlDriversInterface::getConnectionString()
     */

    public function getConnectionString() {
        return $this->connectionString;
    }

    /**
     * set output id for non identity 
     * @param object model to get info 
     * @param string query 
     * @return string query with custom
     */
    public function setOutPutID($model, $query) {
        if (!is_array($model->primaryKeys) && $model->primaryKeys != '') { // there is a single primary key            
            $query.=' OUTPUT INSERTED.' . $model->primaryKeys . ' ';
        }
        return $query;
    }

    /**
     * get output id after insert
     * @param object execution statmnet.
     * @param object current db statment
     * @return mixed output
     */
    public function getInsertedOutputID($stmt) {
        try {
            $outPutParams = $stmt->fetch(PDO::FETCH_NUM);
            if (is_array($outPutParams) && sizeof($outPutParams) > 0) {
                return $outPutParams[0];
            }
        } catch (Exception $e) {
            return false;
        }
    }

}
