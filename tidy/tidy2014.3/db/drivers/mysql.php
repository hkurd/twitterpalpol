<?php

/**
 * DB, Tidy PHP 
 * reperesent all mysql driver function the db can intercat with it
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage DB
 * @filesource
 */
//namespace Tidy\DB;

class mysql implements sqlDriversInterface {

    /**
     * @see DB class
     * @access public
     * @var object
     */
    public $db;

    /**
     * connection string
     * @access private
     * @var string
     */
    private $connectionString = '';

    /**
     * types in mysql
     * @access private
     * @var array
     */
    private $_mysqlTypes = array();

    /**
     * constructor 
     * set the mysql data types and database object
     * @param object db set the data base object
     * @see DB class	   
     * @access public	 	 	 
     */
    public function __construct($db) {
        $this->db = $db;
        $this->connectionString = "mysql:host=%s; dbname=%s";
        $this->_mysqlTypes = array('integerTypes' =>
            array('tinyint' => array('127'/* signed */, '255'/* unsigned */),
                'smallint' => array('32767'/* signed */, '65535'/* unsigned */),
                'mediumint' => array('8388607'/* signed */, '16777215'/* unsigned */),
                'int' => array('2147483647'/* signed */, '4294967295'/* unsigned */),
                'int24' => array('2147483647'/* signed */, '4294967295'/* unsigned */)
                , 'integer' => array('2147483647'/* signed */, '4294967295'/* unsigned */)
                , 'bigint' => array('9223372036854775807'/* signed */, '18446744073709551615'/* unsigned */)),
            'floatTypes' =>
            array('decimal', 'float', 'double', 'real', 'numeric'),
            'stringTypes' =>
            array('char', 'varchar', 'tinytext', 'text', 'mediumtext', 'longtext', 'blob', 'tinyblob', 'varblob', 'mediumblob', 'longblob', 'binary'),
            'bitTypes' =>
            array('boolean', 'bit')
        );
    }

    /**
     * get the query for mysql navigation
     * @access public	 	
     * @param object $model  string of query to get the limits	  	 
     * @param string $columns  
     * @param string $selectResource  	 
     * @param array limits   	 	
     * @param string $condition direct condition 	 	    	 
     * @param array $otherTablesJoin  join in the query	 	   	 
     * @return string of quert 
     */
    public function limitSql($model, $columns, $selectResource, $limit, $condition, $otherTablesJoin) {
        $model = is_string($model) ? $model . 'Model' : $model;
        $model = is_object($model) ? $model : new $model ();
        $modelName = (is_object($model)) ? get_class($model) : $model;

        eval('$sort = ' . $modelName . '::$sortColumns;');
        eval('$sortType = ' . $modelName . '::$sortType;');

        ;
        $primary = $model->primaryKeys;

        $condition = $conditionInternal = isset($condition) ? $condition : $this->db->getCondition($model, $debugString);

        $qry = 'SELECT  ' . $columns . ' FROM ' . $selectResource . ' ' . $otherTablesJoin . '  WHERE 1=1 ' . $condition . ' ORDER BY ' . $sort . ' ' . $sortType . " LIMIT " . $limit [0] . ", " . $limit [1];

        return $qry;
    }

    /**
     * get column type 
     * @access public	 
     * @param string $table  	  	    	 	 	
     * @param string $columns  
     * @return array of column 
     */
    public function getColumnType($table, $column) {

        $sql = 'SELECT DISTINCT COLUMN_NAME,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,IS_NULLABLE ,COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS ' .
                'WHERE TABLE_NAME = :table AND COLUMN_NAME = :column';

        $st = $this->db->pdoResource->prepare($sql);
        $st->execute(array(':table' => $table, ':column' => $column));
        $row = $st->fetch(PDO::FETCH_NUM);
        //name
        $columnInfo['name'] = $row[0];
        //type	
        //$parts = preg_split('/\s*\(/', trim($row[1]));//type
        $columnInfo['type'] = strtolower(trim($row[1]));
        $columnInfo['length'] = $row[2];
        $columnInfo['null'] = ($row[3] == 'NO') ? false : true;

        if (strstr($row[4], 'unsigned')) {
            $columnInfo['unsigned'] = true;
        }

        return $columnInfo;
    }

    /**
     * check column according to db types 
     * @access public	 
     * @param array $column  	  	    	 	 	
     * @param string $value  
     * @return array 
     */
    public function checkTheColumnValueAccordingToDB($column, $value) {

        $candidateValue = NULL;
        switch ($column['type']) {

            case in_array($column['type'], array_keys($this->_mysqlTypes['integerTypes'])): {

                    if (!$GLOBALS ['registry']->validate->isInteger($value)) {
                        //casting	
                        $candidateValue = (int) $value;
                        $msg = ' value of ' . $columnInfo['name'] . ' must be int  , candidate Value -> ' . $candidateValue;
                    }

                    if ($columnInfo['unsigned'])
                        $maxLength = $this->_mysqlTypes['integerTypes'][$column['type']][1];
                    else
                        $maxLength = $this->_mysqlTypes['integerTypes'][$column['type']][0];

                    if ($value >= $maxLength) {
                        //casting	
                        $candidateValue = (int) $value;
                        $msg = ' value of ' . $columnInfo['name'] . ' is bigger than noraml value type  , candidate Value -> ' . $candidateValue;
                    }
                } break;


            case in_array($column['type'], $this->_mysqlTypes['stringTypes']): {

                    if (!is_string($value)) {
                        //casting	
                        $candidateValue = (string) $value;
                        $msg = ' value of ' . $columnInfo['name'] . ' must be string  , candidate Value -> ' . $candidateValue;
                    }
                    if (!$GLOBALS ['registry']->validate->maxChar($column['length'] + 1, $value)) {

                        $candidateValue = substr($value, 0, $column['length']);

                        $msg = ' Wrong in Length of ' . $columnInfo['name'] . ', candidate Value -> ' . $candidateValue;
                    }

                    if (!$column['null'] and !$GLOBALS ['registry']->validate->notEmpty($value)) {
                        $candidateValue = '';
                        $msg = ' value of ' . $columnInfo['name'] . ' Can\'t be null , candidate Value -> ' . $candidateValue;
                    }
                } break;


            case in_array($column['type'], $this->_mysqlTypes['floatTypes']): {

                    if (!$GLOBALS ['registry']->validate->isFloat($value)) {
                        //casting	
                        $candidateValue = (float) $value;
                        $msg = ' value of ' . $columnInfo['name'] . ' must be float , candidate Value -> ' . $candidateValue;
                    }

                    if (!$column['null'] and !$GLOBALS ['registry']->validate->notEmpty($value)) {
                        $candidateValue = 0;
                        $msg = ' value of ' . $columnInfo['name'] . ' Can\'t be null , candidate Value -> ' . $candidateValue;
                    }
                } break;




            case 'datetime': {
                    if (strtotime($value) <= strtotime('01/01/1970')) {

                        $candidateValue = date('Y-m-d H:i:s', time());
                        $msg = ' value of ' . $columnInfo['name'] . ' must be date time , candidate Value -> ' . $candidateValue;
                    }
                } break;

            case 'date': {
                    if (strtotime($value) <= strtotime('01/01/1970')) {

                        $candidateValue = date('Y-m-d ', time());
                        $msg = ' value of ' . $columnInfo['name'] . ' must be date , candidate Value -> ' . $candidateValue;
                    }
                } break;

            case 'timestamp': {

                    if ((int) $value <= strtotime('01/01/1970')) {

                        $candidateValue = time();
                        $msg = ' value of ' . $columnInfo['name'] . ' must be timeStamp , candidate Value -> ' . $candidateValue;
                    }
                } break;

            case in_array($column['type'], $this->_mysqlTypes['bitTypes']): {

                    if (!$GLOBALS ['registry']->validate->isDigit($value) or strlen($value) > 1) {
                        //casting	
                        $candidateValue = (bool) $value;
                        $msg = ' value of ' . $columnInfo['name'] . ' must be bit , candidate Value -> ' . $candidateValue;
                    }
                    if (!$GLOBALS ['registry']->validate->lessThan($column['length'] + 1, $value)) {

                        $candidateValue = $column['length'];
                        $msg = ' Wrong in Length of ' . $columnInfo['name'] . ', candidate Value -> ' . $candidateValue;
                    }

                    if (!$column['null'] and !$GLOBALS ['registry']->notEmpty($value)) {
                        $candidateValue = $column['length'];
                        $msg = ' value of ' . $columnInfo['name'] . ' Can\'t be null , candidate Value -> ' . $candidateValue;
                    }
                } break;
        }

        if ($candidateValue != NULL) {

            $return['candidate'] = $candidateValue;
            $return['conflict'] = true;
            $return['realValue'] = $value;
            $return['msg'] = $msg;
        } else {
            $return['conflict'] = false;
            $return['realValue'] = $value;
        }
        return $return;
    }

    /* (non-PHPdoc)
     * @see sqlDriversInterface::validateColumn()
     */

    public function validateColumn($table, $column, $value) {

        $columnDB = $this->getColumnType($table, $column);
        $result = $this->checkTheColumnValueAccordingToDB($columnDB, $value);
        return $result;
    }

    /* (non-PHPdoc)
     * @see sqlDriversInterface::getConnectionString()
     */

    public function getConnectionString() {
        return $this->connectionString;
    }

    public function getInsertedOutputID($stmt) {
        
    }

    public function setOutPutID($model, $query) {
        
    }

}
