<?php

/**
 * DB, Tidy PHP 
 * sql driver interface 
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage DB
 * @filesource
 */
//namespace Tidy\DB;

interface sqlDriversInterface {

    /**
     * limit sql for models
     * @access public	 	
     * @param object $model  string of query to get the limits	  	 
     * @param string $columns  
     * @param string $selectResource  	 
     * @param array limits   	 	
     * @param string $condition direct condition 	 	    	 
     * @param array $otherTablesJoin  join in the query	 	   	 
     * @return string of quert 
     */
    public function limitSql($model, $columns, $selectResource, $limit, $condition, $otherTablesJoin);

    /**
     * validate column 
     * @access public	 
     * @param string $table  	  	    	 	 	
     * @param string $columns  
     * @param string $value  	 
     * @return array of column 
     */
    public function validateColumn($table, $column, $value);

    /**
     * get connection string
     * @access public
     * @return string
     */
    public function getConnectionString();

    /**
     * set output id for non identity
     * @param object model to get info 
     * @param string query 
     * @return string query with custom
     */
    public function setOutPutID($model, $query);

    /**
     * ge output id after insert
     * @param object statment
     */
    public function getInsertedOutputID($stmt);
}
