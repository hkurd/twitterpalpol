<?php

/**
 * AppDB, Tidy PHP 
 * responsible for all the operations in the db
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage DB 
 * @filesource
 */

/**
 * Class for Build And Execute Query using Models
 * @copyright  Copyright (c) 2010 Hussam Abd El-Razek El-Kurd)
 */
class DB {

    /**
     * To get PDO Object
     * @access protected
     * @var object
     */
    public $pdoResource;

    /**
     * To get prepared statment Object
     * @access protected
     * @var object
     */
    protected $stmt = FALSE;

    /**
     * flag debug option
     * @access private
     * @var boolean
     */
    private $_debug = TIDY_CONSTANTS::DEBUG_FULL_MODE;

    /**
     * Statment Params
     * @access private
     * @var mixed
     */
    private $_param = array();

    /**
     * data Columns And Values
     * @access private
     * @var array
     */
    private $_data = array();

    /**
     * condations params
     * @access private
     * @var array
     */
    private $_condationsParms = array();

    /**
     * last inserted id
     * @access private
     * @var int
     */
    private $_insertID;

    /**
     * QueryBuilder object
     * @access private
     * @var array
     */
    private static $QueryBuilder = array();

    /**
     * check if object created
     * @access private
     * @var array
     */
    private static $isCreated = array(0 => false);

    /**
     * pdo object created
     * @access private
     * @var boolean
     */
    private static $pdoOutSource = FALSE;

    /**
     * driver object 
     * @access private
     * @var boolean
     */
    public $driver = FALSE;

    /**
     * enable multi Parameter Statment 
     * @access public
     * @var boolean
     */
    public $multiParamStmt = false;

    /**
     * values Setting
     * @access public
     * @var array
     */
    public $valueValidateSetting = array();

    /**
     * databaseConection array
     * @access private
     * @var array
     */
    private $_databaseConfig = array();

    /**
     * constructor 
     * @param string $server  server to connect
     * @param string $username database username 
     * @param string $password database password 	
     * @param string $db database name
     * @param string  $driver driver name	
     * @param string  extra connection options	  
     * @access public	 	 	 
     */
    private function __construct($server, $username, $password, $db, $driver, $options = '') {
        if (self::$pdoOutSource === FALSE) {
            if (!interface_exists('sqlDriversInterface'))
                include 'drivers' . DS . 'sqlDriversInterface.php';
            if (!class_exists($driver))
                include 'drivers' . DS . $driver . '.php';

            $this->valueValidateSetting = array('check' => true, 'candidateValue' => true);
            $this->_databaseConfig = array('server' => $server, 'userName' => $username, 'password' => $password, 'databaseName' => $db, 'driver' => $driver);
            $this->driver = new $driver($this);

            try {
                $this->driver = new $driver($this);

                $this->pdoResource = new PDO(sprintf($this->driver->getConnectionString(), $server, $db, $options), $username, $password); //"crm", "012012"); 
                //$this->pdoResource  =new PDO("mysql:host=$host;dbname=$db", $username, $password);
                //$errorMode=($this->_debug==TIDY_CONSTANTS::DEBUG_NO_MODE)?ERRMODE_SILENT:ERRMODE_EXCEPTION;
                $this->pdoResource->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->pdoResource->setAttribute(constant('PDO::SQLSRV_ATTR_DIRECT_QUERY'), true);

                // $this->pdoResource ->query('SET NAMES utf8');
            } catch (pdoException $e) {
                $this->pdoResource = FALSE;
                die("Error connecting to SQL Server : " . $e->getMessage());
            }
        } else
            $this->pdoResource = self::$pdoOutSource;
    }

    /**
     * initiate DataBase Connection
     * @param array $databaseConfig  contain connection information
     * @param int index (for multi instances)
     * server,userName,password,databaseName,driver
     * @return QueryBuilder Object
     */
    public static function createBuilder(array $databaseConfig, $instanceIndex = 0) {
        if (FALSE == @self::$isCreated[$instanceIndex]) {
            if (NULL == @self::$QueryBuilder[$instanceIndex]) {
                @self::$QueryBuilder[$instanceIndex] = new DB($databaseConfig ['server'], $databaseConfig ['userName'], $databaseConfig ['password'], $databaseConfig ['databaseName'], $databaseConfig ['driver'], (array_key_exists('connectionOptions', $databaseConfig) ? $databaseConfig ['connectionOptions'] : NULL));
            }
            @self::$isCreated[$instanceIndex] = TRUE;
            return @self::$QueryBuilder[$instanceIndex];
        } else {
            return NULL;
        }
    }

    /**
     * close Builder 
     * @param int index (for multi instances)
     * @param object $queryBuilderClose queryBuilder
     */
    public function closeBuilder(DB $queryBuilderClose, $instanceIndex = 0) {
        self::$isCreated[$instanceIndex] = FALSE;
    }

    /**
     * connect directly to pdo resource
     * @param object  driver 
     * @params array connection $databaseConfig
     * @return object pdoresource
     */
    public function connectPdoResource($driver, $databaseConfig) {
        try {
            $this->driver = new $driver($this);

            $this->pdoResource = new PDO(sprintf($this->driver->getConnectionString(), $databaseConfig ['server'], $databaseConfig ['databaseName'], $databaseConfig ['connectionOptions']), $databaseConfig ['userName'], $databaseConfig ['password']); //"crm", "012012");		
            $this->pdoResource->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (pdoException $e) {
            $this->pdoResource = FALSE;
            die("Error connecting to SQL Server : " . $e->getMessage());
        }
        self::$pdoOutSource = $this->pdoResource;
        return $this->pdoResource;
    }

    /**
     * set Special pdoResource object 
     * @access public	 
     * @param object $pdoResource pdoResource
     */
    public static function setPdoResource($pdoResource) {
        self::$pdoOutSource = $pdoResource;
    }

    /**
     * set debug function  
     * @access public	 	 	 	 
     * @param boolean $_debug  flag set debug option 
     * MODES:
     * TIDY_CONSTANTS::DEBUG_NO_MODE no mode enabled
     * TIDY_CONSTANTS::DEBUG_QUERY_MODE no mode enabled
     * TIDY_CONSTANTS::DEBUG_VALUE_MODE values in queries
     * TIDY_CONSTANTS::DEBUG_RETURN_MODE result in queries
     * DEBUG_FULL_MODE full mode debug
     * @return void
     */
    public function setDebug($_debug) {
        $this->_debug = $_debug;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////Execute Function				

    /* update model using addStmtPrep | editStmtPrep according to primary keys set this function help to 
     * automaticlly decide which operation is executed 
     * @param object model 
     * @param boolean execute directly or not
     * @return mixed statment object or execution result
     * */
    public function updateStmtPrep($model, $execute) {
        if ($this->_checkUpdatePrimaryCondition($model)) {
            $stmt = $this->query($this->driver->getSimpleLimitQuery($model, '1', NULL, 1));
            $stmt->execute();
            if ($stmt->fetch()) {
                return ($this->editStmtPrep($model, $execute, false) > 0) ? true : false;
            } else {
                $condition = $model->getCondition();
                if (is_array($condition) and (sizeof($condition) > 0))
                    foreach ($condition as $key => $val)
                        $model->$key = $val;

                $status = $this->addStmtPrep($model, $execute);
                return (($this->insertID() > 0) ? $this->insertID() : $status);
            }
        }
        else {
            $status = $this->addStmtPrep($model, $execute);
            return (($this->insertID() > 0) ? $this->insertID() : $status);
        }
    }

    public function getSimpleRecored() {
        $stmt = $this->query($this->driver->getSimpleLimitQuery($model, '1', NULL, 1));
        $stmt->execute();
    }

    /**
     * Create Prepared  Insert Statment using Model Class
     * @access public	 	 
     * @param object $Model  class filled from datatbase
     * @param boolean execute directly or not
     * @return mixed statment object or execution result
     */
    public function addStmtPrep($model, $execute = false) {

        $this->clear();
        $table = $model->tableName;
        $this->_data = $model->getData();
        $this->_debug('<b>Insert ' . $table . '-----------------------------------------------------</b>');
        $sizeOfData = sizeof($this->_data);

        $into = "INSERT INTO $table  (";
        $values = 'VALUES (';
        //$valueArray=implode(',',$data);
        $i = 0;
        foreach ($this->_data as $key => $value) {
            $fieldName = $key;

            $this->_valuesValidateAccordingDatabaseType($table, $fieldName, $value);

            if ($value == NULL) {
                if ($i == $sizeOfData - 1) {
                    $into = substr($into, 0, strlen($into) - 1);
                    $values = substr($values, 0, strlen($values) - 1);
                }
                continue;
            } elseif (preg_match("/^.*\(+.*\)+/", $value)) {
                $stripValue = stripslashes($value);
                $values .= (is_string($stripValue) ? "'$stripValue'" : $stripValue) . (($i == $sizeOfData - 1) ? '' : ',');
            } else {
                $paramKey = $this->_getParamKey($key);
                $values .= $paramKey . (($i == $sizeOfData - 1) ? '' : ',');
                $this->_param [$paramKey] = $value;
                $debugString .= '<br />' . $paramKey . '=' . $value;

                //$this->sp->Add_Parameter($fieldName, $this->checkValueBindType($value));		
            }
            $into .= $fieldName . (($i == $sizeOfData - 1) ? '' : ',');
            $i ++;
        }
        $into .= ') ';
        $values .= ') ';
        //add custom to insert query
        $execQuery = $this->driver->setOutPutID($model, $into) . $values;
        $this->_debug($execQuery, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        if ($debugString)
            $this->_debug($debugString, TIDY_CONSTANTS::DEBUG_VALUE_MODE);

        $this->stmt = $this->pdoResource->prepare($execQuery);

        if ($execute) {
            $exResult = $this->executeStmt();
            $this->setInsertID($this->driver->getInsertedOutputID($this->stmt, $this));

            $this->_debug($exResult, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
            return $exResult;
        }
        $this->_debug($this->stmt, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
        return $this->stmt;
    }

    /**
     * Get Last Insert Key from db 
     * @access public	 	 	 	 
     * @return mixed Last Insert Key
     */
    public function insertID() {
        return $this->pdoResource->lastInsertId();
    }

    /**
     * Set free insert ID (complex operation)
     * @access public
     * @return void
     */
    public function setInsertID($insertID) {
        return $this->_insertID = $insertID;
    }

    /**
     * get seted insert ID (by set function)(complex operation)
     * @access public
     * @return int seted insert id
     */
    public function getInsertID() {
        return $this->_insertID;
    }

    /**
     * Get row Count (affected,resultset ,deleted ...etc)
     * @access public
     * @return int rowCount
     */
    public function rowCount() {
        return $this->stmt->rowCount();
    }

    /**
     * Update Prepared   Statment using Model Class
     * @access public	 	 
     * @param object $Model  class filled from datatbase
     * @param boolean execute directly or not
     * @param boolean to check and update primary conditon 
     * @return mixed statment object or execution result
     */
    public function editStmtPrep($model, $execute = FALSE, $primaryCheck = true) {
        $this->clear();
        $table = $model->tableName;
        $this->_debug('<b>Update ' . $table . ' -----------------------------------------------------</b>');
        $into = 'UPDATE ' . $table . ' SET ';
        if ($primaryCheck)
            $this->_checkUpdatePrimaryCondition($model);
        $this->_data = $model->getData();
        $i = 0;
        $debugString = '';
        $condition = $this->_getCondition($model, $debugString);

        $dataKeys = array_keys($this->_data);
        //filter repated columns 
        foreach ($dataKeys as $key) {
            $paramKey = $this->_getParamKey($key);
            if (array_key_exists($paramKey, $this->_param)) {
                unset($this->_data[$key]);
            }
        };
        unset($dataKeys);
        $sizeOfData = sizeof($this->_data);
        foreach ($this->_data as $fieldName => $value) {
            $paramKey = $this->_getParamKey($fieldName);
            $this->_valuesValidateAccordingDatabaseType($table, $fieldName, $value);
            if ($value == NULL) {
                if ($i == $sizeOfData - 1) {
                    $into = substr($into, 0, strlen($into) - 1);
                }
                continue;
            } elseif (preg_match('/^.*\(+.*\)+/', $value)) {
                $into .= $fieldName . '=';
                $stripValue = stripslashes($value);
                $into .= (is_string($stripValue) ? "'$stripValue'" : $stripValue) . (($i == $sizeOfData - 1) ? '' : ',');
            } else {

                $into .= $fieldName . '=';
                $into .= $paramKey . (($i == $sizeOfData - 1) ? '' : ',');
                $this->_param [$paramKey] = $value;
                $debugString .= '<br />' . $paramKey . '=' . $value;

                //$this->sp->Add_Parameter($fieldName, $this->checkValueBindType($value));		
            }
            //	$into .= $fieldName . (($i == $sizeOfData - 1) ? '' : ',');	


            $i ++;
        }
        $into .= ' WHERE 1=1';
        $into .= $condition;
        $this->_debug($into, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        if ($debugString)
            $this->_debug($debugString, TIDY_CONSTANTS::DEBUG_VALUE_MODE);

        $this->stmt = $this->pdoResource->prepare($into);
        if ($execute) {
            $exResult = $this->executeStmt();
            $this->_debug($exResult, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
            return $exResult;
        }
        $this->_debug($this->stmt, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
        return $this->stmt;
    }

    /**
     * Delete Prepared   Statment using Model Class
     * @access public	 	 
     * @param object $Model  class filled from datatbase
     * @param boolean execute directly or not
     * @return mixed statment object or execution result
     */
    public function deleteStmtPrep($model, $execute = FALSE) {
        $this->clear();
        $table = $model->tableName;
        $this->_debug('<b>Delete From ' . $table . ' -----------------------------------------------------</b>');
        $debugString = '';
        $condition = $this->_getCondition($model, $debugString);
        $into = 'DELETE FROM ' . $table;
        $into .= ' WHERE 1=1';
        $into .= $condition;
        $this->_debug($into, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        if ($debugString)
            $this->_debug($debugString, TIDY_CONSTANTS::DEBUG_VALUE_MODE);
        $this->stmt = $this->pdoResource->prepare($into);
        if ($execute) {
            $exResult = $this->executeStmt();
            $this->_debug($exResult, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
            return $exResult;
        }
        $this->_debug($this->stmt, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
        return $this->stmt;
    }

    /**
     * free query execuition
     * @access public	 	 	 
     * @param string $queryString string of given query
     * @return mixed resutl of execuition
     */
    public function query($queryString) {
        $this->_debug($queryString, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        return $this->pdoResource->query($queryString);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////ResultSet Function				

    /**
     * get recored as model from db
     * @access public	 	 	 	 
     * @param string $clazz  class name
     * @param string $columns in the query
     * @param string $condition direct condition
     * @param string $view select from view	 	   	 	 
     * @param mixed $otherTablesJoin  join with other table
     * @return Object Model filled
     */
    public function getRecord($model, $columns = "*", $condition = NULL, $view = NULL, $otherTablesJoin = NULL) {
        $this->clear();
        $debugString = '';
        $model = $this->_getModel($model);

        $selectResource = isset($view) ? $view : $model->tableName;
        $otherTablesJoin = is_array($otherTablesJoin) ? ',' . implode(',', $otherTablesJoin) : NULL;
        $this->_debug('<b>Get Recored From ' . $selectResource . ' -----------------------------------------------------</b>');
        $qry = "SELECT $columns FROM $selectResource  $otherTablesJoin WHERE 1=1 ";
        $condition = isset($condition) ? $condition : $this->_getCondition($model, $debugString);
        $qry .= $condition;
        $this->_debug($qry, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        if ($debugString)
            $this->_debug($debugString, TIDY_CONSTANTS::DEBUG_VALUE_MODE);
        $this->stmt = $this->pdoResource->prepare($qry);
        $this->executeStmt();
        if ($row = $this->stmt->fetch(PDO::FETCH_NUM)) {
            $resultExists = TRUE;
            foreach ($row as $column_index => $column_value) {
                $columnInfo = $this->stmt->getColumnMeta($column_index);
                $columnName = $columnInfo ['name'];
                $model->$columnName = $column_value;
            }
        }
        if ($resultExists) {
            $this->_debug($model, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
            return $model;
        }
        $this->_debug(NULL, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
    }

    /**
     * get Record set
     * @access public
     * @param string $model  class name or object
     * @param string $columns in the query
     * @param string $condition direct condition
     * @param string $view select from view
     * @param mixed $otherTablesJoin  join with other table
     * @param string $orderKey enable to sort on this column
     * @return object model with set values
     */
    public function getRecordSet($model, $columns = '*', $condition = NULL, $view = NULL, $otherTablesJoin = NULL, $sort = NULL, $sortType = 'DESC') {
        $this->clear();
        $model = $this->_getModel($model);
        $modelName = get_class($model);

        $selectResource = isset($view) ? $view : $model->tableName;
        $condition = isset($condition) ? $condition : $this->_getCondition($model, $debugString);
        $otherTablesJoin = is_array($otherTablesJoin) ? ',' . implode(',', $otherTablesJoin) : NULL;
        $this->_debug('<b>Get Recored set From ' . $selectResource . ' -----------------------------------------------------</b>');
        $reflectionClass = new \ReflectionClass($model);
        $disableOrder = $reflectionClass->getShortName() . 'Order';
        $qry = "SELECT $columns FROM $selectResource $otherTablesJoin WHERE 1=1 $condition  ";
        if ($GLOBALS ['registry']->$disableOrder != 'disable') {
            if ($sort == NULL) {
                eval('$sort =' . $modelName . '::$sortColumns;');
                eval('$sortType =' . $modelName . '::$sortType;');
            }
            $qry .= " ORDER BY  $sort  $sortType";
        }
        $this->_debug($qry, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        if ($debugString)
            $this->_debug($debugString, TIDY_CONSTANTS::DEBUG_VALUE_MODE);
        $models = $this->fetchModelQuery($qry, $model);
        $this->_debug($models, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
        return $models;
    }

    /**
     * fetch query and set models in array format , used for all fetching functions and custom fetching
     * @access public
     * @param string query
     * @param mixed $model  class name or object
     * @param string format (default array)
     * @return array filled Models
     */
    public function fetchModelQuery($query, $model) {
        $models = array();
        $model = $this->_getModel($model);
        $this->stmt = $this->pdoResource->prepare($query);
        $this->executeStmt();
        while ($row = $this->stmt->fetch(PDO::FETCH_NUM)) {
            $model = new $model ();
            foreach ($row as $column_index => $column_value) {
                $columnInfo = $this->stmt->getColumnMeta($column_index);
                $columnName = $columnInfo ['name'];
                $model->$columnName = $column_value;
            }
            $modelOb = clone $model;
            $models[] = $modelOb->getData();
        }
        return $models;
    }

    /**
     * fetch query and set models in  array format , used for all fetching functions and custom fetching 
     * without params it works as query fetched directly
     * @access public
     * @param string query
     * @param mixed $model  class name or object
     * @param string format (default array)
     * @return array filled Models
     */
    public function quickFetchModelQuery($query, $model) {
        $models = array();
        $model = $this->_getModel($model);
        $this->stmt = $this->query($query);

        while ($row = $this->stmt->fetch(PDO::FETCH_NUM)) {
            foreach ($row as $column_index => $column_value) {
                $columnInfo = $this->stmt->getColumnMeta($column_index);
                $columnName = $columnInfo ['name'];
                $model->$columnName = $column_value;
            }
            $modelOb = clone $model;
            $models[] = $modelOb->getData();
        };

        return $models;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////Procedures Function				

    /**
     * execute procedure that return result set
     * @access public	 	 	 	 
     * @param string $procedureName  name of procedure
     * @param object $model  
     * @return array filled Models
     */
    public function resultProcedure($procedureName, $model) { //SELECT Procedure
        $this->clear();
        $models = array();
        $this->_debug('<b>Execute ' . $procedureName . ' -----------------------------------------------------</b>');
        $procdureExecString .= ' EXECUTE ' . $procedureName;
        $paramCounter = 0;
        $paramSize = sizeof($params);
        $this->_data = $model->getAllData();
        $paramSize = sizeof($this->_data);
        $paramCounter = 0;
        foreach ($this->_data as $fieldName => $value) {
            $paramKey = $this->_getParamKey($fieldName);
            $procdureExecString .= ' @' . $fieldName . '= ' . $paramKey . (($paramCounter == $paramSize - 1) ? '' : ' , ');
            $this->_param [$paramKey] = $value;
            $paramCounter ++;
        }
        $this->_debug($procdureExecString, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        $this->stmt = $this->pdoResource->prepare($procdureExecString);
        $this->executeStmt();
        $this->stmt->nextRowset();
        $rows = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        $resultCount = count($rows);
        if ($resultCount > 1) { //Return Multiple Value
            $models = array();
            foreach ($rows as $row) {
                foreach ($row as $columnName => $columnValue) {
                    $model->$columnName = $columnValue;
                }
                $modelOb = clone $model;
                $models[] = $modelOb;
            }
            $returnValue = $models;
        } elseif ($resultCount) { //Return one Value
            $columnName = key($rows [0]);
            $columnValue = $rows [0] [key($rows [0])];
            $model->$columnName = $columnValue;
            $returnValue = $model;
        }

        $this->_debug($returnValue, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
        return $returnValue;
    }

    /**
     * execute procedure that do an operation 
     * @access public	 	 	 	 
     * @param string $procedureName  name of procedure
     * @param object $model  
     * @param mixed execuation result 
     */
    public function exeProcedure($procedureName, $model) {
        $this->clear();
        $this->_debug('<b>Execute ' . (string) $procedureName . ' -----------------------------------------------------</b>');
        $procdureExecString .= 'DECLARE	 @returnVal int EXECUTE @returnVal=' . (string) $procedureName;
        $paramCounter = 0;
        $paramSize = @sizeof($params);
        if (!empty($model)) {
            $this->_data = $model->getAllData();
            $paramSize = sizeof($this->_data);
            $paramCounter = 0;

            foreach ($this->_data as $fieldName => $value) {
                $paramKey = $this->_getParamKey($fieldName);
                $procdureExecString .= ' @' . $fieldName . '= ' . $paramKey . (($paramCounter == $paramSize - 1) ? '' : ' , ');
                $this->_param [$paramKey] = $value;
                $this->_debug($paramKey . '=' . $value, TIDY_CONSTANTS::DEBUG_VALUE_MODE);

                $paramCounter ++;
            }
        }
        $procdureExecString .= ' SELECT "returnVal"=@returnVal';
        $this->_debug($procdureExecString, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        $this->stmt = $this->pdoResource->prepare($procdureExecString);
        $this->executeStmt();
        while ($this->stmt->nextRowset()) {
            try {
                $rows = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
                if (!empty($rows))
                    $returnValue = $rows [0] [key($rows [0])];
                if ($returnValue > 0)
                    break;
            } catch (pdoException $e) {
                continue;
            }
        }
        $this->_debug($returnValue, TIDY_CONSTANTS::DEBUG_RETURN_MODE);
        return $returnValue;
    }

    /**
     * execute procedure that do an operation and  have the ID as GUID
     * @access public
     * @param string $procedureName  name of procedure
     * @param object $model
     * @param mixed execuation result
     */
    public function exeProcedureGUID($procedureName, $model) {
        $this->clear();
        $this->_debug('<b>Execute ' . (string) $procedureName . ' -----------------------------------------------------</b>');
        $procdureExecString .= 'DECLARE @return_valueOut UNIQUEIDENTIFIER DECLARE @returnVal int EXECUTE @returnVal=' . (string) $procedureName;
        $paramCounter = 0;
        $guid = NULL;
        $paramSize = @sizeof($params);
        if (!empty($model)) {
            $this->_data = $model->getAllData();
            $paramSize = sizeof($this->_data);
            $paramCounter = 0;

            foreach ($this->_data as $fieldName => $value) {
                $paramKey = $this->_getParamKey($fieldName);
                $procdureExecString .= ' @' . $fieldName . '= ' . $paramKey . (($paramCounter == $paramSize - 1) ? '' : ' , ');
                $this->_param [$paramKey] = $value;
                $this->_debug($paramKey . '=' . $value, TIDY_CONSTANTS::DEBUG_VALUE_MODE);

                $paramCounter ++;
            }
        }
        $procdureExecString .= ',@GUID = @return_valueOut OUTPUT SELECT	 "GUID" = @return_valueOut  SELECT "returnVal"=@returnVal';
        $this->_debug($procdureExecString, TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        $this->stmt = $this->pdoResource->prepare($procdureExecString);
        $this->executeStmt();
        $counterLoopingForStmt = 0;
        while ($this->stmt->nextRowset()) {
            try {

                $rows = $this->stmt->fetchAll(PDO::FETCH_ASSOC);

                if (!empty($rows)) {
                    $counterLoopingForStmt++;
                    $guid = ($counterLoopingForStmt == 1) ? $rows [0] [key($rows [0])] : $guid;
                    $returnValue = ($counterLoopingForStmt == 2) ? $rows [0] [key($rows [0])] : $returnValue;
                }
                if ($returnValue > 0)
                    break;
            } catch (pdoException $e) {
                continue;
            }
        }
        $this->setInsertID($guid);
        return $returnValue;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////Transaction Function				

    /**
     * begin transaction
     * @access public	 	 	 	
     * @return void
     */
    public function beginTR() {
//        $this->pdoResource->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
        if (!$this->pdoResource->inTransaction()) {
            try {
                $this->pdoResource->beginTransaction();
            } catch (PDOException $e) {
                $this->rollBack();
            }
            $this->_debug('<b>Begin transaction</b>', TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        } else
            $this->_debug('<b>Begin Trans COMMAND WITH ALREDY ACTIVE TRANSACTION</b>', TIDY_CONSTANTS::DEBUG_QUERY_MODE);
    }

    /**
     * commit transaction
     * @access public	 	 	 	
     * @return void
     */
    public function endTR() {
        if ($this->pdoResource->inTransaction()) {
            $this->pdoResource->commit();
            $this->_debug('<b>End transaction</b>', TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        } else
            $this->_debug('<b>END COMMAND WITHOUT TRANSACTION</b>', TIDY_CONSTANTS::DEBUG_QUERY_MODE);
//        $this->pdoResource->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
    }

    /**
     * rollback transaction
     * @access public	 	 	 	
     * @return void
     */
    public function rollBack() {
        if ($this->pdoResource->inTransaction()) {
            $this->pdoResource->rollBack();
            $this->_debug('<b>RollBack transaction</b>', TIDY_CONSTANTS::DEBUG_QUERY_MODE);
        } else
            $this->_debug('<b>RollBack COMMAND WITHOUT TRANSACTION</b>', TIDY_CONSTANTS::DEBUG_QUERY_MODE);
//        $this->pdoResource->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////Helpers Function

    /**
     * get model name 
     * @param mixed model to get 
     * @return mixed according to model (name or object)
     */
    private function _getModel($model) {
        $model = is_string($model) ? $model . 'Model' : $model;
        $model = is_object($model) ? $model : new $model ();
        return $model;
    }

    /**
     * validate values according to database type for both insert and update
     * @access private	 	 
     * @param string $table name
     * @param string $column name
     * @param string $value 
     * @return mixed $value
     */
    private function _valuesValidateAccordingDatabaseType($table, $column, $value) {

        if ($this->valueValidateSetting['check']) {//enable check
            $table = (strpos($table, '.') != null) ? substr($table, strpos($table, '.') + 1, strlen($table)) : $table;
            $result = $this->driver->validateColumn($table, $column, $value);
            if ($result['conflict'] == true) {
                $this->_debug($result['msg'], TIDY_CONSTANTS::DEBUG_VALUE_MODE);

                if ($this->valueValidateSetting['candidateValue'])
                    return $result['candidate'];
            } else
                return $value;
        } else
            return $value;
    }

    /*
     * check if primary is seted in the model values , if so its automaticlly set it in model 
     * condition and remove it from the model value this function also used to check whether the operation 
     * is add or edit
     * @param object model with referance 
     * @return boolean if primary set it or not
     * */

    private function _checkUpdatePrimaryCondition(&$model) {
        $this->_debug('<b>check Primary for ' . $model->tableName . '------------------------------------------------</b>');
        $avaliablePrimaryKey = false;
        //getPrimary Keys to set it directly into condition
        $modelPrimary = $model->primaryKeys;
        $modelInitialCondition = $model->getCondition();
        if (is_array($modelPrimary) and (sizeof($modelPrimary) > 0)) {
            foreach ($modelPrimary as $primary) {
                if ($model->$primary != '') {
                    $model->addCondition($primary, $model->$primary);
                    unset($model->$primary); //remove it from normal data to condition data
                    $avaliablePrimaryKey = true;
                } else {// not all seted
                    $avaliablePrimaryKey = false;
                    $model->setCondition($modelInitialCondition);
                    reset($modelPrimary);
                    foreach ($modelPrimary as $primary)
                        $model->$primary = $primary;
                    break;
                }
            }
        } elseif ($modelPrimary != '') {// just one key
            if ($model->$modelPrimary != '') { //primary key already seted
                $avaliablePrimaryKey = true;
                $model->addCondition($modelPrimary, $model->$modelPrimary);
            }
        }
        unset($modelPrimary);
        unset($modelInitialCondition);
        return $avaliablePrimaryKey;
    }

    /**
     * Execute Prepared Statments
     * @access public	 	 	 	 
     * @param object $Model  class filled from datatbase
     * @param object $st PreparedStatment Object with bind
     * @return  number of affected rows
     */
    public function executeStmt($model = NULL, $st = NULL) {

        if (is_object($model)) {
            $this->_debug('', TIDY_CONSTANTS::DEBUG_VALUE_MODE);

            $this->_data = $model->getAllData();
            $sizeOfData = sizeof($this->_data);
            $i = 0;
            foreach ($this->_data as $fieldName => $value) {

                $paramKey = $this->_getParamKey($fieldName);

                if (preg_match("/^.*\(+.*\)+/", $value)) {
                    continue;
                }
                $value = stripslashes($value);
                $this->_param [$paramKey] = $value;
                //$this->stmt->bindParam(':'.$fieldName,$value, PDO::PARAM_STR);
                $i ++;
                $this->_debug($paramKey . '=' . $value, TIDY_CONSTANTS::DEBUG_VALUE_MODE);
            }
        }

        $result = $this->stmt->execute($this->_param);

        return $result;
    }

    /**
     * get condition from condition array
     * @access private	 	 	 	 
     * @param object $model  class 
     * @param  string $debugString for debugging
     * @return string of condition
     */
    private function _getCondition($model, &$debugString = '') {
        $condition = '';
        $this->_condationsParms = $model->getCondition();
        $sizeOfConParms = sizeof($this->_condationsParms);
        $conParmsCounter = 0;

        if ($sizeOfConParms > 0) {

            $condition = ' AND ';
            foreach ($this->_condationsParms as $fieldName => $value) {
                if (is_array($value)) {
                    $condition = $fieldName;
                    list ( $fieldName, $value ) = $value;
                }
                $condition .= $fieldName . '=';
                $paramKey = $this->_getParamKey($fieldName);

                if (preg_match("/^.*\(+.*\)+/", $value)) {
                    $stripValue = stripslashes($value);
                    $condition .= (is_string($stripValue) ? "'$stripValue'" : $stripValue) . (($conParmsCounter == $sizeOfConParms - 1) ? '' : ' AND ');
                } else {
                    $condition .= $paramKey . (($conParmsCounter == $sizeOfConParms - 1) ? '' : ' AND ');

                    $this->_param [$paramKey] = $value;
                    $debugString .= '<br />' . $paramKey . '=' . $value;
                }
                $conParmsCounter ++;
                continue;
            }
        }

        return $condition;
    }

    /**
     * debug function  
     * @param string $str  string need to debug
     * @param int $debugType  debug type	
     * @see Message	 	  
     * @access private	 	 
     * @return void
     */
    private function _debug($str, $debugType = NULL) {
        $errorMode = ($this->_debug == TIDY_CONSTANTS::DEBUG_NO_MODE) ? PDO::ERRMODE_SILENT : PDO::ERRMODE_EXCEPTION;
        $this->pdoResource->setAttribute(PDO::ATTR_ERRMODE, $errorMode);

        $GLOBALS ['registry']->msg->setDebugDB($this->_debug, $debugType, $str, TRUE);
    }

    /**
     * get param key
     * @access private	 	
     * @param string $fieldName  string  	 
     * @return string param key
     */
    private function _getParamKey($fieldName) {
        $paramKey = ":${fieldName}";
        if (is_array($this->_param) && !empty($this->_param))
            $paramKey = (array_key_exists($paramKey, $this->_param)) ? (($this->multiParamStmt) ? $paramKey . (intval(substr($paramKey, - 1)) + 1) : $paramKey) : $paramKey;
        return $paramKey;
    }

    /**
     * close connection   
     * @access public	 	 	 
     * @return void
     */
    public function close() {
        if ($this->pdoResource)
            $this->pdoResource = FALSE;
        if ($this->stmt)
            $this->stmt = FALSE;
    }

    /**
     * clear data,parms,condations  
     * @access public	 	 	 
     * @return void
     */
    public function clear() {
        $this->_data = NULL;
        $this->_param = NULL;
        $this->_condationsParms = NULL;
        $this->stmt = FALSE;
    }

    /**
     * destructor  
     * @access public	 	 	 
     */
    public function __destruct() {
        //$this->close();
    }

    /**
     * set serilize params before serilization
     * @access public
     */
    function __sleep() {
        return array('_databaseConfig', 'driver');
    }

    /**
     * after unserilization
     * @access public
     */
    function __wakeup() {
        $this->connectPdoResource($this->driver, $this->_databaseConfig);
    }

}
