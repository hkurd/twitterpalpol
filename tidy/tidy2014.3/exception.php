<?php

/**
 * App, Tidy PHP 
 * throws exceprion for tidy
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class TidyException extends \Exception {

    public function __construct($message = null, $code = 0) {
        if (!$message) {
            throw new $this('Unknown ' . get_class($this));
        }
        parent::__construct($message, $code);
    }

    public function __toString() {
        $msg = get_class($this) . " '{$this->message}' in {$this->file}({$this->line})\n"
                . "{$this->getTraceAsString()}";
        $GLOBALS ['registry']->msg->m($msg, TIDY_CONSTANTS::LOG_CRIT);
        return $msg;
    }

}
