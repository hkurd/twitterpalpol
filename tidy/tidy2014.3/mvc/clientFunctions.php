<?php

/**
 * ReqClientFunctions, Tidy PHP 
 * functions for the client side drawing Html , JS ...
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
/*
 * show image
 * @param string $src source
 * @param int $type 
 * @param array $imageProp array params
 * @return string image
 */
function showImage($src, $type = PANEL_THUMB_IMAGE, array $imageProp = NULL) {
    if ($imageProp != NULL)
        foreach ($imageProp as $key => $val)
            $propStr .= ' ' . $key . '="' . $val . '" ';
    switch ($type) {
        case PANEL_THUMB_IMAGE :
            $return = '<img src="' . $src . '" width="16px" height="16px" ' . (($imageProp ['class'] != '') ? '' : 'class="picto"') . ' ' . $propStr . '>';
            break;
        case GRID_THUMB_IMAGE :
            $return = '<img src="' . $src . '" width="50px" height="50px" ' . (($imageProp ['class'] != '') ? '' : 'class="dotted"') . ' ' . $propStr . '>';
            break;

        case ARTICLES_PUBS_MAIN_THUMB_IMAGE :
            $return = '<img src="' . (is_file(ARTICLES_PUBS_MAIN_THUMB_PATH_FILE . $src) ? ARTICLES_PUBS_MAIN_THUMB_PATH . $src : DEFAULT_THUMB) . '" ' . $propStr . '>';
            break;
        case POST_THUMB_IMAGE :
            $return = '<img src="' . (is_file(POST_THUMB_PATH_FILE . $src) ? POST_THUMB_PATH . $src : POST_THUMB_DEFAULT_PATH) . '"  ' . $propStr . '>';
            break;
        case ARTICLES_THUMB_IMAGE :
            $return = '<img src="' . (is_file(ARTICLES_THUMB_PATH_FILE . $src) ? ARTICLES_THUMB_PATH . $src : ARTICLES_THUMB_DEFAULT_PATH) . '" ' . $propStr . '>';
            break;
        case PUBS_THUMB_IMAGE :
            $return = '<img src="' . (is_file(PUBS_THUMB_PATH_FILE . $src) ? PUBS_THUMB_PATH . $src : PUBS_THUMB_DEFAULT_PATH) . '" ' . $propStr . '>';
            break;
    }
    return $return;
}

/**
 * generate css links (auto include model css files)
 * @access public
 * @static 	 
 * @param array $css file names
 * @param boolean $cash enable cash
 * @param boolean $justLinks echo links only 
 * @param boolean $outSource enable out source 
 * @param boolean $options options params in the link 
 * @return void
 */
function generateCSS(array $css, $cash = true, $justLinks = false, $outSource = false, $options = array()) {
    if (!empty($css)) {

        $propStr = getProperites($options);

        $controllerName = $GLOBALS ['registry']->controller;
        $actionName = $GLOBALS ['registry']->action;
        $cssControllerActionFile = CSS_PATH . $controllerName . DS . $actionName . '.css';
        if (file_exists($cssControllerActionFile)) { //Auto Load Control action css if found
            $cssControllerActionName = 'Controller-' . $controllerName . '-' . $actionName . '-Action' . (isset($langShort) ? '-' . $langShort : '');
            $css [] = $cssControllerActionName;
        }

        $cssControllerActionFileLang = CSS_PATH . $controllerName . DS . ($GLOBALS ['registry']->currentLang) . DS . $actionName . '.css';

        if (file_exists($cssControllerActionFileLang)) { //Auto Load Control action Lang css if found
            $cssControllerActionNameLang = 'Controller-' . $controllerName . '-' . $actionName . '-Action-' . $GLOBALS ['lang']->currentLang;
            $css [] = $cssControllerActionNameLang;
        }
        if ($cash) {
            $cssCashString = base64url_encode(implode('|', $css));
            if ($cssCashString != '')
                echo ($justLinks) ? mapFile2URL('css.php') . '?cash=' . $cssCashString : '<link rel="stylesheet" type="text/css" href="' . mapFile2URL('css.php') . '?cash=' . $cssCashString . '" ' . $propStr . ' />';
        } else {
            foreach ($css as $cssFile) { // Normal load without cash
                if (is_array($cssFile))
                    $cssFile = $cssFile [0] . DS . $cssFile [1] . '.css';
                elseif (!$outSource)
                    $cssFile = $cssFile . '.css';
                if (file_exists(CSS_PATH . $cssFile))
                    echo ($justLinks) ? mapFile2URL('css' . DS . $cssFile) : '<link rel="stylesheet" type="text/css" href="' . mapFile2URL('css' . DS . $cssFile) . '" ' . $propStr . '/>';
                elseif ($outSource)
                    echo ($justLinks) ? $cssFile : '<link rel="stylesheet" type="text/css" href="' . $cssFile . '" ' . $propStr . '/>';
            }

            if (!$GLOBALS['noAutomation']) {
                if (file_exists($cssControllerActionFile))
                    echo ($justLinks) ? mapFile2URL(CSS_PATH . $controllerName . DS . $actionName . '.css') : '<link rel="stylesheet" type="text/css" href="' . mapFile2URL(CSS_PATH . $controllerName . DS . $actionName . '.css') . '" ' . $propStr . '/>';
                if (file_exists($cssControllerActionFileLang))
                    echo ($justLinks) ? mapFile2URL(CSS_PATH . $controllerName . DS . ($GLOBALS ['registry']->currentLang) . DS . $actionName . '.css') : '<link rel="stylesheet" type="text/css" href="' . mapFile2URL(CSS_PATH . $controllerName . DS . ($GLOBALS ['registry']->currentLang) . DS . $actionName . '.css') . '" ' . $propStr . '/>';
            }
        }
    }
}

function deployClientFiles($js, $cash = true, $justLinks = false, $type = 'js') {
    if ($type == 'js')
        $invokeFunctionName = 'generateJS';
    else
        $invokeFunctionName = 'generateCSS';


    foreach ($js as $key => $element) {
        if (is_array($element)) {
            $GLOBALS['noAutomation'] = true;
            call_user_func_array($invokeFunctionName, array(@$element['links'], @$element['cash'], @$element['justLinks'], @$element['outSource'], @$element['options']));
        } else
            $elements[] = $element;
    }
    @$elements = (is_array($elements)) ? $elements : array($elements);
    $GLOBALS['noAutomation'] = false;
    call_user_func_array($invokeFunctionName, array($elements, $cash, $justLinks));
}

/**
 * generate js links
 * @access public
 * @static 	 
 * @param array $js file names
 * @param boolean $cash enable cash
 * @param boolean $justLinks echo links only
 * @param boolean $outSource enable out source 
 * @param boolean $options options params in the link
 * @return void
 */
function generateJS(array $js, $cash = true, $justLinks = false, $outSource = false, $options = array()) {

    if (!empty($js)) {
        $propStr = getProperites($options);


        if (!$GLOBALS['noAutomation']) {
            $controllerName = $GLOBALS ['registry']->controller;
            $actionName = $GLOBALS ['registry']->action;
            $jsControllerActionFile = JS_PATH . $controllerName . DS . $actionName . '.js';



            if (file_exists($jsControllerActionFile)) { //Auto Load Control action js  if found
                $jsControllerActionName = 'Controller-' . $controllerName . '-' . $actionName . '-Action';
                $js [] = $jsControllerActionName;
            }

            $jsControllerActionFileLang = JS_PATH . $controllerName . DS . ($GLOBALS ['registry']->currentLang) . DS . $actionName . '.js';
            if (file_exists($jsControllerActionFileLang)) { //Auto Load Control action js lang if found
                $jsControllerActionNameLang = 'Controller-' . $controllerName . '-' . $actionName . '-Action-' . $GLOBALS ['lang']->currentLang;
                $js [] = $jsControllerActionNameLang;
            }
        }


        if ($cash) {
            $jsCashString = base64url_encode(implode('|', $js));
            if ($jsCashString != '')
                echo ($justLinks) ? mapFile2URL('js.php') . '?cash=' . $jsCashString : '<script type="text/javascript"  src="' . mapFile2URL('js.php') . '?cash=' . $jsCashString . '" ' . $propStr . '></script>';
        } else {
            foreach ($js as $jsFile) { // Normal load without cash
                if (is_array($jsFile))
                    $jsFile = $jsFile [0] . DS . $jsFile [1] . '.js';
                elseif (!$outSource)
                    $jsFile = $jsFile . '.js';
                if (file_exists(JS_PATH . $jsFile))
                    echo ($justLinks) ? mapFile2URL('js' . DS . $jsFile) : '<script type="text/javascript"  src="' . mapFile2URL('js' . DS . $jsFile) . '" ' . $propStr . ' ></script>';

                elseif ($outSource)
                    echo ($justLinks) ? $jsFile : '<script type="text/javascript"  src="' . $jsFile . '" ' . $propStr . '></script>';
            }

            if (!$GLOBALS['noAutomation']) {
                if (file_exists($jsControllerActionFile))
                    echo ($justLinks) ? mapFile2URL(JS_PATH . $controllerName . DS . $actionName . '.js') : '<script type="text/javascript" ' . $propStr . '  src="' . mapFile2URL(JS_PATH . $controllerName . DS . $actionName . '.js') . '"></script>';
                if (file_exists($jsControllerActionFileLang))
                    echo ($justLinks) ? mapFile2URL(JS_PATH . $controllerName . DS . ($GLOBALS ['registry']->currentLang) . DS . $actionName . '.js') : '<script type="text/javascript" ' . $propStr . '  src="' . mapFile2URL(JS_PATH . $controllerName . DS . ($GLOBALS ['registry']->currentLang) . DS . $actionName . '.js') . '"></script>';
            }
        }
    }
}

function clientValidation() {
    if (is_object($GLOBALS ['registry']->validate))
        echo $GLOBALS ['registry']->validate->clientValidate();
}

/**
 * generate cash content
 * @access public
 * @static 	 
 * @param string type (files content type)	 
 * @param array files
 * @param boolean $archive enable archive files
 * @return void
 */
function cashFiles($fileType, array $files, $archive = false) {
    // get file last modified dates
    $aLastModifieds = array();

    foreach ($files as $sFile) {
        if (file_exists($sFile))
            $aLastModifieds [] = filemtime("$sFile");
    }
    // sort dates, newest first
    rsort($aLastModifieds);

    // output latest timestamp
    $iETag = (int) $aLastModifieds [0];
    $sLastModified = gmdate('D, d M Y H:i:s', $iETag) . ' GMT';

    // see if the user has an updated copy in browser cache
    if ((isset($_SERVER ['HTTP_IF_MODIFIED_SINCE']) && $_SERVER ['HTTP_IF_MODIFIED_SINCE'] == $sLastModified) || (isset($_SERVER ['HTTP_IF_NONE_MATCH']) && $_SERVER ['HTTP_IF_NONE_MATCH'] == $iETag)) {
        header($_SERVER ['SERVER_PROTOCOL'] . " 304 Not Modified");
        //header("filename=\"".$combressPath."\"; "); 	 		 
        exit();
    }

    // create a directory for storing current and archive versions


    if ($archive && !is_dir(CASH_ARCHIVE_FOLDER)) {
        mkdir(CASH_ARCHIVE_FOLDER);
    }

    // get code from archive folder if it exists, otherwise grab latest files, merge and save in archive folder
    if ($archive && file_exists(CASH_PATH . DS . "$iETag.cache")) {

        $sCode = file_get_contents(CASH_PATH . DS . "$iETag.cache");
    } else {
        // get and merge code
        $sCode = '';
        $aLastModifieds = array();
        foreach ($files as $sFile) {
            if (file_exists($sFile)) {

                $aLastModifieds [] = filemtime("$sFile");
                $sCode .= file_get_contents("$sFile");
            }
        }
        // sort dates, newest first
        rsort($aLastModifieds);

        if ($archive) {
            if ($iETag == $aLastModifieds [0]) { // check for valid etag, we don't want invalid requests to fill up archive folder
                $oFile = fopen(CASH_PATH . DS . "$iETag.cache", 'w');
                if (flock($oFile, LOCK_EX)) {
                    fwrite($oFile, $sCode);
                    flock($oFile, LOCK_UN);
                }
                fclose($oFile);
            } else {
                // archive file no longer exists or invalid etag specified
                header($_SERVER ['SERVER_PROTOCOL'] . " 404 Not Found");
                //header("filename=\"".$combressPath."\"; "); 	 		 
                exit();
            }
        }
    }

//	var_dump($sCode);
    cashContent($sCode, $fileType, $sLastModified, $iETag);
}

/*
 * redirect link
 * @param array $link details
 * @return void
 */

function redirectTo(array $link) {
    $url = getURL($link);
    if (headers_sent()) {
        echo "<script type=\"text/javascript\">document.location.href=\"" . $url . "\";</script>\n";
    } else {
        header("Location: " . $url);
    }
    exit();
}

function cashContent($content, $fileType, $lastModified, $iETag) {

    $combressContent = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);
    //$combressContent=$content;
    //$combressContent = str_replace ( array ("\r", "\n", "\t", '  ', '   ' ), '', $combressContent );	
    /*
      //Minfy JS Files
      if ($fileType == 'text/javascript')
      $combressContent = JSMin::minify ( $content );

      //Minfy CSS Files
      elseif ($fileType == 'text/css') {
      $combressContent = preg_replace ( '!/\*[^*]*\*+([^/][^*]*\*+)* /!', '', $content );
      $combressContent = str_replace ( array ("\r", "\n", "\t", '  ', '   ' ), '', $combressContent );
      }
      // send HTTP headers to ensure aggressive caching
      $maxCash = 10000;
     */
    //$sizeOFComabine = strlen ( $combressContent );
    //var_dump($combressContent );
    // $numberOfContents=ceil($sizeOFComabine/90000);






    header('Expires: ' . gmdate('D, d M Y H:i:s', strtotime('100 days', time())) . ' GMT'); // 1 year from now
    header('Content-Type: ' . $fileType);
    //	header ( 'Content-Length: ' . $sizeOFComabine );
    header("Last-Modified: $lastModified");
    header("ETag: $iETag");
    header('Cache-Control: max-age=' . CACHE_FILES_LENGTH);
    echo $combressContent;
    exit();

    /* 	
      if (preg_match('|MSIE ([0-8].[0-8]{1,2})|',$useragent,$matched)) {
      $browser_version=$matched[1];
      $browser = �IE�;


      echo $combressContent;
      exit();

      }
      if ($sizeOFComabine > $maxCash) {
      header ( 'Content-Type: ' . $fileType );
      header ( 'Expires: ' . gmdate ( 'D, d M Y H:i:s', time () + strtotime(CACHE_FILES_LENGTH, time()) ) . ' GMT' ); // 1 year from now
      header ( 'Cache-Control: max-age=' . CACHE_FILES_LENGTH );

      // Enable compression
      if (extension_loaded ( 'zilb' )) {
      ini_set ( 'zlib.output_compression', 'On' );
      }
      header ( "Last-Modified: $lastModified" );
      header ( "ETag: $iETag" );

      echo $combressContent;
      } else {
      ob_start ();
      header ( 'Expires: ' . gmdate ( 'D, d M Y H:i:s', time () + CACHE_FILES_LENGTH ) . ' GMT' ); // 1 year from now
      header ( 'Content-Type: ' . $fileType );
      //	header ( 'Content-Length: ' . $sizeOFComabine );
      header ( "Last-Modified: $lastModified" );
      header ( "ETag: $iETag" );
      header ( 'Cache-Control: max-age=' . CACHE_FILES_LENGTH );
      echo $combressContent;
      }
      exit ();
     */
}

/*
 * get properties from array and draw it in inputs
 * @param array properties
 * @return string html properties
 */

function getProperites($prop) {
    $propStr = '';
    if (!empty($prop) and is_array($prop))
        foreach ($prop as $key => $val)
            $propStr .= ' ' . $key . '="' . $val . '" ';
    return $propStr;
}

/*
 * format link 
 * @param mixed value
 * @return mixed  formated value
 */

function formatGetValue($value) {

    $value = trim($value);
    if (is_string($value))
        $value = str_replace(' ', '-', $value);
    return $value;
}
