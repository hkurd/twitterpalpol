<?php

/**
 * AppView, Tidy PHP 
 * Templete Calss set and get data through the view
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class View {

    /**
     * vars stored in the view
     * @access private
     * @var array
     */
    private $vars = array();

    /**
     * constructor 
     * @param registry object
     * @access public
     * @return void
     */
    function __construct() {
        
    }

    /**
     * set variables through views
     * @param string $index
     * @param mixed $value
     * @return void
     */
    public function __set($index, $value) {
        $this->vars [$index] = $value;
    }

    /**
     * get the set variables
     * @return array
     */
    function getViewVariables() {
        return $this->vars;
    }

}
