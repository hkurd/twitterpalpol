<?php

/**
 * Auth, Tidy PHP 
 * Enable Authentication and Authorization
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;


class AuthSetting {

    public $loginOption = array(); //autoLogin,maxLoginTime,sessionLength,loginRestTime,maxLoginAttempts 
    public $cookieOptions = array(); //name,path,domain,secure
    public $pageID;
    public $currentTime;
    public $userID;
    public $userIP;
    public $enableAuth = false;
    public $userObject;
    public $loginAction = array();
    public $redirectAction = array();
    public $allow = array();

    public function __construct($loginOption, $cookieOptions, $pageID, $userID, $userIP) {

        $this->currentTime = time();
        $this->loginOption = $loginOption;
        $this->cookieOptions = $cookieOptions;
        $this->pageID = $pageID;
        $this->userIP = $userIP;
        $this->userID = $userID;
    }

}

//whither the user is true or not
class Authentication {

    public $errorMsgKeys = array();
    private $sessionData;
    private $sessionID;
    private $authSetting;
    public $sessionUser;
    public $loginStatus;
    public $lastVisit;

    public function __construct(authSetting $authSetting) {
        $this->sessionUser = new Model ();
        $this->authSetting = $authSetting;
    }

    public function errorHandle($msgKey) {
        $this->errorMsgKeys [] = $msgKey;
        //$this->resetSessionKeys();	
        $this->endSession();
    }

    public function sessionUserBegin() {
        $this->storgeCheck(); //get info from storge if avaliable
        $this->authSetting->userID = ($this->authSetting->userID != ANONYMOUS) ? $this->authSetting->userID : $this->authSetting->userObject->User_ID;
        if (isset($this->authSetting->loginOption ['autoLogin']) && !$this->authSetting->loginOption ['autoLogin']) { //check auto login option
            $this->sessionData ['autologinID'] = false;
        }
        $user = array();

        if ($this->authSetting->userID != ANONYMOUS) { // known user 
            if (isset($this->sessionData ['autologinID']) && (string) $this->sessionData ['autologinID'] != '' && $this->authSetting->userID) {

                $this->authSetting->userObject = $GLOBALS ['registry']->db->getRecord('users', 'u.*', USERS_TABLE . ' u,' . SESSIONS_KEYS_TABLE . ' k', 'AND u.User_Status = ' . ACTIVE_STATUS . ' AND k.User_ID = u.User_ID AND k.key_ID = \'' . md5($this->sessionData ['autologinID']) . '\''); // avaliable and active user on database


                $this->loginStatus = LOGIN_AUTO_STATUS;
            } else {
                $this->sessionData ['autologinID'] = '';
                $this->sessionData ['userID'] = $this->authSetting->userID;

                // get the user
                if (!is_object($this->authSetting->userObject))
                    $this->authSetting->userObject = $GLOBALS ['registry']->db->getRecord('users', 'u.*', USERS_TABLE . ' u', ' AND u.User_Status = ' . ACTIVE_STATUS . ' AND u.User_ID = ' . $this->authSetting->userID);

                $this->loginStatus = LOGIN_AUTO_CREATE_STATUS;
            }
        }

        if (!is_object($this->authSetting->userObject)) { //unknown user from storge
            $this->sessionData ['autologinID'] = '';
            $this->sessionData ['userID'] = $this->authSetting->userID = ANONYMOUS;
            $this->loginStatus = LOGIN_WRONG_STATUS;

            //	$userObject = $GLOBALS['registry']->db->getRecord ( 'users', 'u.*', USERS_TABLE . ' u', 'AND u.User_ID = ' . $this->authSetting->userID); // get the user
            $this->errorHandle(ERR_NO_EXISTS_USER_LOGIN_KEY);
        }
        //save in the session table
        if ($this->sessionID != null)
            $sessionsModel = $GLOBALS ['registry']->db->getRecord('sessions', SESSIONS_TABLE . '.Session_ID', SESSIONS_TABLE, ' AND Session_ID=\'' . $this->sessionID . '\'');
        //AND Session_IP =\''. $this->authSetting->userIP.'\'	
        if (is_object($sessionsModel)) {
            $sessionsModel->User_ID = $this->authSetting->userID;
            $sessionsModel->Session_Start = $this->authSetting->currentTime;
            $sessionsModel->Session_Time = $this->authSetting->currentTime;
            $sessionsModel->Session_Page = $this->authSetting->pageID;
            $sessionsModel->Session_Logged_In = $this->loginStatus;
            $sessionsModel->Session_Admin = $this->authSetting->loginOption ['admin'];

            $sessionsModel->setCondition(array('Session_ID' => $this->sessionID, 'Session_IP' => $this->authSetting->userIP));

            $GLOBALS ['registry']->db->editStmtPrep($sessionsModel, true);
        } else {
            $sessionsModel = new sessionsModel ();
            $sessionsModel->User_ID = $this->authSetting->userID;
            $sessionsModel->Session_Start = $this->authSetting->currentTime;
            $sessionsModel->Session_Time = $this->authSetting->currentTime;
            $sessionsModel->Session_Page = $this->authSetting->pageID;
            $sessionsModel->Session_Logged_In = $this->loginStatus;
            $sessionsModel->Session_IP = $this->authSetting->userIP;
            $sessionsModel->Session_Admin = $this->authSetting->loginOption ['admin'];
            $this->sessionID = $sessionsModel->Session_ID = md5($GLOBALS ['registry']->session->getId());
            $GLOBALS ['registry']->db->addStmtPrep($sessionsModel, true);
        }

        if ($this->authSetting->userID != ANONYMOUS) {

            // save the last visit in the users table
            $this->lastVisit = ($userObject->User_Session_Time > 0) ? $userObject->User_Session_Time : $this->authSetting->currentTime;
            if (!$this->authSetting->loginOption ['admin']) {
                $usersModel = new usersModel ();

                $usersModel->User_Session_Time = $this->authSetting->currentTime;
                $usersModel->User_Session_Page = $this->authSetting->pageID;
                $usersModel->User_LastVisit = $this->lastVisit;

                $usersModel->setCondition(array('User_ID' => $this->authSetting->userID));

                if (!$GLOBALS ['registry']->db->editStmtPrep($usersModel, true) > 0) {
                    $this->errorHandle(ERR_UPDATE_VISIT_TIME_LOGIN_KEY);
                }
            }
            $usersModel->User_LastVisit = $this->lastVisit;

            if ($this->authSetting->loginOption ['autoLogin']) {
                $autologinID = uniqid();

                //exists  auto login key
                if (isset($this->sessionData ['autologinID']) && (string) $this->sessionData ['autologinID'] != '') {
                    $sql = 'UPDATE ' . SESSIONS_KEYS_TABLE . ' SET ' . SESSIONS_KEYS_TABLE . ' .Key_Last_Login=' . $this->authSetting->currentTime . ',' . SESSIONS_KEYS_TABLE . ' .Key_ID=\'' . md5($autologinID) . '\' WHERE ' . SESSIONS_KEYS_TABLE . ' .Key_ID = \'' . md5($this->sessionData ['autologinID']) . '\'';

                    if (!$GLOBALS ['registry']->db->query($sql)) {
                        $this->errorHandle(ERR_UPDATE_SESSION_KEY_LOGIN_KEY);
                    }
                } else {
                    //create  auto login key
                    $sessionsKeyModel = new sessionsKeyModel ();
                    $sessionsKeyModel->Key_Last_Login = $this->authSetting->currentTime;
                    $sessionsKeyModel->Key_Last_IP = $this->authSetting->userIP;
                    $sessionsKeyModel->User_ID = $this->authSetting->userID;
                    $sessionsKeyModel->Key_ID = md5($autologinID); //still	
                    //	die(var_dump($sessionsKeyModel));
                    if (!$GLOBALS ['registry']->db->addStmtPrep($sessionsKeyModel, true) > 0) {
                        $this->errorHandle(ERR_UPDATE_SESSION_KEY_LOGIN_KEY);
                    }
                }
                $this->sessionData ['autologinID'] = $autologinID;
            } else {
                $this->sessionData ['autologinID'] = '';
            }
            //		$sessiondata['autologinID'] = (!$admin) ? (( $enable_autologin && $sessionmethod == SESSION_METHOD_COOKIE ) ? $auto_login_key : '') : $sessiondata['autologinID'];
            $this->sessionData ['userID'] = $this->authSetting->userID;
        }
        //set the global object
        $this->setSessionUser();
        //save the session
        $this->saveSession();
        //get the global object
        return $this->sessionUser;
    }

    public function sessionPageStart($pageID, $currentIP) {

        $this->storgeCheck();
        $this->authSetting->pageID = $pageID;

        if ($this->sessionID != null) {
            //$GLOBALS['registry']->db->setDebug(DEBUG_FULL_MODE);


            $this->authSetting->userObject = $GLOBALS ['registry']->db->getRecord('users', 'u.*', USERS_TABLE . ' u,' . SESSIONS_TABLE . ' s', 'AND u.User_Status = ' . ACTIVE_STATUS . ' AND s.User_ID = u.User_ID AND s.Session_ID = \'' . $this->sessionID . '\'');
            if (!is_object($this->authSetting->userObject)) {

                $this->errorHandle(ERR_NO_EXISTS_USER_LOGIN_KEY);
            } else {

                $sessionIPCheck = substr($this->sessionUser->sessionIP, 0, 6);
                $currentIPCheck = substr($currentIP, 0, 6);
                if ($sessionIPCheck == $currentIPCheck) { //check if the ip login == ip in the session
                    //	$SID = ($sessionmethod == SESSION_METHOD_GET || defined('IN_ADMIN')) ? $session_id : '';
                    if ($this->authSetting->currentTime - $userObject->User_Session_Time > 60) {

                        $sessionsModel = new sessionsModel ();
                        $sessionsModel->Session_Time = $this->authSetting->currentTime;
                        $sessionsModel->Session_Page = $this->authSetting->pageID;

                        $sessionsModel->setCondition(array('Session_ID' => $this->sessionID));

                        //$update_admin = (!defined('IN_ADMIN') && $current_time - $user['session_time'] > ($config['session_length']+60)) ? ', session_admin = 0' : '';
                        if (!$GLOBALS ['registry']->db->editStmtPrep($sessionsModel, true) > 0) {
                            $this->errorHandle(ERR_UPDATE_SESSION_LOGIN_KEY);
                        }

                        if ($this->authSetting->userID != ANONYMOUS) {

                            $usersModel = new usersModel ();

                            $usersModel->User_Session_Time = $this->authSetting->currentTime;
                            $usersModel->User_Session_Page = $this->authSetting->pageID;
                            $usersModel->setCondition(array('user_ID' => $this->authSetting->userID));

                            if (!$GLOBALS ['registry']->db->editStmtPrep($usersModel, true) > 0) {
                                $this->errorHandle(ERR_UPDATE_USER_LOGIN_KEY);
                            }
                        }
                        $this->cleanSession();
                        $this->saveSession();
                    }

                    if (isset($this->sessionData ['autologinID']) && $this->sessionData ['autologinID'] != '') {
                        $this->sessionUser->sessionKey = $this->sessionData ['autologinID'];
                    }
                    return $this->sessionUser;
                }
            }
            $this->authSetting->userID = (isset($this->sessionData ['userID'])) ? intval($this->sessionData ['userID']) : ANONYMOUS;
            if (!($this->sessionUser = $this->sessionUserBegin())) {
                $this->errorHandle(ERR_CREATE_USER_SESSION_KEY);
            }
        }

        return $this->sessionUser;
    }

    private function storgeCheck() {

        if (isset($_COOKIE [$this->authSetting->cookieOptions ['cookieName'] . '_sid']) || isset($_COOKIE [$this->authSetting->cookieOptions ['cookieName'] . '_data'])) {

            $this->sessionID = isset($_COOKIE [$this->authSetting->cookieOptions ['cookieName'] . '_sid']) ? $_COOKIE [$this->authSetting->cookieOptions ['cookieName'] . '_sid'] : '';
            $this->sessionData = isset($_COOKIE [$this->authSetting->cookieOptions ['cookieName'] . '_data']) ? unserialize(stripslashes($_COOKIE [$this->authSetting->cookieOptions ['cookieName'] . '_data'])) : array();
        }
        /* 		if (! preg_match ( '/^[A-Za-z0-9]*$/', $this->sessionID )) {
          $this->sessionID = '';
          } */
    }

    private function saveSession() {

        setcookie($this->authSetting->cookieOptions ['cookieName'] . '_sid', serialize($this->sessionData), time() + 31536000, $this->authSetting->cookieOptions ['cookiePath'], $this->authSetting->cookieOptions ['domain'], $this->authSetting->cookieOptions ['secure']);
        setcookie($this->authSetting->cookieOptions ['cookieName'] . '_sid', $this->sessionID, time() + 31536000, $this->authSetting->cookieOptions ['cookiePath'], $this->authSetting->cookieOptions ['domain'], $this->authSetting->cookieOptions ['secure']);
    }

    public function removeSession() {

        setcookie($this->authSetting->cookieOptions ['cookieName'] . '_data', '', time() - 31536000, $this->authSetting->cookieOptions ['cookiePath'], $this->authSetting->cookieOptions ['domain'], $this->authSetting->cookieOptions ['secure'])

        ;
        setcookie($this->authSetting->cookieOptions ['cookieName'] . '_sid', '', time() - 31536000, $this->authSetting->cookieOptions ['cookiePath'], $this->authSetting->cookieOptions ['domain'], $this->authSetting->cookieOptions ['secure'])

        ;
        unset($_COOKIE [$this->authSetting->cookieOptions ['cookieName'] . '_data']);
        unset($_COOKIE [$this->authSetting->cookieOptions ['cookieName'] . '_sid']);
    }

    private function setSessionUser() {

        $this->sessionUser->sessionID = $this->sessionID;
        $this->sessionUser->sessionIP = $this->authSetting->userIP;
        $this->sessionUser->sessionUserID = $this->authSetting->userID;
        $this->sessionUser->sessionLoggedIn = $this->loginStatus;
        $this->sessionUser->sessionPage = $this->authSetting->pageID;
        $this->sessionUser->sessionStart = $this->authSetting->currentTime;
        $this->sessionUser->sessionTime = $this->authSetting->currentTime;
        $this->sessionUser->sessionAdmin = $this->authSetting->loginOption ['admin'];
        $this->sessionUser->sessionKey = $this->sessionData ['autologinID'];
        $this->sessionUser->userObject = $this->authSetting->userObject;

        return $this->sessionUser;
    }

    private function cleanSession() {

        $sql = '
		DELETE FROM ' . SESSIONS_TABLE . ' 
		WHERE
		Session_Time < ' . (time() - (int) $this->authSetting->loginOption ['sessionLength']) . " 
			AND Session_ID <> '" . $this->sessionID . "'
	";
        if (!$GLOBALS ['registry']->db->query($sql)) {

            $this->errorHandle(ERR_CLEAN_SESSION_LOGIN_KEY);
        }

        if (!empty($this->authSetting->loginOption ['maxLoginTime']) && $this->authSetting->loginOption ['maxLoginTime'] > 0) {
            $sql = '
			DELETE FROM ' . SESSIONS_KEYS_TABLE . '
			WHERE
				Key_Last_Login < ' . (time() - (86400 * (int) $this->authSetting->loginOption ['maxLoginTime']));
            $GLOBALS ['registry']->db->query($sql);
        }
        return true;
    }

    public function endSession() {
        if (!preg_match('/^[A-Za-z0-9]*$/', $this->sessionID)) {
            return;
        }
        $userID = ($this->authSetting->userID != ANONYMOUS) ? $this->authSetting->userID : $this->authSetting->userObject->User_ID;
        $sqlExeptionSessionTable = 'truncate ' . SESSIONS_TABLE;
        $sqlExeptionSessionKeyTable = 'truncate ' . SESSIONS_KEYS_TABLE;
        //var_dump($userID);
        if ($userID != NULL) {
            $sqlSession = '
		DELETE FROM ' . SESSIONS_TABLE . ' 
		WHERE 1=1
			AND Session_ID = "' . $this->sessionID . '" AND User_ID=' . $userID;

            if (!$GLOBALS ['registry']->db->query($sqlSession)) {
                $this->errorHandle(ERR_REMOVE_SESSION_USER_LOGIN_KEY);
            }

            if (isset($this->sessionUser->sessionKey) && $this->sessionUser->sessionKey != '') {
                $autoLoginKey = md5($this->sessionUser->sessionKey);
                $sql = '
			DELETE FROM ' . SESSIONS_KEYS_TABLE . '
			WHERE 1=1
				user_id    = ' . (int) $userID . "
				AND Key_ID = '$autoLoginKey'
		";
                if (!$GLOBALS ['registry']->db->query($sql)) {
                    $this->errorHandle(ERR_REMOVE_SESSION_USER_LOGIN_KEY);
                }
            }
        }

        $this->removeSession();
        return true;
    }

    public function resetSessionKeys() {

        $userID = ($this->authSetting->userID == ANONYMOUS) ? $this->authSetting->userID : $this->authSetting->userObject->User_ID;

        $whereCondKeyID = (!empty($this->sessionUser->sessionKey)) ? "AND Key_ID != '" . md5($this->sessionUser->sessionKey) . "'" : '';

        $sql = '
				DELETE FROM ' . SESSIONS_KEYS_TABLE . '
				WHERE
					User_ID = ' . (int) $userID . "
					$whereCondKeyID
			";
        if (!$GLOBALS ['registry']->db->query($sql)) {
            $this->errorHandle('Error rest  key');

            //die('Error removing auto-login keys');
        }

        $whereCondSessionTable = 'User_ID = ' . (int) $this->authSetting->userID;
        $whereCondSessionTable .= ($this->authSetting->userID = $this->sessionUser->sessionUserID) ? " AND Session_ID <> '" . $this->sessionUser->sessionID . "'" : '';
        $sql = '
		DELETE FROM ' . SESSIONS_TABLE . "
		WHERE
			$whereCondSessionTable
	";
        if (!$GLOBALS ['registry']->db->query($sql)) {
            $this->errorHandle(ERR_REMOVE_SESSION_USER_LOGIN_KEY);
        }
        if (!empty($whereCondKeyID)) {
            $autologinID = uniqid(microtime());

            $sessionsKeyModel = new sessionsKeyModel ();
            $sessionsKeyModel->Key_Last_IP = $this->authSetting->currentTime;
            $sessionsKeyModel->Key_ID = md5($autologinID);
            $sessionsKeyModel->Key_Last_Login = $this->authSetting->currentTime;
            $sessionsKeyModel->setCondition(array('Key_ID' => md5($this->sessionUser->sessionUserID)));

            $sql = '
			UPDATE ' . SESSIONS_KEYS_TABLE . "
			SET
				Key_Last_IP = '$user_ip', Key_ID = '" . md5($autologinID) . "', Key_Last_Login = $current_time
			WHERE
				Key_ID = '" . md5($this->sessionUser->sessionUserID) . "'
		";

            if (!$GLOBALS ['registry']->db->editStmtPrep('users', true) > 0) {
                $this->errorHandle(ERR_UPDATE_SESSION_KEY_LOGIN_KEY_MSG);

                //die('Error updating session key');
            }

            $this->sessionData ['userID'] = $this->authSetting->userID;
            $this->sessionData ['autologinID'] = $this->sessionUser->sessionKey = $autologinID;
            $this->saveSession();
        }
    }

}

//wither the user can enter the page or not	
class Authrization {
    
}

?>