<?php

/*
 * rotuerMVC, Tidy PHP 
 * Router class ini the controllers and views
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */

class RouterMVC extends Router {

    /**
     * load MVC actions and view
     * @param array layouts that is rendered without router
     */
    public function loader($renderedLayouts = array()) {
        $this->loaderCommon();
        // define run actions
        $beforeAction = 'before' . ucfirst($this->_mainActionFuncName);
        $afterAction = 'after' . ucfirst($this->_mainActionFuncName);
        $afterActionPageLoad = 'after' . ucfirst($this->_mainActionFuncName) . 'PageLoad';


        if ($this->_controllerObject->authSetting->enableAuth) {
            if (!in_array($this->controller . ':' . $this->action, $this->_controllerObject->authSetting->allow)) {
                if ($this->_controllerObject->verifyAccess()) {
                    // if (!is_object($GLOBALS ['registry']->userSessionObject)){
                    if ($GLOBALS ['registry']->request->getCookie('redirectURL') == '' and $this->controller != $this->_controllerObject->authSetting->loginAction ['controller'] and $this->action != $this->_controllerObject->authSetting->loginAction ['action'])
                        $GLOBALS ['registry']->response->setCookie('redirectURL', $_SERVER ['REQUEST_URI']);
                    redirectTo($this->_controllerObject->authSetting->loginAction);
                }
                // }
            }
        }

        /**
         * * check if the beforeLoad is callable **
         */
        if (is_callable(array(
                    $this->_controllerObject,
                    'beforeLoad'
                )))
            $this->_controllerObject->beforeLoad();
        /**
         * * check if the beforeAction is callable **
         */
        if (is_callable(array(
                    $this->_controllerObject,
                    $beforeAction
                )))
            $this->_controllerObject->$beforeAction();
        /**
         * get Model and set the validation
         */
        if (is_array($this->_controllerObject->validateActions))
            if (in_array($this->action, $this->_controllerObject->validateActions)) {
                $GLOBALS ['registry']->request->requestId = md5($this->controller . $this->action);
                $GLOBALS ['registry']->validate->setValdationArray($this->_controllerObject->validateRules);
                $GLOBALS ['registry']->validate->writeJsFileValidation();

                if (sizeof($GLOBALS ['registry']->data) > 0) {
                    $validation = $GLOBALS ['registry']->validate->serverValidate();
                    if (is_array($validation) and !empty($validation)) { // validation error
                        $GLOBALS ['registry']->msg->flushMessages($validation);
                        $this->_controllerObject->validate = false;
                    } else
                        $this->_controllerObject->validate = true;
                }
            }

        /**
         * * check if the action is callable **
         */
        if (is_callable(array(
                    $this->_controllerObject,
                    $this->action
                )) == false) {
            $action = DEFAULT_ACTION;
        } else {
            $action = $this->action;
        }

        $this->_controllerObject->view = $this->controller . DS . $action;


        /**
         * * run the action **
         */
        call_user_func_array(array(
            $this->_controllerObject,
            $action
                ), $this->_args);

        /**
         * * check if the afterLoad is callable **
         */
        if (is_callable(array(
                    $this->_controllerObject,
                    'afterLoad'
                )))
            $this->_controllerObject->afterLoad();

        /**
         * * check if the afterAction is callable **
         */
        if (is_callable(array(
                    $this->_controllerObject,
                    $afterAction
                )))
            $this->_controllerObject->$afterAction();

        App::$controller = $this->_controllerObject;
        $GLOBALS['currentController'] = App::$controller;
        $GLOBALS['currentModel'] = $GLOBALS['currentController']->model;
        if (!in_array($this->_controllerObject->layout, $renderedLayouts)){ // is not in manual rendered layouts
            //check if the request is ajax
            if (!empty($_SERVER ['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER ['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $this->_controllerObject->render($this->_controllerObject->view);
            }
            else{ // not ajax request so lets load auto route layout
                 App::import($this->_controllerObject->layout, 'layout');            
            }
        }
             
        /**
         * * check if the afterLoad is callable **
         */
        if (is_callable(array(
                    $this->_controllerObject,
                    'afterPageLoad'
                )))
            $this->_controllerObject->afterPageLoad();

        /**
         * * check if the afterLoad is callable **
         */
        if (is_callable(array(
                    $this->_controllerObject,
                    $afterActionPageLoad
                )))
            $this->_controllerObject->$afterActionPageLoad();
    }

}
