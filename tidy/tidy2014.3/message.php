<?php

/**
 * AppMessage, Tidy PHP 
 * show messages through views
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class Message {

    /**
     * messages
     * @access public
     * @var object
     */
    public $messages;

    function __construct() {
        
    }

    /**
     * get messages size 
     * @access public
     * @return size of messages
     * */
    public function getMessagesSize() {
        return sizeof($this->messages);
    }

    /**
     * set messages in session and log
     * @param array $msgs
     * @access public
     * @return size of messages
     * */
    public function flushMessages($msgs) {
        if (is_array($msgs) and !empty($msgs))
            foreach ($msgs as $msg)
                $this->setFlush();
    }

    /**
     * set messagess directly msgs key message & type
     * @param array $msgs
     * @access public
     * @return size of messages
     * */
    public function setMessages($msgs) {
        if (is_array($msgs) and !empty($msgs))
            foreach ($msgs as $msg)
                $this->setMsg($msg['message'], $msg['type']);
    }

    /**
     * set set message into messages array  key message & type
     * @param string $msgs
     * @param int $type 
     * @access public
     * @return array message & type
     * */
    public function setMsg($msg, $type, $flushAndLogUpdate = false) {
        $this->messages[] = array('message' => $msg, 'type' => $type);
        if ($flushAndLogUpdate)
            $this->updateLogMessageWithSession($msg, $type);
        return array('message' => $msg, 'type' => $type);
    }

    /**
     * get all messages
     * @param int filter messages on type
     * @access public
     * @return array of messsages
     * */
    public function getAllMessages($filter = null) {
        if ($filter != NULL) {
            if (is_array($this->messages))
                return array_filter($this->messages, function ($element) use ($filter) {
                    return ($element['type'] == $filter);
                });
        }
        return $this->messages;
    }

    /**
     * get all messages as string
     * @param int filter messages on type
     * @access public
     * @return array of messsages
     * */
    public function getStringAllMessages($filter = null) {
        $msgs = $this->getAllMessages($filter);
        $stringMesages = '';
        if (sizeof($msgs) > 0)
            foreach ($msgs as $msg) {
                $stringMesages.=$msg['message'] . "\n";
            }
        return $stringMesages;
    }

    /**
     * clear messages with toolbar session
     * @access public
     * @return void 
     * */
    public function clearMessagesSession() {
        $this->messages = array();
        $GLOBALS ['registry']->session->destroySessionKey('toolbarObMessages');
    }

    /**
     * clear messages
     * @access public
     * @return void
     * */
    public function clearMessages() {
        $this->messages = array();
    }

    /**
     * set messages in session
     * @access public
     * @return void
     * */
    public function setFlush() {
        $messages = $this->getAllMessages();
        if (sizeof($messages) > 0)
            $GLOBALS ['registry']->session->setSession('toolbarObMessages', $this->getAllMessages());
    }

    /**
     * flush the messages in the view
     * @access public
     * @return void
     * */
    public function flush() {

        $toolbarObMessages = $GLOBALS ['registry']->session->getSession('toolbarObMessages');
        $msgString = '';
        if (!empty($toolbarObMessages)) {

            $msgString .= '<script type="text/javascript" >';
            foreach ($toolbarObMessages as $msg) {
                //The way to show the messages
                $msgString .= 'notify("' . $msg ['message'] . '",{messageType:"' . $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [$msg ['type']] . '"});';
            }
            $msgString .= '</script>';



            //$this->clear();	
        }
        $GLOBALS ['registry']->session->destroySessionKey('toolbarObMessages');
        return $msgString;
    }

    /**
     * for datatabase debug messages
     * @access public
     * @return void
     * */
    public function setDebugDB($rule, $mood, $str, $inline = false) {
        /* 	$this->msgStack->clearFilters (); */

        if ($GLOBALS ['registry']->aclDatabaseDebug->isAllowed($rule, null, TIDY_CONSTANTS::DEBUG_QUERY_MODE) and $mood == TIDY_CONSTANTS::DEBUG_QUERY_MODE) {
            $msgString = '> <strong>SQL Statment</strong> :<br />' . $str . '<br />';
            ($inline) ? $this->showDebug($msgString) : $this->m($msgString, TIDY_CONSTANTS::LOG_DEBUG_QUERY);
        }

        if ($GLOBALS ['registry']->aclDatabaseDebug->isAllowed($rule, null, TIDY_CONSTANTS::DEBUG_VALUE_MODE) and $mood == TIDY_CONSTANTS::DEBUG_VALUE_MODE) {
            $msgString = '> <strong>Values</strong> ---------------------------------------------------' . $str . '<br />';
            ($inline) ? $this->showDebug($msgString) : $this->m($msgString, TIDY_CONSTANTS::LOG_DEBUG_QUERY);
        }
        if ($GLOBALS ['registry']->aclDatabaseDebug->isAllowed($rule, null, TIDY_CONSTANTS::DEBUG_RETURN_MODE) and $mood == TIDY_CONSTANTS::DEBUG_RETURN_MODE) {
            $msgString = '> <strong>Return</strong> ---------------------------------------------------<br />' . print_r($str, TRUE);
            ($inline) ? $this->showDebug($msgString) : $this->m($msgString, TIDY_CONSTANTS::LOG_DEBUG_QUERY);
        }
    }

    /**
     * show all debug messages
     * @access public
     * @return void
     * */
    public function debug() {

        $debugMessages = $this->getAllMessages(TIDY_CONSTANTS::LOG_DEBUG);
        foreach ($debugMessages as $msg) {
            $this->showDebug($msg ['message']);
        }

        //		return $debugString;
    }

    /**
     * print the debug messages
     * @access private
     * @return void
     * */
    private function showDebug($str) {

        echo '<hr>' . $str . '<br />';
    }

    /**
     * write usel log info (ip , request , time)
     * @param string encoded $userIP
     * @param int log type
     * @access public
     * @return void
     * */
    public function writeUserLog($userIP, $logType = TIDY_CONSTANTS::LOG_INFO) {

        $reqInfo = $GLOBALS['registry']->request->getRequstInfo();
        $GLOBALS['registry']->logger['user']->log('IP (' . $userIP . '),URI(' . $reqInfo['REQUEST_URI'] . '),Method(' . $reqInfo['REQUEST_METHOD'] . ')', $logType);
    }

}
