<?php

/**
 * AppLang, Tidy PHP 
 * save the system labels and messages acccording to the requsts
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class Translate {

    /**
     * messages array
     * @access private
     * @var array
     */
    private $_message = array();

    /**
     * current Number of messages
     * @access private
     * @var integer
     */
    private $_currentNumber;

    /**
     * the current language short
     * @access public
     * @var integer
     */
    public $currentLang;

    /**
     * the current language key
     * @access public
     * @var integer
     */
    public $currentLangKey;

    /**
     * store the application request registry object
     * @access private
     * @static
     * @var object
     */
    private static $_isCreated;

    /**
     * translation object
     * @static	 
     * @var object 
     */
    private $_translate;

    /**
     * object Translate
     * @access private 
     * @static	 
     * @var object 
     */
    private static $_translateObject;

    /**
     * get the object app language and create it if is not found
     * @return object applang
     */
    public static function createLangOB($local) {
        if (FALSE == self::$_isCreated) {
            if (NULL == self::$_translateObject) {
                self::$_translateObject = new Translate($local);
                /* // in your bootstrap file
                  $locale = new Zend_Locale();
                  Zend_Registry::set('Zend_Locale', $locale);

                  // default language when requested language is not available
                  $defaultlanguage = 'en';

                  // somewhere in your application
                  $translate = new Zend_Translate(
                  array('adapter' => 'gettext', 'content' => 'my_de.mo')
                  );

                  if (!$translate->isAvailable($locale->getLanguage())) {
                  // not available languages are rerouted to another language
                  $translate->setLocale($defaultlanguage);
                  }

                  $translate->getLocale(); */
            }
            self::$_isCreated = TRUE;
            return self::$_translateObject;
        } else {
            return self::$_translateObject;
        }
    }

    private function __construct() {
        
    }

    /**
     * translate string
     * @param string $string
     * @param int $msgType optional for messages	 
     * @return string of translaton or string key for messages
     */
    public function __($string, $msgType = NULL) {
        $translation = $this->_translate->_($string);
        if ($msgType != NULL) {
            $this->msg->m($translation, $msgType);
        }
        return $translation;
    }

    /**
     * set Local
     * @param string $key	 
     * @return array 
     */
    public function setLocal($local) {
        $this->_translate->setLocale($local);
    }

    /**
     * get message
     * @param string $key	 
     * @return array 
     */
    public function getMessage($msg) {
        return $this->_message [md5($msg)];
    }

    /**
     * get all labels
     * @return array
     */
    public function getAllLabels() {
        return $this->_translate->getMessages();
    }

    /**
     * get all messagess
     * @return array
     */
    public function getAllMessages() {
        return $this->_message;
    }

    /* 	
      /**
     * put messages in the session
     * @access public
     * @return void
     * 		
      public function setFlush() {
      $GLOBALS['registry']->session->setSession ( 'toolbarObMessages', $this->_message  );
      } */
}

?>