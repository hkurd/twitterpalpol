<?php

/**
 * requestFunctions, Tidy PHP 
 * functions needed for load in implement the request
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
function getURL(array $link) {
    $url = $formatParams = '';
    $controller = $link ['controller'];
    $action = $link ['action'];
    $mainURL = (array_key_exists('mainURL', $link) && $link ['mainURL'] != '') ? $link ['mainURL'] : HTTP_HOST_PATH;
    $params = '';
    if (array_key_exists('params', $link))
        if (is_array($link ['params'])) {
            foreach ($link ['params'] as $key => $value) {
                if (is_string($key))
                    $link ['params'] [$key] = $key . GET_PARAM_PATTERN . $value;
            }
            $params = implode($link ['params'], '/');
        }
    if (is_array(@$GLOBALS ['routerMapping']) and !@empty($GLOBALS ['routerMapping']))
        foreach ($GLOBALS ['routerMapping'] as $pattern => $linkArr) {
            if ($linkArr ['controller'] == $link ['controller'] and $linkArr ['action'] == $link ['action']) {
                //formating URL
                if ($linkArr ['format'] != '' and array_key_exists('params', $link))
                    if (preg_match_all("/\{[0-9]\}/", $linkArr['format'], $matches)) {//formating
                        asort($matches[0]);
                        $counterParts = 0;
                        $formatParams = $linkArr['format'];
                        foreach ($link ['params'] as $key => $value) {
                            $value = str_replace(GET_PARAM_PATTERN, '', substr($value, strpos($value, GET_PARAM_PATTERN), strlen($value)));
                            $formatParams = str_replace($matches[0][$counterParts], (($value != null) ? $value : ''), $formatParams);

                            $counterParts++;
                        }
                        $formatParams = preg_replace("/\{[0-9]\}/", '', $formatParams);
                    }
                $url = $mainURL . str_replace('*', (($formatParams != '' and $formatParams != $linkArr['format']) ? $formatParams : $params), $pattern);
            }
        }

    if ($url == '')// there is no params
        $url = $mainURL . $controller . '/' . $action . (((isset($params[0])) && $params[0] != '/') ? '/' : '') . $params;

    return $url;
}

/*
 * get primaryKeys from model
 * @param object Model	 
 * @return array condition
 */

function getPrimaryCondition($model) {
    $conditionArray = array();
    $primaryKeys = $model->primaryKeys;
    if (is_array($primaryKeys))
        foreach ($primaryKeys as $key)
            $conditionArray [$key] = $model->$key;
    else
        $conditionArray [$primaryKeys] = $model->$primaryKeys;

    return $conditionArray;
}

/*
 * generate param array for db order 
 * @param object Model	 
 * @param string custom order	 
 * @return array parms
 */

function getParamOrder($model, $column, $custom = NULL) {
    $model = $model . 'Model';
    //$sort = (!$column)? $model::$sortColumns:$column;
    $sort = (!$column) ? eval('' . $model . '::$sortColumns;') : $column;

    if ($custom)
        $orderParam = "$sort:$custom";
    else {
        //$sortType = $model::$sortType;
        //eval('$sort = $model::$sortColumns;');
        eval('$sortType = $model::$sortType;');
        //eval('$sortType ='.$model.'::'.$sortType.';');		
        if ($sortType == 'DESC') {
            $orderParam = "$sort:ASC";
        } elseif ($sortType == 'ASC') {
            $orderParam = "$sort:DESC";
        }
    }
    $param = array('sort' => $orderParam);
    return $param;
}

/*
 * change the order in model
 * @param string sort get variable	 
 * @param string class Model	 
 * @return void
 */

function sortModel($sort, $model) {
    $model = $model . 'Model';
    if (isset($sort)) {
        $sortType = substr(strrchr($sort, ':'), 1);
        $sort = str_replace(":" . $sortType, "", $sort);
        //$model::$sortColumns = $sort;
        //$model::$sortType = $sortType;
        eval('' . $model . '::$sortColumns=$sort;');
        eval('' . $model . '::$sortType=$sortType;');
        //eval(''.$model.'::'.$sortColumns.'= $sort;');
        //eval(''.$model.'::'.$sortType.'= $sortType;');	
        if (!is_object($model))
            $model = new $model ();

        $model->$sortColumns = $sort;
        $model->$sortType = $sortType;
    }
}

/*
 * get the apache version
 * @return string of apache version
 */

function apacheVersion() {
    if (function_exists('apache_get_version')) {
        if (preg_match('|Apache\/(\d+)\.(\d+)\.(\d+)|', apache_get_version(), $version)) {
            return $version [1] . '.' . $version [2] . '.' . $version [3];
        }
    } elseif (isset($_SERVER ['SERVER_SOFTWARE'])) {
        if (preg_match('|Apache\/(\d+)\.(\d+)\.(\d+)|', $_SERVER ['SERVER_SOFTWARE'], $version)) {
            return $version [1] . '.' . $version [2] . '.' . $version [3];
        }
    }

    return '(unknown)';
}

/*
 * check the number of succssfull and failure operations 
 * @param int checked elements size 
 * @param int number of succssfull operation 
 * @param int number of failure operation 
 * @param array messageKeys
 * @return array message
 */

function checkOperationCounters($boxSize, $succCounter, $errCounter, $msgs = NULL) {
    if ($msgs == NULL) {
        $msgs ['succ'] = _tr('Done');
        $msgs ['err'] = _tr('Wrong');
    }

    $successMSG = array('msg' => $msgs['succ'], 'type' => $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [TIDY_CONSTANTS::LOG_INFO]);
    $successMSG ['msg'] = sprintf($successMSG ['msg'], $succCounter);

    $failureMSG = array('msg' => $msgs['err'], 'type' => $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [TIDY_CONSTANTS::LOG_ERR]);

    return ($boxSize == $succCounter) ? $successMSG : $failureMSG;
}

/*
 * get array from table
 * @param array grabInfo for connection
 * @return array of rows
 */

function getArrayFromTable($grabInfo) {
    $stmt = $GLOBALS ['registry']->db->pdoResource->prepare('SELECT ' . $grabInfo ['value'] . ' ,' . $grabInfo ['label'] . '  FROM ' . $grabInfo ['table'] . ' WHERE 1=1 ' . $grabInfo ['whereCond']);

    $stmt->execute();
    $result = array();
    while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
        $result [$row [0]] = $row [1];
    }
    return $result;
}

/*
 * convert array into json string to put it in input value
 * @param array value 
 * @param boolean slashes enable 
 * @return string json 
 */

function jsonInputEncode($value, $slashes = true) {
    return ($slashes) ? '' . addslashes(str_replace('"', "'", json_encode($value))) . '' : '' . str_replace('"', "'", json_encode($value)) . '';
}

/*
 * decode json string to array 
 * @param string json
 * @return array value  
 */

function jsonInputDecode($value) {
    $value = str_replace("'", '"', $value);
    $value = str_replace("\\\\", '%', $value);
    $value = str_replace("\\", '', $value);
    $value = str_replace("%", "\\", $value);
    return json_decode(stripslashes($value), true);
}

/* 	
 * Convert a string to camel case, optionally capitalizing the first char and optionally setting which characters are
 * acceptable.
 * @param  string  $str              text to convert to camel case.
 * @param  bool    $capitalizeFirst  optional. whether to capitalize the first chare (e.g. "camelCase" vs. "CamelCase").
 * @param  string  $allowed          optional. regex of the chars to allow in the final string
 * @return string camel cased result
 *
 */

function strtocamel($str, $capitalizeFirst = false, $allowed = 'A-Za-z0-9') {
    return preg_replace(array('/([A-Z][a-z])/e', // all occurances of caps followed by lowers
        '/([a-zA-Z])([a-zA-Z]*)/e', // all occurances of words w/ first char captured separately
        '/[^' . $allowed . ']+/e', // all non allowed chars (non alpha numerics, by default)
        '/^([a-zA-Z])/e'), // first alpha char
            array('" "."$1"', // add spaces
        'strtoupper("$1").strtolower("$2")', // capitalize first, lower the rest
        '', // delete undesired chars
        'strto' . ($capitalizeFirst ? 'upper' : 'lower') . '("$1")'), // force first char to upper or lower
            $str);
}

/*
 * covert the vector into array
 * @param object $vector
 * @return array 
 */

function fromVectorToArray($vector, $key = NULL, $value = NULL) {
    if (is_object($vector)) {
        $array = array();

        for ($i = 0; $i < $vector->size(); $i ++) {
            $array [($key) ? $vector->get($i)->$key : $i] = ($value) ? $vector->get($i)->$value : $vector->get($i);
        }
        return $array;
    }
}

/*
 * encode Client IP
 * @param String $dotquad_ip
 * @return String Encoded 
 */

function encodeIP($dotquad_ip) {
    $ip_sep = explode('.', $dotquad_ip);
    if (sizeof($ip_sep) > 3)
        return sprintf('%02x%02x%02x%02x', $ip_sep [0], $ip_sep [1], $ip_sep [2], $ip_sep [3]);
}

/*
 * decode Client IP
 * @param String $dotquad_ip
 * @return String decoded 
 */

function decodeIP($int_ip) {
    $hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
    return hexdec($hexipbang [0]) . '.' . hexdec($hexipbang [1]) . '.' . hexdec($hexipbang [2]) . '.' . hexdec($hexipbang [3]);
}

/*
 * encode any info
 * @param mixed $info
 * @param boolean $json enable
 * @return String Encoded 
 */

function encode($info, $json = false) {
    return $GLOBALS ['registry']->rsa->encrypt(($json) ? json_encode($info) : $info, $GLOBALS ['registry']->rsaKeys[1], $GLOBALS ['registry']->rsaKeys[0], 5/* packets splits */);
}

/*
 * decode Client IP
 * @param String encoded info
 * @param boolean $json decode
 * @return String decoded 
 */

function decode($enInfo, $json = false) {
    $decryptStr = $GLOBALS ['registry']->rsa->decrypt($enInfo, $GLOBALS ['registry']->rsaKeys[2], $GLOBALS ['registry']->rsaKeys[0]);
    return ($json) ? json_decode($decryptStr, true) : $decryptStr;
}

/*
 * get file extention
 * @param String $filename
 * @return String ext 
 */

function getFileExt($filename) {
    $ext = end(explode('.', $filename));
    return $ext;
}

/*
 * get file list by directory name
 * @param String $directory path
 * @return Array list 
 */

function getDirectoryList($directory) {
    // create an array to hold directory list
    $results = array();
    // create a handler for the directory
    $handler = opendir($directory);
    // open directory and walk through the filenames
    while ($file = readdir($handler)) {
        // if file isn't this directory or its parent, add it to the results
        if ($file != "." && $file != "..") {
            $results [] = $file;
        }
    }
    // tidy up: close the handler
    closedir($handler);
    // done!
    return $results;
}

/*
 * delete files in directory and subs
 * @param directory path
 * @return void
 */

function deleteAllDirFiles($dir) {
    $mydir = opendir($dir);
    while (false !== ($file = readdir($mydir))) {
        if ($file != "." && $file != "..") {
            chmod($dir . $file, 0777);
            if (is_dir($dir . $file)) {
                chdir('.');
                deleteAllDirFiles($dir . $file . '/');
                rmdir($dir . $file) or die("couldn't delete $dir$file<br />");
            } else {
                //	rename($dir.$file, $dir.$file.'.del');
                unlink($dir . $file) or die("couldn't delete $dir$file<br />");
            }
        }
    }
    closedir($mydir);
}

/*
 * map file to url
 * @param String $filePhysicalPath
 * @param String http beforePath
 * @return String file URL Path 
 */

function mapFile2URL($filePhysicalPath, $realHttp = HTTP_HOST_PATH) {
    $fileDirArray = explode(DS, $filePhysicalPath);
    $fileName = array_pop($fileDirArray);
    $realPathDirArr = explode(DS, APP_PATH);
    $diffrentWithRealPathArray = array_values(array_diff($fileDirArray, $realPathDirArr));
    //filter according to rewrite module 
    $fileURL = str_replace(ROOT . '/', '', implode('/', $diffrentWithRealPathArray) . '/' . $fileName);
    $fileURL = ($fileURL[0] == '/') ? substr($fileURL, 1, strlen($fileURL)) : $fileURL;

    return ($realHttp . $fileURL);
}

/*
 * encode url data
 * @param string data
 * @return encoded string
 */

function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

/*
 * decode url data
 * @param string data
 * @return decoded string
 */

function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

/*
 * quick fucntion for translation
 * @param String $filePhysicalPath
 * @param int msg type (optional)
 * @return String $tranlation
 */

function _tr($msg, $msgType = null) {
    //$msg=trim(strtolower($msg));
    $tranlation = $GLOBALS ['registry']->tr->_($msg);
    if ($msgType != NULL) {
        $GLOBALS ['registry']->msg->setMsg($msg, $msgType);
    }

    return $tranlation;
}

/*
 * application language variable defination
 * @see App class languageDefine method
 * @param string $variable value
 * @param string $variableSessionName used for session
 * @return String $variable value
 */

function defineLanguageVariable($variable, $variableSessionName) {

    if (isset($variable) and $variable != NULL and in_array($variable, $GLOBALS ['sysLang'])) { //Insert Session Language
        $GLOBALS ['registry']->session->setSession($variableSessionName, $variable);
    } elseif ($GLOBALS ['registry']->session->getSession($variableSessionName) != NULL) {
        $variable = $GLOBALS ['registry']->session->getSession($variableSessionName);
    } else
        $variable = $GLOBALS ['sysLang'] [DEFAULT_LANG];

    return $variable;
}

/*
 * application language variable defination
 * @see App class languageDefine method
 * @param string $variable value
 * @param string $variableCookieName used for session
 * @return String $variable value
 */

function defineLanguageVariableCookie($variable, $variableCookieName) {

    if (isset($variable) and $variable != NULL and in_array($variable, $GLOBALS ['sysLang'])) { //Insert Session Language
        //	echo 'Set Cookie';
        $GLOBALS ['registry']->response->setCookie($variableCookieName, $variable, TTL_REMEMBER);
    } elseif ($GLOBALS ['registry']->request->getCookie($variableCookieName) != NULL) {
        //	echo 'Get Cookie';

        $variable = $GLOBALS ['registry']->request->getCookie($variableCookieName);
    } else {
        //	echo 'use Defualt';

        $variable = $GLOBALS ['sysLang'] [DEFAULT_LANG];
    }
    return $variable;
}

/*
 * format Number
 * @param float number
 * @param boolean  get integer value
 * @return float formatted Number
 */

function numberFormat($number, $integer = false) {
    if ($integer)
        return Zend_Locale_Format::toInteger($number, array('locale' => $GLOBALS ['registry']->currentLang));

    return Zend_Locale_Format::toNumber($number, array('precision' => NUMBER_FORMAT_PRECISION,
                'locale' => $GLOBALS ['registry']->currentLang));
}

/*
 * check if the browser is mobile or not
 * @return boolean if mobile
 */

function checkIfMobileBrowser() {
    $mobile_browser = '0';

    if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $mobile_browser++;
    }

    if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
        $mobile_browser++;
    }

    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array(
        'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
        'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
        'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
        'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
        'newt', 'noki', 'oper', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
        'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
        'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
        'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
        'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

    if (in_array($mobile_ua, $mobile_agents)) {
        $mobile_browser++;
    }

    if (strpos(strtolower($_SERVER['ALL_HTTP']), 'OperaMini') > 0) {
        $mobile_browser++;
    }

    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') > 0) {
        $mobile_browser = 0;
    }
    return (($mobile_browser > 0) ? true : false);
}

/*
 * Decrypt Client Js functionc2s decrypt
 * @param string encrypted value 
 * @param string security key
 *  */

function c2sdecrypt($s, $k) {
    $s = urldecode($s);
    $k = str_split(str_pad('', strlen($s), $k));
    $sa = str_split($s);
    foreach ($sa as $i => $v) {
        $t = ord($v) - ord($k[$i]);
        $sa[$i] = chr($t < 0 ? ($t + 256) : $t);
    }
    return join('', $sa);
}

/*
 * get header variable value for test purpose
 * @return mixed variable header value
 * @author Hussam El-Kurd
 * */

function getHeaderVariable($header) {
    $headers = getallheaders();
    if (array_key_exists($header, $headers)) {
        return $headers[$header];
    }
}
/*
 * if getallheaders is not defined
 * @return array variable header value
 * */
if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
           $headers = '';
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
           }
       }
       return $headers;
    }
} 
