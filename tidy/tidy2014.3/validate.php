<?php

/**
 * AppValidate, Tidy PHP 
 * validate either server side or client 
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class Validate {

    /**
     * validation array
     * @access private
     * @var array
     */
    private $_validationArray = array();

    /**
     * application request
     * @access private
     * @see AppRequest
     * @var object
     */
    private $_request;

    /**
     * error counting for server side validation
     * @access private
     * @var integer
     */
    private $_errorCounter;

    /**
     * error occure check for server side validation
     * @access private
     * @var boolean
     */
    private $_error = false;

    /**
     * construct 
     * @param object $requestObject	 
     * @see AppRequest	 
     * @access public	  	 
     * @return void
     */
    public function __construct($requestObject) {
        $this->_request = $requestObject;
    }

    /**
     * set the validation array 
     * @param array $validationArray	
     * @access public	  
     * @return void
     */
    public function setValdationArray(array $validationArray) {
        $this->_validationArray = $validationArray;
    }

    /**
     * generate client validation require Jquery
     * @param string $ValidationFunctionName	
     * @param string $greenClass css succssfull class	
     * @param string $wrongClass css error class		 	 
     * @access public	  
     * @return string validation
     */
    public function clientValidate($ValidationFunctionName = 'checkValidation', $greenClass = 'green', $wrongClass = 'red') {
        $JSValidation = "
							// Removes leading whitespaces 
							function LTrim( value ) {var re = /\s*((\S+\s*)*)/;return value.replace(re, \"$1\");}
								// Removes ending whitespaces
								function RTrim( value ) {var re = /((\s*\S+)*)\s*/;return value.replace(re, \"$1\");}
								// Removes leading and ending whitespaces
								function trim( value ) {return LTrim(RTrim(value));}
								function returnWithHtml(value){return '<li>'+value+'</li>';}
							var counter=0;				
							function $ValidationFunctionName()
							{	var messages = [];var error=null; var tube
							\n";
        $requiredString = '$(document).ready(function(){';
        foreach ($this->_validationArray as $SingleControlName => $ConditionAndErrorMessage)
            foreach ($ConditionAndErrorMessage as $Condition => $ErrorMessage) {
                $ErrorMessage = _tr($ErrorMessage);
                //_($ErrorMessage,TIDY_CONSTANTS::LOG_ERR);
                ///	$ErrorMessage = $GLOBALS ['lang']->getMessage ( $ErrorMessage );
                //$ErrorMessage = $ErrorMessage ['msg'];
                $epos = strrpos($Condition, "=");
                list ( $getCondition, $getvalue ) = @split("=", $Condition);
                if ($epos >= 0) {
                    $getMyCondition = $getCondition;
                    $value = $getvalue;
                    if (is_string($value) and $getMyCondition == 'same')
                        $getMyCondition = 'sameFields';
                } else {
                    $getMyCondition = $Condition;
                }

                $JSValidation .= " if($('[name=\"$SingleControlName\"]').length){";
                switch ($getMyCondition) {
                    case '' :
                        $JSValidation .= " 
								if($('[name=\"$SingleControlName\"]').val()==''){
									" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "
									}
									else{
									" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
								
									}	\n";
                        $requiredString .= "$( \"label[for='$SingleControlName']\" ).html($( \"label[for='$SingleControlName']\" ).html()+\" <span class='required'>*</span>\");";
                        break;

                    case 'tinyEditor' :
                        $JSValidation .= " 
								if(tinyMCE.get(\"$SingleControlName\").getContent()==''){
									" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "
									}
									else{
									" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
								
									}	\n";
                        $requiredString .= "$( \"label[for='$SingleControlName']\" ).html($( \"label[for='$SingleControlName']\" ).html()+\" <span class='required'>*</span>\");";
                        break;

                    case "MinChar" :
                        $JSValidation .= " 	
									if($('[name=\"$SingleControlName\"]').val()!=''){						
									if(parseInt(trim($('[name=\"$SingleControlName\"]').val()).length) < parseInt($value))
									{	
											" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "
									
									}
									else{
												" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
								
									}}
										\n";
                        break;

                    case "MaxChar" :
                        $JSValidation .= " 	
									if($('[name=\"$SingleControlName\"]').val()!=''){
									if(parseInt(trim($('[name=\"$SingleControlName\"]').val()).length) > parseInt($value))
									{	
												" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "
									}
									else{
												" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
										}
									}
									\n";
                        break;

                    case "Email" :
                        $JSValidation .= "
						   					var checkEmail;
											checkEmail=$('[name=\"$SingleControlName\"]').val();
											if(checkEmail!='')
											{
											if ((checkEmail.indexOf('@') <=0) || ((checkEmail.charAt(checkEmail.length-4) != '.') && (checkEmail.charAt(checkEmail.length-3) != '.')))
											{
											" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "
											}
											 else{
												" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
												}														
													}	\n";
                        break;

                    case "checkBoxesSelected" :
                        $JSValidation .= "

											if ($('input[type=checkbox]:checked').length<1)
											{
											" . $this->_clientWrongCase($SingleControlName . '[]', $ErrorMessage, $wrongClass, $greenClass) . "
													}
											 else{
												" . $this->_clientSuccCase($SingleControlName . '[]', $wrongClass, $greenClass) . "
												}														
														\n";
                        break;

                    case "checkedBtn" :
                        $JSValidation .= "
              								 if(!$('[name=\"$SingleControlName\"]').is(':checked'))
											 {
											" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "
													}
											 else{
												" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
												}														
												\n
													";
                        break;

                    case "Integer" :
                        $JSValidation .= "
											if(trim($('[name=\"$SingleControlName\"]').val())!=''){
              								 if(!isInteger($('[name=\"$SingleControlName\"]').val()))
											 {
											
											" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "

																							 }
												
											 else{

												" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
											 																			 
											 }
											 }
											 \n";

                        break;

                    case "Float" :
                        $JSValidation .= "
											if(trim($('[name=\"$SingleControlName\"]').val())!=''){
              								 if(isDecimal($('[name=\"$SingleControlName\"]').val()))
											 {
											
											" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "

																							 }
												
											 else{

												" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
											 																			 
											 }
											 }
											 \n";

                        break;

                    case "URL" :
                        $JSValidation .= "
							if($('[name=\"$SingleControlName\"]').val()!='')
											{
											var charpos = $('[name=\"$SingleControlName\"]').val().search(\"^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$\"); 
											if($('[name=\"$SingleControlName\"]').val().length > 0 &&  charpos < 0)
											 {
												" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "
													
																							 }
											 else{
												" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
												 
												}								 
											 
											 }\n";
                        break;

                    case "sameFields" :
                        $JSValidation .= "
							if($('[name=\"$SingleControlName\"]').val()!=$('[name=\"$value\"]').val())
											{
												" . $this->_clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) . "
													
																}
											 else{
												" . $this->_clientSuccCase($SingleControlName, $wrongClass, $greenClass) . "
												 
												}\n								 
											 
											 ";
                        break;
                }
                $JSValidation .= " }";

                //$JSValidation .= " }";				
            }
        $requiredString .= '});';
        $JSValidation .= $this->_clientViewErrors();
        return $JSValidation . $requiredString;
    }

    /**
     * generate client error case for the client
     * @param string $SingleControlName controll validation
     * @param string $ErrorMessage	error message	 
     * @param string $greenClass css succssfull class	
     * @param string $wrongClass css error class		 	 
     * @access private	  
     * @return string 
     */
    private function _clientWrongCase($SingleControlName, $ErrorMessage, $wrongClass, $greenClass) {

        return "messages[messages.length]='$ErrorMessage';//Messages 
				$('[name=\"$SingleControlName\"]').focus();	
				errorCaseValidation($('[name=\"$SingleControlName\"]'))	;			
				//$('[name=\"$SingleControlName\"]').addClass('$wrongClass');	
			//	$('[name=\"$SingleControlName\"]').removeClass('$greenClass');																									
				$('#${SingleControlName}_desc').html('$ErrorMessage');
				error='err';
				counter++;	";
    }

    /**
     * generate client validation file	 	 
     * @access public	  
     * @return void 
     */
    public function writeJsFileValidation() {
        $validationFile = JS_PATH . 'validation' . DS . $this->_request->requestId . '.js';
        if (!file_exists($validationFile)) {
            try {
                $fileValidationHandler = @fopen($validationFile, 'w');
                fwrite($fileValidationHandler, @$this->clientValidate());
                fclose($fileValidationHandler);
            } catch (TidyException $e) {

                echo "can't open validation file";
            }
        }
    }

    /**
     * generate Succssfull  case for the client
     * @param string $SingleControlName controll validation
     * @param string $greenClass css succssfull class	
     * @param string $wrongClass css error class		 	 
     * @access private	  
     * @return string 
     */
    private function _clientSuccCase($SingleControlName, $wrongClass, $greenClass) {

        return "successCaseValidation($('[name=\"$SingleControlName\"]'));
	//	$('[name=\"$SingleControlName\"]').removeClass('$wrongClass');
		//			$('[name=\"$SingleControlName\"]').addClass('$greenClass');
					$('#${SingleControlName}_desc').html('');										
					counter--;	";
    }

    /**
     * generate the action case for the client	 	 
     * @access private	  
     * @return string 
     */
    private function _clientViewErrors() {
        return 'if(error==null){
									return true;
									}
									else{
									var messageString="<ul>";
									for (i in messages) { 	
										notify(messages[i],{messageType:"' . $GLOBALS ['registry']->constantResources['clientFlashMessageLogPriority'][TIDY_CONSTANTS::LOG_ERR] . '"});
										messageString+=messages[i];
										}
									 messageString+="</ul>";

									return false;	
									}										
									 }		//End of JS Validation Function
							';
    }

    /**
     * execute server side validation 
     * @param array $dataSetValidation validation on an array without get from request	 
     * @access public	  
     * @return boolean 
     */
    public function serverValidate($dataSetValidation = NULL) {
        $counter = 0;
        $multipleValue = array();
        $dataSetValidation = (is_array($dataSetValidation) and (sizeof($dataSetValidation) > 0)) ? $dataSetValidation : $this->_request->getRequest();

        foreach ($this->_validationArray as $SingleControlName => $ConditionAndErrorMessage)
            foreach ($ConditionAndErrorMessage as $Condition => $ErrorMessage) {
                //Set Error MEssage
                $epos = strrpos($Condition, "=");
                list ( $getCondition, $getvalue ) = @split("=", $Condition);

                if ($epos >= 0) {
                    $getMyCondition = trim($getCondition);
                    $value = $getvalue;

                    if (is_string($value) and $getMyCondition == 'same') {
                        $getMyCondition = 'sameFields';
                        $value = $dataSetValidation[$value];
                    }
                } else {
                    $getMyCondition = trim($Condition);
                }

                $controlValue = $dataSetValidation[$SingleControlName];
                //enable multi post check and validate
                if (is_array($controlValue) and !empty($controlValue)) {
                    $counterInnerValues = 0;
                    if (sizeof($controlValue) > 1)
                        foreach ($controlValue as $elementKey => $elementValue) {
                            $this->_validationArray[$SingleControlName . '_' . $counterInnerValues] = $ConditionAndErrorMessage;
                            $value = $dataSetValidation[$SingleControlName . '_' . $counterInnerValues] = $elementValue;
                            //$value = $this->_request->setRequest ($SingleControlName.'_'.$counterInnerValues, $elementValue );						
                            $counterInnerValues++;
                            unset($this->_validationArray[$SingleControlName]);
                            $this->_servRsepeateValidation($dataSetValidation);
                        } else
                        $controlValue = array_pop($controlValue);
                    //remove element from current validation
                }

                if (isset($controlValue)) {
                    switch ($getMyCondition) {

                        case '' : {
                                if ($this->notEmpty($controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;

                        case 'tinyEditor' : {

                                if ($this->minChar($value, $controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;


                        case 'MinChar' : {

                                if ($this->minChar($value, $controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;

                        case 'MaxChar' : {

                                if ($this->maxChar($value, $controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;

                        case 'Email' : {
                                if ($this->isEmail($controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;

                        case 'checkBoxesSelected' : {

                                if ($this->notEmpty($controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;

                        case 'checkedBtn' : {

                                if ($this->notEmpty($controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;

                        case 'Integer' : {
                                if ($controlValue == NULL || $controlValue == '')
                                    $this->_serverSuccCase($SingleControlName);
                                else {
                                    if ($this->isInteger($controlValue))
                                        $this->_serverSuccCase($SingleControlName);
                                    else
                                        $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                                }
                            }
                            break;

                        case 'Float' : {
                                if ($controlValue == NULL || $controlValue == '')
                                    $this->_serverSuccCase($SingleControlName);
                                else {
                                    if ($this->isFloat($controlValue))
                                        $this->_serverSuccCase($SingleControlName);
                                    else
                                        $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                                }
                            }
                            break;
                        case 'URL' : {
                                if ($this->isUrl($controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;

                        case 'NotEmptyArray' : {
                                if ($this->isNotEmptyArray($controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;

                        case 'sameFields' : {
                                if ($this->isEqual($value, $controlValue))
                                    $this->_serverSuccCase($SingleControlName);
                                else
                                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);
                            }
                            break;
                    }
                }
                elseif ($getMyCondition === '')
                    $this->_serverWrongCase($SingleControlName, $ErrorMessage);

                $counter ++;
            }

        if ($this->_error) { //there is error
            $errorMessages = $this->_serverViewErrors();
            return $errorMessages;
        } else {
            return true;
        }
    }

    /**
     * validate is array and not empty
     * @param array $value
     * @access public
     * @return boolean
     */
    public function isNotEmptyArray($value) {
        if (is_array($value)) {
            $status = (sizeof($value) > 0) ? true : false;
        } else
            $status = false;
        return $status;
    }

    /**
     * validate if float (accept integer)
     * @access public	  
     * @return boolean 
     */
    public function isFloat($value) {
        $status = true;
        if (!is_float($value + 1))
            $status = false;
        return ($status) ? $status : is_int($value + 1);
    }

    /**
     * validate digits value 
     * @access public	  
     * @return boolean 
     */
    public function isDigit($value) {
        $validator = new Zend_Validate_Digits ();
        return $validator->isValid($value);
    }

    /**
     * validate email value 
     * @access public	  
     * @return boolean 
     */
    public function isEmail($value) {
        $validator = new Zend_Validate_EmailAddress ();
        return $validator->isValid($value);
    }

    /**
     * validate integer value 
     * @access public	  
     * @return boolean 
     */
    public function isInteger($value) {
        $validator = new Zend_Validate_Int ();
        return $validator->isValid($value);
    }

    /**
     * validate less than  value 
     * @access public	  
     * @return boolean 
     */
    public function lessThan($max, $value) {
        $validator = new Zend_Validate_LessThan(array('max' => $max));
        return $validator->isValid($value);
    }

    /**
     * validate greate than value 
     * @access public	  
     * @return boolean 
     */
    public function greaterThan($min, $value) {
        $validator = new Zend_Validate_GreaterThan(array('min' => $min));
        return $validator->isValid($value);
    }

    /**
     * validate between value
     * @access public
     * @return boolean
     */
    public function between($min, $max, $value) {
        $validator = new Zend_Validate_Between(array('min' => $min, 'max' => $max));
        return $validator->isValid($value);
    }

    /**
     * validate required  value 
     * @access public	  
     * @return boolean 
     */
    public function notEmpty($value) {
        $validator = new Zend_Validate_NotEmpty ();
        return $validator->isValid($value);
    }

    /**
     * validate min value 
     * @access public	  
     * @return boolean 
     */
    public function minChar($min, $value) {
        $validator = new Zend_Validate_StringLength(array('min' => $min));
        $validator->setEncoding("UTF-8");
        return $validator->isValid($value);
    }

    /**
     * validate max value 
     * @access public	  
     * @return boolean 
     */
    public function maxChar($max, $value) {
        $validator = new Zend_Validate_StringLength(array('max' => $max));
        $validator->setEncoding("UTF-8");
        return $validator->isValid($value);
    }

    /**
     * validate url value 
     * @access public	  
     * @return boolean 
     */
    public function isUrl($value) {
        $validator = new Zend_Validate_Callback(array('Zend_Uri', 'check'));
        return $validator->isValid($value);
    }

    /**
     * validate equal value 
     * @access public	  
     * @return boolean 
     */
    public function isEqual($value1, $value2) {
        return ($value1 == $value2);
    }

    /**
     * validate mobile value
     * @access public
     * @return boolean
     */
    public function isMobileNumber($mobile) {
        if (preg_match('/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/', $mobile, $matches)) {
            //print_r($matches);
            return true;
        } else
            return false;
    }

    /**
     * generate server succsesfull case
     * @param string $SingleControlName controll validation 	 
     * @access private	  
     * @return string 
     */
    private function _serverSuccCase($SingleControlName) {
        $this->_errorCounter --;
    }

    /**
     * generate server error case
     * @param string $SingleControlName controll validation
     * @param string $errorMessage	error message	 	 	 
     * @access private	  
     * @return void 
     */
    private function _serverWrongCase($SingleControlName, $errorMessage) {
        $this->_errorCounter ++;
        //Messages
        //$GLOBALS['registry']->msg->m($errorMessage,TIDY_CONSTANTS::LOG_ERR);
        $GLOBALS['registry']->msg->setMsg($errorMessage, ERROR_VALIDATE_MSG);

        $this->_error = true;
    }

    /**
     * repeate validation server sider and clear the messages and faluts
     * @param array $dataSetValidation validation on an array without get from request	 
     * @access private
     * @return void
     */
    private function _servRsepeateValidation($dataSetValidation) {
        $this->_errorCounter = 0;
        //Messages
        $allMessagesWithoutValidationError = array();
        $allMessages = $GLOBALS['registry']->msg->getAllMessages();
        $messagesValidate = $GLOBALS['registry']->msg->getAllMessages(ERROR_VALIDATE_MSG);
        if (is_array($allMessages) and is_array($messagesValidate))
            $allMessagesWithoutValidationError = array_diff($allMessages, $messagesValidate);
        $GLOBALS['registry']->msg->messages = $allMessagesWithoutValidationError;
        $this->_error = false;
        $this->serverValidate($dataSetValidation);
    }

    /**
     * generate the action case for the server	 	 
     * @access private	  
     * @return void 
     */
    private function _serverViewErrors() {
        return $GLOBALS['registry']->msg->getAllMessages(ERROR_VALIDATE_MSG);
    }

}
