<?php

/**
 * AppSession, Tidy PHP 
 * set and get variables through sessions
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @uses Zend_Session Class
 * @filesource
 */
//namespace Tidy;

class Session {

    /**
     * session name Space
     * @access private 
     * @var string 
     */
    private $_sessionNameSpace;

    /**
     * session array keys
     * @access private 
     * @var array 
     */
    private $_sessionKeys = array();

    /**
     * session expire time
     * @access public 
     * @var integer 
     */
    public $expireSession = 3600;

    /**
     * constructor call start function 
     * @access public
     * @return void
     */
    function __construct() {
        $this->startSession();
    }

    /**
     * start session
     * @access private
     * @see Zend Session 
     * @return object Zend Session
     */
    private function startSession() {
        return $GLOBALS ['registry']->sessionZendOb;
    }

    /**
     * set session variable
     * @access public
     * @param string session key
     * @param mixed session value
     * @param integer optional expire time
     * @return void
     */
    public function setSession($index, $value, $expireTime = null) {
        $value = is_object($value) ? base64_encode(serialize($value)) : $value;
        $GLOBALS ['registry']->sessionZendOb->$index = $value;
        $GLOBALS ['registry']->sessionZendOb->setExpirationSeconds(is_int($expireTime) ? $expireTime : $this->expireSession , $index);
    }

    /**
     * get values from specific session variable
     * @access public
     * @param string session key
     * @param boolean  srilaization option if the value is object
     * @return mixed session value
     */
    public function getSession($index, $serialize = false) {
        return ($serialize) ? unserialize(base64_decode($GLOBALS ['registry']->sessionZendOb->$index)) : $GLOBALS ['registry']->sessionZendOb->$index;
    }

    /**
     * destory special session key
     * @access public
     * @param string session key
     * @return void
     */
    public function destroySessionKey($index) {
        unset($this->_sessionKeys [$index]);
        unset($GLOBALS ['registry']->sessionZendOb->$index);
    }

    /**
     * get the current session id
     * @access public
     * @return mixed sessionID
     */
    public function getId() {
        return Zend_Session::getId();
    }

    /**
     * set  session id
     * @access public
     */
    public function setId($id) {
        return Zend_Session::setId($id);
    }

    /**
     * destory special all session keys
     * @access public
     * @return void
     */
    public function destroySession() {

        if (!empty($this->_sessionKeys)) {
            foreach ($this->_sessionKeys as $key => $value) {
                $this->destroySessionKey($key);
            }
        } else {
            unset($this->_sessionKeys);
        }
        session_destroy();
    }

}
