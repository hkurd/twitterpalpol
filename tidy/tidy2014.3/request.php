<?php

/**
 * AppRequest, Tidy PHP
 * Application requests (GET,POST_COOKIE)
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class Request extends Http {

    /**
     * request Method
     * @access public
     * @var string
     */
    public $requestMethod = '';

    /**
     * save request info
     * @access public
     * @var array
     */
    public $server = array();

    /**
     * generate request id for each request
     * @access public
     * @var string
     */
    public $requestId = array();

    /**
     * check if object created
     * @static
     * @access private
     * @var boolean
     */
    private static $_isCreated;
    /**
     * store the application request registry object
     * @access private
     * @static
     * @var object
     */

    /**
     * cookie default expire time
     * @access public
     * @var integer
     */
    public $expireTime = 86400;

    /**
     * object request registry
     * @access private
     * @static
     * @var object
     */
    private static $RequestRegistry;

    private function __construct() {

        $this->iniRequest();
    }

    /**
     * get the object request registry and create it if is not found
     * @return object registry
     */
    public static function createRequestRegistry() {
        if (FALSE == self::$_isCreated) {
            if (NULL == self::$RequestRegistry) {
                self::$RequestRegistry = new Request ();
            }
            self::$_isCreated = TRUE;
            return self::$RequestRegistry;
        } else {
            self::$RequestRegistry->iniRequest();
            return self::$RequestRegistry;
        }
    }

    /**
     * define request varaibles initial
     * @return object registry
     */
    private function iniRequest() {

        parent::iniSuperGlobals();
        //$this->_request
        $this->server = $_SERVER;
        $this->requestId = md5($this->server ["REQUEST_URI"]);
        $this->requestMethod = $this->server['REQUEST_METHOD'];
    }

    /**
     * decode request data
     * @param array data to be encoded
     * @param boolean return array or object
     * @return mixed object|array
     */
    public function decodeRequestData($data, $returnArray = true) {
        if (get_magic_quotes_gpc()) {
            $data = stripslashes($data);
        }
        $dataEncode = json_decode($data, $returnArray);
        if ($dataEncode == NULL)
            $dataEncode = json_decode(substr($data, strpos($data, '{')), $returnArray); //for encodeing BOM
        return $dataEncode;
    }

    /**
     * set or overwrite special index in $_GET
     * @param string $index
     * @param mixed $value
     * @return void
     */
    public function setGet($index, $value) {
        $_GET [$index] = $value;
        $this->setRequest($index, $value);
        $this->_get [$index] = $value;
    }

    /**
     * get  special index in $_GET,$_REQUEST
     * @param string $index
     * @return  mixed $value
     */
    public function getGet($index = NULL) {

        return isset($index) ? ((isset($this->_get [$index]) and $this->_get [$index] != '') ? $this->_get [$index] : NULL) : $this->_get;
    }

    /**
     * delete special index in $_GET
     * @param string $index
     * @return  void
     */
    public function deleteGet($index) {
        //$this->deleteRequest ( $index );
        unset($_GET [$index]);
        unset($this->_get [$index]);
    }

    /**
     * set or overwrite special index in $_POST,$_REQUEST
     * @param string $index
     * @param mixed $value
     * @return void
     */
    public function setPost($index, $value) {
        $_POST [$index] = $value;
        $this->setRequest($index, $value);
        return $this->_post [$index] = $value;
    }

    /**
     * get special index in $_POST
     * @param string $index
     * @return  mixed $value
     */
    public function getPost($index = NULL) {
        return isset($index) ? ((isset($this->_post [$index]) and $this->_post [$index] != '') ? $this->_post [$index] : NULL) : $this->_post;
    }

    /**
     * delete special index in $_POST
     * @param string $index
     * @return  void
     */
    public function deletePost($index) {
        $this->deleteRequest($index);
        unset($_POST [$index]);
        unset($this->_post [$index]);
    }

    /**
     * getPut data with PUT
     * @return  array data
     */
    public function getPut($index = NULL) {
        return $this->getPost($index);
    }

    /**
     * get special index in $_COOKIE
     * @param string $index
     * @return  mixed $value
     */
    public function getCookie($index = NULL, $serialize = false) {

        return isset($index) ? (isset($this->_cookie [$index]) ?
                        (($serialize) ? unserialize(base64_decode($this->_cookie [$index])) : $this->_cookie [$index]) : NULL) : $this->_cookie;
    }

    /**
     * get special index in $_REQUEST
     * @param string $index
     * @return  mixed $value
     */
    public function setRequest($index, $value) {
        $_REQUEST [$index] = $value;
        return $this->_request [$index] = $value;
    }

    /**
     * set or overwrite special index in $_REQUEST
     * @param string $index
     * @param mixed $value
     * @return void
     */
    public function getRequest($index = NULL) {
        return isset($index) ? (isset($this->_request [$index]) ? $this->_request [$index] : NULL) : $this->_request;
    }

    /**
     * delete special index in $_REQUEST
     * @param string $index
     * @return  void
     */
    public function deleteRequest($index) {
        unset($_REQUEST [$index]);
        unset($this->_request [$index]);
    }

    /**
     * get request info
     * @return  array server
     */
    public function getRequstInfo() {
        return $this->server;
    }

}
