<?php

/**
 * AppRegistry, Tidy PHP 
 * Registry class set and get global variables in the system
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class Registry {

    /**
     * check if object created
     * @static	 
     * @access private
     * @var boolean
     */
    private static $_isCreated;

    /**
     * store the application registry object
     * @access private
     * @static
     * @var object
     */
    private static $_registry;

    /**
     * vars stored 
     * @access private
     * @var array
     */
    private $vars = array();

    /**
     * constructor 
     * @param registry object
     * @access private
     * @return void
     */
    private function __construct() {
        
    }

    /**
     * get the object regisry and create it if is not found
     * @return object registrty
     */
    public static function createRegistry() {
        if (FALSE == self::$_isCreated) {
            if (NULL == self::$_registry) {
                self::$_registry = new Registry ();
            }
            self::$_isCreated = TRUE;
            return self::$_registry;
        } else {
            return NULL;
        }
    }

    /**
     * set variables
     * @param string $index
     * @param mixed $value
     * @return void
     */
    public function __set($index, $value) {
        $GLOBALS[$index] = $value;
    }

    /**
     * get variables
     * @param string $index
     * @return mixed $value
     */
    public function __get($index) {
        return $GLOBALS[$index];
    }

}
