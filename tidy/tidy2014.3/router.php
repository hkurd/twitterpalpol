<?php

/**
 * AppRouter, Tidy PHP 
 * Router class ini the controllers and views
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
// namespace Tidy;
class Router {

    /**
     * include path
     * 
     * @access private
     * @var string
     */
    protected $_path;

    /**
     * arguments
     * 
     * @access private
     * @var array
     */
    protected $_args = array();

    /**
     * included file
     * 
     * @access public
     * @var string
     */
    public $file;

    /**
     * current controller
     * 
     * @access public
     * @var string
     */
    public $controller;

    /**
     * current action
     * 
     * @access public
     * @var string
     */
    public $action;

    /**
     * @var string 
     * main action function name
     */
    protected $_mainActionFuncName, $_controllerObject;

    function __construct() {
        
    }

    /**
     * load the controller
     * @access public
     * @return void
     */
    protected function loaderCommon() {
        /**
         * * if the file is not there diaf **
         */
        if (is_readable($this->file) == false) {
            $this->controller = DEFAULT_CONTROLLER;
            $this->file = CONTROLLER_PATH . $this->controller . 'Controller' . CONTROLLER_EXT;
        }
        /**
         * * include the controller **
         */
        include $this->file;
        /**
         * * a new controller class instance **
         */
        $class = $this->controller . 'Controller';
        $modelName = $this->controller . 'Model';
        $this->_controllerObject = new $class($GLOBALS ['registry']);
        // Model
        if (class_exists($this->_controllerObject->relatedModel)) {
            $modelName = $this->_controllerObject->relatedModel;
            $model = new $modelName ();
            $this->_controllerObject->model = $model;
        } elseif (class_exists($modelName)) {
            $model = new $modelName ();
            $this->_controllerObject->model = $model;
        }
        $GLOBALS ['registry']->data = $GLOBALS ['registry']->request->getPost();
        /**
         * *Load Helpers **
         */
        if (is_array($this->_controllerObject->helpers))
            $this->loadHelpers($this->_controllerObject->helpers);

        $this->_mainActionFuncName = ucwords($this->action);
        /**
         * * call the iniaitor **
         */
        $this->_controllerObject->ini();
    }

    /**
     * inite the Loader (define controller , define action , define params)
     * @return void
     */
    public function iniLoader() {
        $controller = $action = NULL;

        $route = (empty($_GET ['rt'])) ? '' : $_GET ['rt'];
        $parts = explode('/', $route);
        if (empty($route)) {
            $route = 'index';
        } else {
            $linkArr = $this->_getMapLinkArr();
            $controller = $linkArr ['controller'];
            $action = $linkArr ['action'];
            $params = $linkArr ['params'];

            if ($controller == NULL or $action == NULL) {
                $part1 = array_shift($parts);
                $controller = $part1;
                $action = array_shift($parts);
            }

            $this->controller = $controller;

            if (isset($action)) {
                $this->action = $action;
            }

            /* Setting Params Already defined in the mapping */
            if (isset($params) and is_array($params)) {
                foreach ($params as $key => $vlaue) {
                    $parts [] = $key . GET_PARAM_PATTERN . $vlaue;
                }
                unset($params);
            }

            if (!empty($parts)) {
                $partNoPatternCounter = 0;
                foreach ($parts as $part) {
                    if (strstr($part, GET_PARAM_PATTERN)) {
                        $key = str_replace(GET_PARAM_PATTERN, '', substr($part, 0, strpos($part, GET_PARAM_PATTERN)));
                        $value = str_replace(GET_PARAM_PATTERN, '', substr($part, strpos($part, GET_PARAM_PATTERN), strlen($part)));
                        $registryArgs [$key] = $value;
                        $GLOBALS ['registry']->request->setGet($key, $value);
                    } else {
                        $partNumber = 'param' . ($partNoPatternCounter + 1);
                        $registryArgs [$partNumber] = $part;
                        $GLOBALS ['registry']->request->setGet($partNumber, $part);
                        $partNoPatternCounter ++;
                    }
                }
                $this->_args = $registryArgs;
            }
        }

        if (empty($this->controller) or empty($this->action)) {
            $this->controller = DEFAULT_CONTROLLER;
            $this->action = DEFAULT_ACTION;
        }

        $GLOBALS ['registry']->controller = $this->controller;
        $GLOBALS ['registry']->action = $this->action;
        $GLOBALS ['registry']->args = $this->_args;
        /**
         * * set the file path **
         */
        $this->file = CONTROLLER_PATH . $this->controller . 'Controller' . CONTROLLER_EXT;
    }

    /**
     * check mapping URLS and return the real name controllers 
     * and action and params
     */
    private function _getMapLinkArr() {
        /**
         * get the parts of the route **
         */
        if (!empty($GLOBALS ['routerMapping'])) {
            foreach ($GLOBALS ['routerMapping'] as $pattern => $linkArr) {

                $pattern = str_replace('*', '', $pattern);
                if (strstr($route, $pattern)) {
                    $partsString = str_replace($pattern, '', $route);
                    $parts = explode('/', str_replace($pattern, '', $route));
                    $paramsArray = array();
                    if ($linkArr ['format'] != '') { // if there is exsist format @todo order variables according to given numbers
                        $paramsFormatArray = array();
                        $orderdFormatSegmants = $formatSegmants = explode("########Replace######", preg_replace("/\{[0-9]\}/", "########Replace######$0", $linkArr ['format'])); // replace get values with specifc string
                        $orderdFormatSegmants = $formatSegmants = array_filter($formatSegmants, 'strlen'); // removing null values

                        asort($orderdFormatSegmants); // sort according to varaibles order {1},{2} etc
                        $orderdFormatSegmants = array_values($orderdFormatSegmants);

                        $removeStringValue = function ($value) {
                            return preg_replace("/\{[0-9]\}/", '', $value);
                        };
                        $orderdFormatSegmants = array_map($removeStringValue, $orderdFormatSegmants); // remove {1} , {2}
                        $formatSegmants = array_map($removeStringValue, $formatSegmants); // remove {1} , {2}
                        $segmentCounter = 1;
                        $sizeOfSegmants = sizeof($formatSegmants);
                        foreach ($formatSegmants as $segmant) { // start processing an needed parts
                            // var_dump($partsString);
                            if ($sizeOfSegmants != $segmentCounter && $segmant != '')
                                $valueSegment = trim(substr($partsString, 0, (strpos($partsString, $segmant))));
                            else
                                $valueSegment = $partsString;
                            if ($valueSegment != '') { // find a value
                                $keyOrder = array_search($segmant, $orderdFormatSegmants); // get the true order of the value
                                unset($orderdFormatSegmants [$keyOrder]);
                                $partsString = substr($partsString, strlen($valueSegment . $segmant)); // remove the segment and the value from the parts string
                                $paramsFormatArray [$keyOrder] = 'param' . ($keyOrder + 1) . GET_PARAM_PATTERN . $valueSegment; // adding values as get variables
                                $segmentCounter ++;
                            }
                        }
                        asort($paramsFormatArray); // sort according to varaibles order {1},{2} etc
                        // $linkArr['regex']=preg_replace("/(*|/|+|d|D|w|W|{|}|s|S|^|$)/",'\$0', $linkArr['regex']);
                        $parts = array_merge($parts, $paramsFormatArray); // merge with parts array
                    }
                    return $linkArr;

                    break;
                }
            }
        }
    }

    /**
     * load the helpers for controller
     * @access private
     * @param array helpers
     * @return void
     */
    public function loadHelpers(array $helpers) {
        if (!empty($helpers))
            foreach ($helpers as $helper) {
                if (!@include (HELPER_PATH . $helper . HELPER_EXT))
                    throw new Exception("Failed to include '$helper' helper");
            }
    }

    /**
     * set controller directory path
     * @param string $path        	
     * @return void
     */
    function setPath($path) {
        // check if path i sa directory 
        if (is_dir($path) == false) {
            throw new Exception('Invalid controller path: `' . $path . '`');
        }
        $this->_path = $path;
    }

}
