<?php

/**
 * AppResponse, Tidy PHP
 * Application requests (GET,POST_COOKIE)
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class Response extends Http {

    /**
     * check if object created
     * @static
     * @access private
     * @var boolean
     */
    private static $_isCreated;

    /**
     * object request registry
     * @access private
     * @static
     * @var object
     */
    private static $ResponseRegistry;

    private function __construct() {

        $this->iniResponse();
    }

    /**
     * get the object request registry and create it if is not found
     * @return object registry
     */
    public static function createResponseRegistry() {
        if (FALSE == self::$_isCreated) {
            if (NULL == self::$ResponseRegistry) {
                self::$ResponseRegistry = new Response ();
            }
            self::$_isCreated = TRUE;
            return self::$ResponseRegistry;
        } else {
            self::$ResponseRegistry->iniResponse();
            return self::$ResponseRegistry;
        }
    }

    /**
     * set the request values (super globals)
     * @return void
     */
    private function iniResponse() {
        parent::iniSuperGlobals();
    }

    /**
     * process request
     * @param array data
     * @return  mixed (json | mixed data)
     */
    private static function encodeResponseData($data, $contentType) {
        //alwayes json
        if (strpos($contentType, 'json'))
            return json_encode($data);
        else
            return $data;
    }

    /**
     * response http request
     * @param int $statusapplication/json
     * @param array
     * @return  void
     */
    public static function sendResponse($status = 200, $data = null, $contentType = 'application/json', $headers = array(), $cash = 0) {
        $statusHeader = 'HTTP/1.1 ' . $status . ' ' . parent::getStatusMessage($status);
        header($statusHeader);
        header('Content-type: ' . $contentType);
        //enable all domains to ger resonse @todo security
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST'); //, DELETE , PUT
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Headers: X-Requested-With');
        header('Access-Control-Allow-Headers: Content-Type, Accept');
        header('Access-Control-Max-Age: 1728000');

        if (!empty($headers))
            foreach ($headers as $header)
                header($header);

        if ($cash > 0) {
            $ts = gmdate("D, d M Y H:i:s") . " GMT";
            header("Expires: $ts");
            header("Last-Modified: $ts");
            header("Pragma: no-cache");
            header("Cache-Control: no-cache, must-revalidate");
        } else {
            $ts = gmdate("D, d M Y H:i:s", time() + $cash) . " GMT";
            header("Expires: $ts");
            header("Pragma: cache");
            header("Cache-Control: max-age=$cash");
        }

        if ($data != '') {
            // send the body
            die(self::encodeResponseData($data, $contentType));
        }
    }

    /**
     * response file
     * @param file path
     * @return  void
     */
    public static function responseDownloadFile($fullPath) {

        // Must be fresh start
        if (headers_sent())
            self::sendResponse(HTTP::HTTP_BAD_REQUEST);

        // Required for some browsers
        if (ini_get('zlib.output_compression'))
            ini_set('zlib.output_compression', 'Off');

        // File Exists?
        if (file_exists($fullPath)) {

            // Parse Info / Get Extension
            $fsize = filesize($fullPath);
            $path_parts = pathinfo($fullPath);
            $ext = strtolower($path_parts["extension"]);

            // Determine Content Type
            switch ($ext) {
                case "pdf": $ctype = "application/pdf";
                    break;
                case "exe": $ctype = "application/octet-stream";
                    break;
                case "zip": $ctype = "application/zip";
                    break;
                case "doc": $ctype = "application/msword";
                    break;
                case "xls": $ctype = "application/vnd.ms-excel";
                    break;
                case "ppt": $ctype = "application/vnd.ms-powerpoint";
                    break;
                case "gif": $ctype = "image/gif";
                    break;
                case "png": $ctype = "image/png";
                    break;
                case "jpeg":
                case "jpg": $ctype = "image/jpg";
                    break;
                default: $ctype = "application/force-download";
            }

            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false); // required for certain browsers
            header("Content-Type: $ctype");
            header("Content-Disposition: attachment; filename=\"" . basename($fullPath) . "\";");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . $fsize);
            ob_clean();
            flush();
            readfile($fullPath);
        } else
            self::sendResponse(HTTP::HTTP_BAD_REQUEST);
    }

    /**
     * set or overwrite special index in $_COOKIE,$_REQUEST
     * @param string $index
     * @param mixed $value
     * @return void
     */
    public function setCookie($index, $value, $expireTime = null, $path = '/') {
        //	$this->setResponse ( $index, $value );
        $value = is_object($value) ? base64_encode(serialize($value)) : $value;
        setcookie($index, $value, time() + (is_int($expireTime) ? $expireTime : $this->expireTime), $path);
        setcookie($index . '_PATH', $path, time() + (is_int($expireTime) ? $expireTime : $this->expireTime), $path);

        $this->_cookie [$index][$path] = $path;
        return $this->_cookie [$index] = $value;
    }

    /**
     * delete special index in $_COOKIE
     * @param string $index
     * @return  void
     */
    public function deleteCookie($index) {
        if (array_key_exists($index . '_PATH', $this->_cookie)) {
            setcookie($index, "", - 1, $this->_cookie [$index . '_PATH']);
            setcookie($index . '_PATH', "", - 1, $this->_cookie [$index . '_PATH']);
            unset($this->_cookie [$index]);
        }
    }

    /**
     * destroy all the cookie
     * @return  void
     */
    public function destroyCookie() {
        foreach ($this->_cookie as $index => $value) {
            unset($this->_cookie [$index]);
            setcookie($index, "", - 1, $this->_cookie [$index . '_PATH']);
            setcookie($index . '_PATH', "", - 1, $this->_cookie [$index . '_PATH']);
        }
    }

}
