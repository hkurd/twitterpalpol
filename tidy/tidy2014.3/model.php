<?php

/**
 * AppModel, Tidy PHP 
 * Parent class for all models to set and get values from and to DB
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
//namespace Tidy;

class Model {

    /**
     * request data (POST , PUT)
     * @access public
     * @var array
     */
    public $data = array();

    /**
     * save setters data 
     * @access private
     * @var array
     */
    private $_data = array();

    /**
     * save condations if exists  
     * @access private
     * @static
     * @var array
     */
    private static $_conditions = array();

    /**
     * navigation option
     * @access public
     * @var boolean
     */
    public static $navigation = false;

    /**
     * set condations function
     * @param array of Conditions
     * @return void 
     */
    public static function setCondition(array $conditions) {
        self::$_conditions = $conditions;
    }

    /**
     * add condation to conditions array
     * @param string $index
     * @param string $value
     * @return void
     */
    public function addCondition($index, $value) {
        self::$_conditions[$index] = $value;
    }

    /**
     * remove condition from a conditons array
     * @param array of Conditions
     * @return void
     */
    public function removeCondition($index) {
        unset(self::$_conditions[$index]);
    }

    /**
     * get condition by index key or conditons array 
     * @param string $index (optional)
     * @return mixed elemet or array according to $index
     */
    public function getCondition($index = null) {
        return isset($index) ? ((isset(self::$_conditions[$index]) and self::$_conditions [$index] != '') ? self::$_conditions : NULL) : self::$_conditions;
    }

    /**
     * magic set function
     * @param string $key
     * @param mixed $value
     * @return void 
     */
    public function __set($key, $value) {
        if (isset($value) and $value != NULL)
            $this->_data [$key] = $value;
    }

    /**
     * magic get function
     * @param string $key
     * @return mixed $value 
     */
    public function __get($key) {
        return $this->_data [$key];
    }

    /**
     * magic unset function
     * @param string $key
     * @return void 
     */
    public function __unset($key) {
        unset($this->_data [$key]);
    }

    /**
     * get the data without condition
     * @return array data
     */
    public function getData() {
        return $this->_data;
    }

    /**
     * get all the stored data with condition if exists
     * @return array data
     */
    public function getAllData() {
        $condation = $this->getCondition();
        $data = $this->getData();
        if (!empty($condation))
            return (!empty($data)) ? array_merge($condation, $data) : $condation;
        else
            return $data;
    }

    /**
     * clear all saved data
     * @static 
     * @return void 
     */
    public static function clear() {
        self::$_conditions = array();
        //$this->_data = array ();
    }

    /**
     * destructor  
     * @access public	 	 	 
     */
    public function __destruct() {
        
    }

}

?>