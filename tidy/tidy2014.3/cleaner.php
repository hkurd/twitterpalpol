<?php

class Cleaner {

    public $stopwords = array(' insert ', ' update ', ' alter ', ' delete ', ' drop '); //you need to extend this big time.
    public $symbols = array('/', '\\', '\'', '"', ',', '.', '<', '>', '?', ';', '[', ']', '{', '}', '|', '=', '+', '-', '_', ')', '(', '@', '!', '~', '`'); //this will remove punctuation

    public function cleanInput($string) {
        if (is_string($string) and $string != '') {
            $string = ' ' . strtolower($string) . ' ';
            //$string = $this->stringStripQuotes ( $string );
            $string = $this->removeStopwords($string);
            $string = $this->removeSymbols($string);

            return $string;
        }
    }

    public function removeStopwords($string) {
        for ($i = 0; $i < sizeof($this->stopwords); $i ++) {
            $string = str_replace($this->stopwords [$i], ' ', $string);
        }

        //$string = str_replace('  ',' ',$string);
        return trim($string);
    }

    public function removeSymbols($string) {
        for ($i = 0; $i < sizeof($this->symbols); $i ++) {
            $string = str_replace($this->symbols [$i], ' ', $string);
        }

        return trim($string);
    }

    public function stringStripQuotes($string) {
        if (get_magic_quotes_gpc())
            $string = stripslashes($string);
        if (!is_numeric($string))
            $string = mysql_real_escape_string($string);
        return $string;
    }

    public function stripWordHtml($text, $allowedTags = '<b><i><sup><sub><em><strong><u><br><img><object><param><p>') {
        mb_regex_encoding('UTF-8');
        //replace MS special characters first
        $search = array('/&lsquo;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u');
        $replace = array('\'', '\'', '"', '"', '-');
        $text = preg_replace($search, $replace, $text);
        //make sure _all_ html entities are converted to the plain ascii equivalents - it appears
        //in some MS headers, some html entities are encoded and some aren't
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        //try to strip out any C style comments first, since these, embedded in html comments, seem to
        //prevent strip_tags from removing html comments (MS Word introduced combination)
        if (mb_stripos($text, '/*') !== FALSE) {
            $text = mb_eregi_replace('#/\*.*?\*/#s', '', $text, 'm');
        }
        //introduce a space into any arithmetic expressions that could be caught by strip_tags so that they won't be
        //'<1' becomes '< 1'(note: somewhat application specific)
        $text = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
        $text = strip_tags($text, $allowedTags);
        //eliminate extraneous whitespace from start and end of line, or anywhere there are two or more spaces, convert it to one
        $text = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $text);
        //strip out inline css and simplify style tags
        $search = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
        $replace = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
        $text = preg_replace($search, $replace, $text);
        //on some of the ?newer MS Word exports, where you get conditionals of the form 'if gte mso 9', etc., it appears
        //that whatever is in one of the html comments prevents strip_tags from eradicating the html comment that contains
        //some MS Style Definitions - this last bit gets rid of any leftover comments */
        $num_matches = preg_match_all("/\<!--/u", $text, $matches);
        if ($num_matches) {
            $text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
        }
        return $text;
    }

    public function fixJsonDecodeValues(&$value) {
        $fixValue = '';
        if (is_string($value)) {
            if (strstr($value, '_')) {
                $fixValue = str_replace('_', '.', $value);
                if (preg_match('/^-?(?:\d+|\d*\.\d+)$/', $fixValue)) {
                    $value = $fixValue;
                }
            }
        }
    }

    public function isJson($string) {
        return !empty($string) && is_string($string) && preg_match('/^("(\\.|[^"\\\n\r])*?"|[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t])+?$/', $string);
    }

}
