<?php

/**
 * AppRouter, Tidy PHP 
 * Router class ini the controllers and views
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
class RouterApi extends Router {

    /**
     * @var boolean 
     * enable api restfull
     */
    private $_isRestfull = false;

    /**
     * @var string
     * restfull action name 
     */
    private $_resfullFuncName = '';

    public function __construct($restFull = false) {
        $this->_isRestfull = $restFull;
    }

    /**
     * set data according to request type
     */
    private function _getDataAccordingToRequestType() {
        $preProcessArray = array();

        switch ($GLOBALS ['registry']->request->requestMethod) { // getData
            case "GET" :
                $this->_resfullFuncName = 'get' . ucfirst($this->_mainActionFuncName);
                break;
            case "POST" :
                $this->_resfullFuncName = 'post' . ucfirst($this->_mainActionFuncName);
                // $GLOBALS ['registry']->request->getPost ();
                $preProcessArray = $GLOBALS ['registry']->request->decodeRequestData(file_get_contents('php://input'));
                if (is_array($preProcessArray)) {
                    array_walk($preProcessArray, array(
                        $GLOBALS ['registry']->cleaner,
                        'fixJsonDecodeValues'
                    ));
                }
                break;
            case "PUT" :
                $this->_resfullFuncName = 'put' . ucfirst($this->_mainActionFuncName);
                $preProcessArray = $GLOBALS ['registry']->request->decodeRequestData(file_get_contents('php://input'));
                if (is_array($preProcessArray)) {
                    array_walk($preProcessArray, array(
                        $GLOBALS ['registry']->cleaner,
                        'fixJsonDecodeValues'
                    ));
                }
                $GLOBALS ['registry']->data = $preProcessArray;
                break;

            case "DELETE" :
                $this->_resfullFuncName = 'delete' . ucfirst($this->_mainActionFuncName);
                break;
            default :
                // we could parse other supported formats here
                break;
        }
        return $preProcessArray;
    }

    /**
     * load request for API
     */
    public function loader() {
        $this->loaderCommon();
        // define run actions 
        $beforeAction = 'before' . ucfirst($this->_mainActionFuncName);
        $GLOBALS ['registry']->data = $this->_getDataAccordingToRequestType();
//        $GLOBALS ['registry']->loggingTime = round((microtime() - $GLOBALS ['registry']->loggingTime), 4);
//        $GLOBALS ['registry']->logger->LogInfo("after define function for load in router: " . $GLOBALS ['registry']->loggingTime . " milliseconds");
        $verifiedTenant = false;
        // $GLOBALS ['registry']->session->destroySessionKey($GLOBALS ['registry']->session->getId());

        if ($this->_controllerObject->enableAPIOauth) { // authintication params for API
            $actionKey = $this->controller . ':' . $this->action;
            if (!in_array($actionKey, $this->_controllerObject->apiAuth->allow)) {
                $this->_controllerObject->apiAuth->verifyAccess($actionKey, $this->controller, $this->action);
                $verifiedTenant = true; // the tenant already verified by verify access
//                $GLOBALS ['registry']->loggingTime = round((microtime() - $GLOBALS ['registry']->loggingTime), 4);
//                $GLOBALS ['registry']->logger->LogInfo("verify access: $actionKey " . $GLOBALS ['registry']->loggingTime . " milliseconds");
                unset($actionKey);
            }
        }

        if (!$verifiedTenant) {// there is no verify access for the request we must check the tenant
            $GLOBALS ['registry']->apiAuth->verifyTenant($this->controller, $this->action); //check teant any way and if its enabled get the info     
            //           $GLOBALS ['registry']->loggingTime = round((microtime() - $GLOBALS ['registry']->loggingTime), 4);
            //           $GLOBALS ['registry']->logger->LogInfo("verify tenant $this->_mainActionFuncName : " . $GLOBALS ['registry']->loggingTime . " milliseconds");
            $verifiedTenant = false;
        }
        //remove token from get args
        @$GLOBALS ['registry']->request->deleteGet($this->_controllerObject->apiAuth->tokenVerifyInfoArray['tokenClientName']);
        if (key_exists($this->_controllerObject->apiAuth->tokenVerifyInfoArray['tokenClientName'], $this->_args))
            unset($this->_args[$this->_controllerObject->apiAuth->tokenVerifyInfoArray['tokenClientName']]);
        /**
         * * check if the beforeLoad is callable **
         */
        if (is_callable(array($this->_controllerObject, 'beforeLoad')))
            $this->_controllerObject->beforeLoad();

//        $GLOBALS ['registry']->loggingTime = round((microtime() - $GLOBALS ['registry']->loggingTime), 4);
//        $GLOBALS ['registry']->logger->LogInfo("before load $this->_mainActionFuncName : " . $GLOBALS ['registry']->loggingTime . " milliseconds");
        /**
         * * check if the beforeAction is callable **
         */
        if (is_callable(array($this->_controllerObject, $beforeAction)))
            $this->_controllerObject->$beforeAction();
        /**
         * get Model and set the validation
         */
        // Auto validation
        if (is_array($this->_controllerObject->validateActions))
            if (in_array($this->_mainActionFuncName, $this->_controllerObject->validateActions) || in_array(lcfirst($this->_mainActionFuncName), $this->_controllerObject->validateActions)) {
                $GLOBALS ['registry']->validate->setValdationArray($this->_controllerObject->validateRules);
                $validationData = (sizeof($this->_controllerObject->validateData) > 0) ? $this->_controllerObject->validateData : $GLOBALS ['registry']->data;
                if (sizeof($validationData) > 0) {
                    $validation = $GLOBALS ['registry']->validate->serverValidate($validationData);
                    $this->_controllerObject->validate = (is_array($validation) and !empty($validation)) ? false : true;
                }
            }
//        $GLOBALS ['registry']->loggingTime = round((microtime() - $GLOBALS ['registry']->loggingTime), 4);
//        $GLOBALS ['registry']->logger->LogInfo("validation check $this->_mainActionFuncName  : " . $GLOBALS ['registry']->loggingTime . " milliseconds");
        $this->_mainActionFuncName = lcfirst($this->_mainActionFuncName);

        if ($this->_isRestfull)
            call_user_func_array(array($this->_controllerObject, $this->_resfullFuncName), $this->_args);
        elseIf (is_callable(array($this->_controllerObject, $this->_mainActionFuncName)))
            call_user_func_array(array($this->_controllerObject, $this->_mainActionFuncName), $this->_args);
        else
            $GLOBALS ['registry']->response->sendResponse(Http::HTTP_BAD_REQUEST);
    }

}
