<?php

/**
 * apiAuth, Tidy PHP
 * Enable Authentication and Authorization for apis
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 */
class apiAuth {
    /*
     * var array allow actions without verify in (controller::action) format
     * */

    public $allow = array();
    /*
     * var array relate actions to permissions
     * */
    public $actionsPermissions = array();

    /*
     * var array default redirection action if there is no previous redirect
     * */
    public $redirectAction = array();

    /*
     * var array of request user token get info for permission
     * */
    public $tokenVerifyArray = array();

    /**
     * contain tenenat url 
     */
    public $tenantVerifyURL = array();
    /*
     * token verify info array
     * */
    public $tokenVerifyInfoArray = array();

    public function __construct($allow = array(), $redirectAction = array(), $tokenVerifyArray = array(), $tokenVerifyInfoArray = array()) {
        $this->allow = $allow;
        $this->redirectAction = $redirectAction;
        $this->tokenVerifyArray = $tokenVerifyArray;
        $this->tokenVerifyInfoArray = $tokenVerifyInfoArray;
    }

    /*
     * verify access token
     * @param string access key
     * @param string controller 
     * @param string action
     * return mixed (Json if there is a wrong | void active token)
     * @author Hussam El-Kurd
     * */

    public function verifyAccess($actionKey, $controller, $action) {

        if (!($this->tokenVerifyArray['controller'] == $controller && $this->tokenVerifyArray['action'] == $action)) {
            $tokenStatus = 0;
            $tokenVerifyArray = $this->tokenVerifyInfoArray;
            $token = $this->_getToken();
            if ($token != '') {
                $cashResponse = $GLOBALS['registry']->cache->load($this->_getCasheTitle($token));
                if ($cashResponse == false || $cashResponse['cashTime'] < time()) {
                    //$GLOBALS ['registry']->logger->LogInfo('Without Session');

                    $GLOBALS ['registry']->restRequest->setVerb('GET');
                    $GLOBALS ['registry']->restRequest->setUrl(getURL($this->tokenVerifyArray));
					$GLOBALS ['registry']->restRequest->setCustomeCurlParams($tokenVerifyArray['curlRequestParams']);					
                    $GLOBALS ['registry']->restRequest->setAsync(false);
                    $GLOBALS ['registry']->restRequest->execute();
                    $userResponse = $GLOBALS ['registry']->request->decodeRequestData($GLOBALS ['registry']->restRequest->getResponseBody());

                    //$GLOBALS ['registry']->logger->LogInfo(getURL($this->tokenVerifyArray).$GLOBALS ['registry']->restRequest->getResponseBody());
                    $GLOBALS ['registry']->restRequest->flush();
                    //check token
                    $checkStatus = $this->_checkToken($userResponse);

                    if ($tokenVerifyArray['enableTenant']) {//check tenant
                        $checkStatus = $this->_checkTenant($userResponse) && $checkStatus;
                    }
                    $GLOBALS['registry']->userInfo = $this->_setUserInfoArrayFromResponse($userResponse);
                    if ($checkStatus) {
                        $cashResponse['response'] = $userResponse;
                        $cashResponse['cashTime'] = $userResponse['ttl'] + time();
                        $GLOBALS['registry']->cache->save($cashResponse, $this->_getCasheTitle($token));
                    }
                    //user default lang
                    if ($GLOBALS['registry']->userInfo[$tokenVerifyArray['userClientConfig']] != NULL &&
                            is_array($GLOBALS['registry']->userInfo[$tokenVerifyArray['userClientConfig']]) &&
                            array_key_exists('currentLang', $GLOBALS['registry']->userInfo[$tokenVerifyArray['userClientConfig']])) {
                        App::setDefaultLanguage($GLOBALS['registry']->userInfo[$tokenVerifyArray['userClientConfig']]['currentLang']);
                    }
                } else {
                    $GLOBALS['registry']->userInfo = $this->_setUserInfoArrayFromResponse($cashResponse['response']);
                }

                if (!in_array($GLOBALS['registry']->userInfo[$tokenVerifyArray['userLevelDataKey']], $tokenVerifyArray['allowedLevels']))
                    $this->verifyUserActionPermission(
                            $actionKey, $GLOBALS['registry']->userInfo[$tokenVerifyArray['userPermissionDataKey']], $GLOBALS['registry']->userInfo[$tokenVerifyArray['userLevelDataKey']]);
            }
            elseif ($token == '' && !($this->tenantVerifyURL['controller'] == $controller && $this->tenantVerifyURL['action'] == $action)) {
                $tokenStatus = TOKEN_NOT_FOUND;
                $GLOBALS ['registry']->msg->setMsg($tokenVerifyArray['tokenStatusValues'][$tokenStatus]['message'], ERROR_MSG);
                $this->_responseNotAcceptRequest($tokenStatus);
            }
        }
    }
    /**
     * get token 
     */
    private function _getToken(){
           return $GLOBALS ['registry']->request->getGet($this->tokenVerifyInfoArray['tokenClientName']);       
    }
    /**
     * get cash name for response
     * @param string (optional) $token
     */
    private function _getCasheTitle($token = '') {

        return md5('userResponse' . (($GLOBALS ['registry']->tenantName != NULL) ? $GLOBALS ['registry']->tenantName : '').$token);
    }

    /**
     * verify tenant access if its enabled and 
     * @param string controller
     * @param string action
     * @return int tenant id
     */
    public function verifyTenant($controller, $action) {
        $tokenVerifyArray = $this->tokenVerifyInfoArray;
        $token = $this->_getToken();
        $cashResponse = $GLOBALS['registry']->cache->load($this->_getCasheTitle($token));
        if ($tokenVerifyArray['enableTenant'] &&
                !($this->tenantVerifyURL['controller'] == $controller && $this->tenantVerifyURL['action'] == $action)) {
            if ($cashResponse == false) {
                //$GLOBALS ['registry']->logger->LogInfo('Tenant Without Session');
                $GLOBALS ['registry']->restRequest->setVerb('GET');
                $GLOBALS ['registry']->restRequest->setUrl(getURL($this->tenantVerifyURL));
				$GLOBALS ['registry']->restRequest->setCustomeCurlParams($tokenVerifyArray['curlRequestParams']);				
                $GLOBALS ['registry']->restRequest->setAsync(false);
                $GLOBALS ['registry']->restRequest->execute();
                $userResponse = $GLOBALS ['registry']->request->decodeRequestData($GLOBALS ['registry']->restRequest->getResponseBody());
                //$GLOBALS ['registry']->logger->LogInfo(getURL($this->tenantVerifyURL) . $GLOBALS ['registry']->restRequest->getResponseBody());
            } else
                $userResponse = $cashResponse['response'];
            $this->_checkTenant($userResponse);
            $this->_appendTenantInfoToUserInfoArray($userResponse);
        }
    }

    /**
     * fill tenant response info into user info array 
     * @return array response user with append
     */
    private function _setUserInfoArrayFromResponse($userResponse) {

        $GLOBALS['registry']->userInfo = $userResponse[$this->tokenVerifyInfoArray['userInfoData']];
        if ($this->tokenVerifyInfoArray['enableTenant']) {
            $GLOBALS['registry']->userInfo = $this->_appendTenantInfoToUserInfoArray($userResponse);
        }
        return $GLOBALS['registry']->userInfo;
    }

    /**
     * fill tenant response info into user info array 
     * @return array response user with append
     */
    private function _appendTenantInfoToUserInfoArray($tenantInfo) {
        $tenantResponseArray = array();
        $tenantResponseArray['tenant'] = $tenantInfo['tenant'];
        $tenantResponseArray['federation'] = $tenantInfo['federation'];
        $tenantResponseArray[$this->tokenVerifyInfoArray['tenantTagParam']] = $tenantInfo[$this->tokenVerifyInfoArray['tenantTagParam']];
        $GLOBALS['registry']->userInfo = array_merge($GLOBALS['registry']->userInfo, $tenantResponseArray);
        return $GLOBALS['registry']->userInfo;
    }

    /**
     * check if tenant is avaliable 
     * @param array user respone 
     * @return mixed (boolean | string response)
     */
    private function _checkTenant($userResponse) {
        $tokenVerifyArray = $this->tokenVerifyInfoArray;
        $status = true;
        $tenantStatus = $userResponse[$tokenVerifyArray['tenantClientStatusName']];
        $tenantTagName = @strtolower($userResponse[$tokenVerifyArray['tenantTagParam']]);
        $tenantGetTag = @strtolower($GLOBALS ['registry']->request->getGet($tokenVerifyArray['tenantClientParam']));

        //check if the user info already set
        if (is_array($GLOBALS['registry']->userInfo) && sizeof($GLOBALS['registry']->userInfo) > 0) {
            $tenantStatus = $this->acceptUserNameToTenantTag($GLOBALS['registry']->userInfo['User_Name'], $tenantGetTag, $tenantStatus);
        }
        //check if the tenant status is active 
        if ($tenantStatus != TENANT_ACTIVE) {
            $GLOBALS ['registry']->msg->setMsg($tokenVerifyArray['tenantStatusValues'][$tenantStatus]['message'], ERROR_MSG);
            $status = false;
            $this->_responseNotAcceptRequest('', $tenantStatus);
        }
        return $status;
    }

    /**
     * check user accepted to tenant
     * @param string username 
     * @param string $tenantGetTag 
     * @param int current tenant status
     * return int new tenant status
     */
    public function acceptUserNameToTenantTag($userName, $tenantGetTag, $tenantStatus) {
        //get the tenant tag from user name to check relation with current tenant        
        $tenantSplit = @explode('@', $userName);
        // get the tenant tag from the user name
        $tenantTagName = @strtolower($tenantSplit[1]);
        //the username relate to the current tenant 
        return (@strtolower($tenantGetTag) != $tenantTagName) ? TENANT_WRONG_USER : $tenantStatus;
    }

    /**
     * check if tenant is avaliable 
     * @param array user respone 
     * @return mixed (boolean | string response)
     */
    private function _checkToken($userResponse) {
        $tokenVerifyArray = $this->tokenVerifyInfoArray;
        $status = true;
        $tokenStatus = $userResponse[$tokenVerifyArray['tokenClientStatusName']];
        if ($tokenStatus != TOKEN_ACTIVE) {
            $GLOBALS ['registry']->msg->setMsg($tokenVerifyArray['tokenStatusValues'][$tokenStatus]['message'], ERROR_MSG);
            $status = false;
            $this->_responseNotAcceptRequest($tokenStatus);
        }
        return $status;
    }

    /*
     * check user action permission
     * @param string action key (controller & action)
     * @param array user permission
     * @param int user level 
     * return mixed (Json if there is a wrong | void if true)
     * */

    public function verifyUserActionPermission($actionKey, $userPermission, $level) {
        $tokenVerifyArray = $this->tokenVerifyInfoArray;
        $error = false;
        //level is allowed for this permission
        if (@in_array($level, $GLOBALS ['registry']->apiAuth->actionsPermissions[$actionKey]['allowedLevels']))
            return;
        if (array_key_exists($actionKey, $GLOBALS ['registry']->apiAuth->actionsPermissions)) {
            //$GLOBALS ['registry']->authSetting->actionsPermissions[$actionKey]
            if (is_array($userPermission)) {
                if (sizeof(array_intersect($userPermission, $GLOBALS ['registry']->apiAuth->actionsPermissions[$actionKey])) < 1) {
                    //there is no permission
                    $error = true;
                    $tokenStatus = TOKEN_NO_PERMISSION;
                    $GLOBALS ['registry']->msg->setMsg($tokenVerifyArray['actionPermissionNotAuthorized']['message'], ERROR_MSG);
                }
            } else {
                $error = true;
                $tokenStatus = TOKEN_NO_PERMISSION;
                $GLOBALS ['registry']->msg->setMsg($tokenVerifyArray['actionPermissionNotAuthorized']['message'], ERROR_MSG);
            }
        } else {
            $error = true;
            $tokenStatus = TOKEN_NO_PERMISSION;
            $GLOBALS ['registry']->msg->setMsg($tokenVerifyArray['actionPermissionNotFound']['message'], ERROR_MSG);
        }
        if ($error) {//response if error
            $this->_responseNotAcceptRequest($tokenStatus);
        }
    }

    /*
     * response not accepted tenant
     * @param integer $tokenStatus  
     * @param integer $tenantStatus  
     * @return string json
     * */

    private function _responseNotAcceptRequest($tokenStatus = '', $tenantStatus = '') {
        $tokenVerifyArray = $this->tokenVerifyInfoArray;
        // destroy session if its alive
        $GLOBALS['registry']->cache->save(false, $this->_getCasheTitle());
        $msgs = $GLOBALS ['registry']->msg->getAllMessages();
        $GLOBALS ['registry']->response->sendResponse(\Http::HTTP_OK, array(
            'messages' => $msgs,
            $tokenVerifyArray['tenantClientStatusName'] => $tenantStatus,
            $tokenVerifyArray['tokenClientStatusName'] => $tokenStatus
        ));
        return;
    }

}

class ApiOauthServer extends ApiOauth {

    private $timeStampThreshold = 300; //5 min

    /**
     * check that the timestamp is new enough
     */

    private function checkTimeStamp($timestamp) {
        if (!$timestamp)
            return false;


        // verify that timestamp is recentish
        $now = time();
        if (abs($now - $timestamp) > $this->timeStampThreshold) {
            //$GLOBALS ['registry']->response->sendResponse(Http::HTTP_BAD_REQUEST);
            //return false;

            throw new TidyException(
            "Expired timestamp, yours $timestamp, ours $now"
            );
        } else
            return true;
    }

    public function checkSignature($signature, $timeStamp) {


        if ($signature == parent::generateSignature($this->_userName, $this->_password, $this->_key) && $this->checkTimeStamp($timeStamp)) {
            //Secure
            return true;
        } else
            return false;

        /* 	
          throw new TidyException(
          "Expired timestamp, yours $timeStamp, ours $now"
          );
         */
    }

}

class ApiOauth {

    protected $_key;
    protected $_userName;
    protected $_password;
    private $_hashAlgorithm;

    function __construct($hashAlgorithm, $key, $userName, $password) {

        $this->setKey($key);
        $this->setUserName($userName);
        $this->setPassword($password);
        $this->_hashAlgorithm = $hashAlgorithm;
    }

    /**
     * @return the $key
     */
    public function getKey() {
        return $this->_key;
    }

    /**
     * @return the $useName
     */
    public function getUserName() {
        return $this->_userName;
    }

    /**
     * @return the $password
     */
    public function getPassword() {
        return $this->_password;
    }

    /**
     * @param field_type $key
     */
    public function setKey($key) {
        $this->_key = $key;
    }

    /**
     * @param field_type $useName
     */
    public function setUserName($useName) {
        $this->_userName = $useName;
    }

    /**
     * @param field_type $password
     */
    public function setPassword($password) {
        $this->_password = $password;
    }

    public function generateSignature() {
        return hash_hmac($this->_hashAlgorithm, 'user:' . $this->_userName . '&pass=' . $this->_password, $this->_key);
    }

}
