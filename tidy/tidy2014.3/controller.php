<?php

/**
 * AppController, Tidy PHP 
 * Parent controller for all the controllers in the system
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage App
 * @filesource
 * @abstract
 */
//namespace Tidy;


abstract class BaseController extends TidyException {

    /**
     * related Model
     * @see Model
     * @access public
     * @var string
     */
    public $relatedModel = '';

    /**
     * data array
     * @see AppRequest 
     * @access public
     * @var array
     */
    public $data;

    /**
     * data for custom validation array
     * @access public
     * @var array
     */
    public $validateData = array();

    /**
     * registry object
     * @see AppRegistry 
     * @access public
     * @var object
     */
    public $registry;

    /**
     * Page title
     * @access public
     * @var string
     */
    public $pageTitle;

    /**
     * render option
     * @access public
     * @var boolean
     */
    public static $rendered = false;

    /**
     * enable navigation
     * @access public
     * @var boolean
     */
    public $navigation = false;

    /**
     * css array
     * @access public
     * @var array
     */
    public $css = array('main');

    /**
     * js array
     * @access public
     * @var array
     */
    public $js = array('main');

    /**
     * helpers used in the controller
     * @access public
     * @var array
     */
    public $helpers = array();

    /**
     * helpers used in the controller
     * @access public
     * @var string
     */
    public $layout = 'default';

    /**
     * serverSide Validation Result
     * @access public
     * @var boolean
     */
    public $validate = false;

    /**
     * page encode
     * @access public
     * @var string
     */
    public $encode = 'utf-8';

    /**
     * constructor 
     * @param registry object
     * @access public
     * @return void
     */
    function __construct(Registry $registry = NULL) {
        if (is_object($registry))
            $this->registry = $registry;
        else
            $registry = $GLOBALS['registry'];
    }

    /**
     * iniate all the controller 
     * @access public
     * @return void
     */
    public function ini() { //iniate all the controllers
    }

    /**
     * set variables in global registry
     * @access public	 
     * @param string $index
     * @param mixed $value
     * @return void
     */
    public function __set($index, $value) {
        if (isset($value) and $value != NULL)
            @$this->registry->$index = $value;
    }

    /**
     * get variables from global registry
     * @access public	 
     * @param string $index
     * @return mixed $value
     */
    public function __get($index) {
        return $this->registry->$index;
    }

    /**
     * set variables in the action view
     * @access public	 
     * @param string $index
     * @param mixed $value
     * @return void
     */
    public function set($index, $value) {
        $this->registry->template->$index = $value;
    }

    /**
     * get variable from the view action 
     * @access public	 
     * @param string $index
     * @return mixed $value
     */
    public function get($index) {
        return $this->registry->template->$index;
    }

    /**
     * render specfic  view 
     * @access public	 
     * @param string path
     * @param boolean enable layout	 
     * @return void
     */
    public function render($path, $withoutLayout = TRUE) {
        App::$controller = $this;
        if ($withoutLayout) {
            $this->rendered = TRUE;
            App::import($path, 'view');
        } else
            $this->view = $path;
    }

    /**
     * render json string
     * @access protected	 
     * @param array 
     * @return string json
     */
    protected function renderJson($array) {
        $this->rendered = true;
        echo json_encode($array);
    }

    /**
     * set multi variables in views
     * @access protected	 	 
     * @param array values 
     * @param bool camel var name format 	 
     * @return void
     */
    protected function setMultiVarsInView($values, $camel = true, $debug = false) {
        if (is_object($values))
            $values = get_object_vars($values);
        foreach ($values as $valuKey => $value) {
            $key = ($camel) ? strtocamel($valuKey) : $valuKey;
            if ($debug)
                echo $key . '-->' . $value . '</br>';
            $this->set($key, $value);
        }
    }

    /*
     * update  status or delete 
     * @access protected
     * @param string $actionFunction 
     * @param array $msgKeys(succ,err)
     * @return void
     * */

    protected function changeStatus($model, $actionFunction, $msgs) {
        if (!empty($GLOBALS['registry']->data ['selected'])) {
            $errCounter = $succCounter = 0;
            foreach ($GLOBALS['registry']->data ['selected'] as $value) {
                $condationArray = array_filter(jsonInputDecode($value));
                $model->setCondition($condationArray);
                if (is_array($actionFunction) and array_key_exists('procedure', $actionFunction)) {
                    $return = call_user_func_array(array($GLOBALS['registry']->db, 'exeProcedure'), array($actionFunction['procedure'], $model));
                } else
                    $return = call_user_func_array(array($GLOBALS['registry']->db, $actionFunction), array($model, TRUE));
                if ($return > 0)
                    $succCounter ++;
                else
                    $errCounter ++;
            }
            $msgJson = checkOperationCounters(sizeof($GLOBALS['registry']->data ['selected']), $succCounter, $errCounter, $msgs);
        } elseif ($GLOBALS['registry']->data ['hdchangeid'] != '') {
            $condationArray = jsonInputDecode($GLOBALS['registry']->data ['hdchangeid']);
            $model->setCondition($condationArray);
            if (call_user_func_array(array($GLOBALS['registry']->db, $actionFunction), array($model, TRUE)) > 0)
                $msgJson = array('msg' => $msgs['succ'], 'type' => $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [TIDY_CONSTANTS::LOG_INFO]);
            else
                $msgJson = array('msg' => $msgs['err'], 'type' => $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [TIDY_CONSTANTS::LOG_ERR]);

            $msgJson = sprintf($msgJson['msg'], 1);
        } else
            $msgJson = array('msg' => _tr('you must select at least one item'), 'type' => $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [TIDY_CONSTANTS::LOG_ALERT]);

        $this->renderJson($msgJson);
    }

    /*
     * render result set as json messages format and enable session
     * @access protected
     * @param array error $msgs
     * @param mixed successful message (string or array)
     * @param array json attach more data (optional)
     * @param string session title (optional)
     * @param string session value (optional)
     * */

    protected function renderJsonFinalResult($msgs, $succMessageTitle, $jsonMoreData = null, $sessionTitle = null, $sessionValue = null) {
        if (empty($msgs) and !is_array($msgs)) {
            if ($sessionTitle != '' and $sessionValue != '')
                $GLOBALS ['registry']->session->setSession($sessionTitle, $sessionValue);
            if (is_array($succMessageTitle)) {
                foreach ($succMessageTitle as $msg)
                    $msgJson [] = array(
                        'msg' => $msg,
                        'type' => $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [TIDY_CONSTANTS::LOG_INFO]
                    );
            } else
                $msgJson = array(
                    'msg' => _tr($succMessageTitle),
                    'type' => $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [TIDY_CONSTANTS::LOG_INFO]
                );
            $data ['valid'] = 'true';
        } else {
            foreach ($msgs as $msg)
                $msgJson [] = array(
                    'msg' => $msg,
                    'type' => $GLOBALS ['registry']->constantResources ['clientFlashMessageLogPriority'] [TIDY_CONSTANTS::LOG_ERR]
                );
            $data ['valid'] = 'false';
        }
        $data ['msg'] = $msgJson;
        if (is_array($jsonMoreData) and !empty($jsonMoreData)) {
            $data = array_merge($data, $jsonMoreData);
        }
        $this->renderJson($data);
        return $data ['valid'];
    }

}
