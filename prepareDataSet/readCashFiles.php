<?php

include '../core/ini.php';
/*
  $tweetsHead = md5('tweetsHeadInMac');
  $cashTweets1 = $GLOBALS['registry']->cache->load($tweetsHead);
  $tweetsHead = md5('tweetsHead2');
  $cashTweets2 = $GLOBALS['registry']->cache->load($tweetsHead);
 * 
 */
ini_set('include_path', ini_get('include_path') . ':/Applications/MAMP/htdocs/tweitterPalPol/lib/PHPExcel_1.8.0_doc/Classes/');
/** PHPExcel */
require 'PHPExcel.php';
/** PHPExcel_Writer_Excel2007 */
require 'PHPExcel/Writer/Excel2007.php';

$objPHPExcel = new PHPExcel();

$sheetChars[] = 'A';
$sheetChars[] = 'B';
$sheetChars[] = 'C';
$sheetChars[] = 'D';
$sheetChars[] = 'E';
$sheetChars[] = 'F';
$sheetChars[] = 'G';
$sheetChars[] = 'H';
$sheetChars[] = 'I';
$sheetChars[] = 'J';
$sheetChars[] = 'K';
$sheetChars[] = 'L';
$sheetChars[] = 'M';
$sheetChars[] = 'N';
$sheetChars[] = 'O';
$sheetChars[] = 'P';
$sheetChars[] = 'Q';
$sheetChars[] = 'R';
$sheetChars[] = 'S';
$sheetChars[] = 'T';
$sheetChars[] = 'U';
$sheetChars[] = 'V';



//$objPHPExcel->save('MyExcel.xslx');

$files = scandir(CASH_PATH);
$excelCheetCounter = 2;
$charCounter = 0;

$objPHPExcel->setActiveSheetIndex(0)
        
        ->setCellValue($sheetChars[$charCounter++].'1', 'id_str')
        ->setCellValue($sheetChars[$charCounter++].'1', 'text')
        ->setCellValue($sheetChars[$charCounter++].'1', 'hashtags') 
        ->setCellValue($sheetChars[$charCounter++].'1', 'tweet_location_iso')
        ->setCellValue($sheetChars[$charCounter++].'1', 'tweet_location')
        ->setCellValue($sheetChars[$charCounter++].'1', 'location')
        
        ->setCellValue($sheetChars[$charCounter++].'1', 'citations')       
        ->setCellValue($sheetChars[$charCounter++].'1', 'created_at')
        
        ->setCellValue($sheetChars[$charCounter++].'1', 'user_created_at')
        ->setCellValue($sheetChars[$charCounter++].'1', 'user_id')
        ->setCellValue($sheetChars[$charCounter++].'1', 'screen_name')
        ->setCellValue($sheetChars[$charCounter++].'1', 'verified')
       
        ->setCellValue($sheetChars[$charCounter++].'1', 'profile_location')
        
        ->setCellValue($sheetChars[$charCounter++].'1', 'description')
        ->setCellValue($sheetChars[$charCounter++].'1', 'followers_count')
        ->setCellValue($sheetChars[$charCounter++].'1', 'favourites_count')
                
        ->setCellValue($sheetChars[$charCounter++].'1', 'timestamp_ms')
        ->setCellValue($sheetChars[$charCounter++].'1', 'listed_count')
        ->setCellValue($sheetChars[$charCounter++].'1', 'tweets_count')
        ->setCellValue($sheetChars[$charCounter++].'1', 'time_zone');

        
        

        //->setCellValue('I1', 'profile_location')
foreach ($files as $file) {
    $filePath = CASH_PATH.$file;
    if (is_file($filePath)) {
        $f = @fopen($filePath, 'rb');
        if ($f) {
            $result = stream_get_contents($f);
            @fclose($f);
            $tweetsArray = @unserialize($result);
            if (is_array($tweetsArray) && sizeof($tweetsArray)) {
                foreach ($tweetsArray as $tweetInfo) {
                    if (!in_array($tweetInfo['id_str'], $ids)) {
                        $charCounter = 0;
                        $regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@";
                        $text = preg_replace($regex, ' ', $tweetInfo['text']);
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['id_str'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $text)
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['hashtags'])   
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['tweet_location_iso'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['tweet_location'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['location'])
                                
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['citations'])                                
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['created_at'])
                                
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['user_created_at'])                                
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['user_id'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['screen_name'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['verified'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['profile_location'])
                                
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['description'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['followers_count'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['favourites_count'])
                                //->setCellValue('I' . $excelCheetCounter, $tweetInfo['profile_location'])

                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['timestamp_ms'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['listed_count'])
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['tweets_count'])
                                
                                ->setCellValue($sheetChars[$charCounter++] . $excelCheetCounter, $tweetInfo['time_zone']);
                                
                        
                        $ids[] = $tweetInfo['id_str'];
                        $excelCheetCounter++;
                    }
                }
            }
        }
    }
}

$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

$date = getdate();

$objWriter->save('tweets'.$date['year'].$date['mon'].$date['mday'].'.xlsx');
exit;


