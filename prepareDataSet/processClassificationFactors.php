<?php

include '../core/ini.php';

//ini_set('include_path', ini_get('include_path') . ':/Applications/MAMP/htdocs/tweitterPalPol/lib/PHPExcel_1.8.0_doc/Classes/'); //MAC
ini_set('include_path', ini_get('include_path') . ';' . MAIN_APP_PATH . 'lib/PHPExcel_1.8.0_doc/Classes/');

//$objWriter->save('tweets'.$date['year'].$date['mon'].$date['mday'].'.xlsx');
/** PHPExcel_IOFactory */
require 'PHPExcel.php';

include 'PHPExcel/IOFactory.php';
require 'PHPExcel/Writer/Excel2007.php';

$objPHPExcel = new PHPExcel();
$sheetChars[] = 'A';
$sheetChars[] = 'B';
$sheetChars[] = 'C';
$sheetChars[] = 'D';
$sheetChars[] = 'E';
$sheetChars[] = 'F';
$sheetChars[] = 'G';
$sheetChars[] = 'H';
$sheetChars[] = 'I';
$sheetChars[] = 'J';
$sheetChars[] = 'K';
$sheetChars[] = 'L';
$sheetChars[] = 'M';
$sheetChars[] = 'N';
$sheetChars[] = 'O';
$sheetChars[] = 'P';
$sheetChars[] = 'Q';
$sheetChars[] = 'R';
$sheetChars[] = 'S';
$sheetChars[] = 'T';
$sheetChars[] = 'U';
$sheetChars[] = 'V';

$excelCheetCounter = 2;
$charCounter = 0;
// Classify
$objPHPExcel->setActiveSheetIndex(0)
        //->setCellValue($sheetChars[$charCounter++].'1', 'Classify')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'id')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'text')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'hashtags')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'tweet_location_iso')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'tweet_location')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'location')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'citations')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'created_at')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'user_created_at')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'user_id')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'screen_name')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'verified')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'profile_location')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'description')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'followers_count')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'favourites_count')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'timestamp_ms')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'listed_count')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'tweets_count')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'time_zone')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'Classify');


$inputFileAllDataName = 'prepareTrainingAndTestingDataSet'.DS.'allDataCredit.xlsx';
$inputFileTrainDataName = 'prepareTrainingAndTestingDataSet'.DS.'trainingCreditDataSet2015719.xlsx';

//$inputFileName = 'allData.xlsx';
//echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory to identify the format<br />';
$objPHPExcelLoadAllData = PHPExcel_IOFactory::load($inputFileAllDataName);
$objPHPExcelLoadTrainData = PHPExcel_IOFactory::load($inputFileTrainDataName);


echo '<hr />';

$sheetAllData = $objPHPExcelLoadAllData->getActiveSheet()->toArray(null, true, true, true);
$sheetTrainData = $objPHPExcelLoadTrainData->getActiveSheet()->toArray(null, true, true, true);

for ($i = 0; $i < sizeof($sheetAllData); $i++) {

    if ($sheetAllData[$i]['A'] != null || $sheetAllData[$i]['A'] != "") {
        //echo '<pre>';
        for ($j = 0; $j < sizeof($sheetTrainData); $j++) {
           //echo $sheetAllData[$i]['A'].'---'.$sheetTrainData[$j]['B'].'</br></br>';
            if (trim($sheetAllData[$i]['A']) == trim($sheetTrainData[$j]['A']) ) {//find the same id
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $excelCheetCounter, $sheetAllData[$i]['A'])
                        ->setCellValue('B' . $excelCheetCounter, $sheetAllData[$i]['B'])
                        ->setCellValue('C' . $excelCheetCounter, $sheetAllData[$i]['C'])
                        ->setCellValue('D' . $excelCheetCounter, $sheetAllData[$i]['D'])
                        ->setCellValue('E' . $excelCheetCounter, $sheetAllData[$i]['E'])
                        ->setCellValue('F' . $excelCheetCounter, $sheetAllData[$i]['F'])
                        ->setCellValue('G' . $excelCheetCounter, $sheetAllData[$i]['G'])
                        ->setCellValue('H' . $excelCheetCounter, $sheetAllData[$i]['H'])
                        ->setCellValue('I' . $excelCheetCounter, $sheetAllData[$i]['I'])
                        ->setCellValue('J' . $excelCheetCounter, $sheetAllData[$i]['J'])
                        ->setCellValue('K' . $excelCheetCounter, $sheetAllData[$i]['K'])
                        ->setCellValue('L' . $excelCheetCounter, $sheetAllData[$i]['L'])
                        ->setCellValue('M' . $excelCheetCounter, $sheetAllData[$i]['M'])
                        ->setCellValue('N' . $excelCheetCounter, $sheetAllData[$i]['N'])
                        ->setCellValue('O' . $excelCheetCounter, $sheetAllData[$i]['O'])
                        ->setCellValue('P' . $excelCheetCounter, $sheetAllData[$i]['P'])
                        //->setCellValue('I' . $excelCheetCounter, $tweetInfo['profile_location'])
                        ->setCellValue('Q' . $excelCheetCounter, $sheetAllData[$i]['Q'])
                        ->setCellValue('R' . $excelCheetCounter, $sheetAllData[$i]['R'])
                        ->setCellValue('S' . $excelCheetCounter, $sheetAllData[$i]['S'])
                        ->setCellValue('T' . $excelCheetCounter, $sheetAllData[$i]['T'])
                        ->setCellValue('U' . $excelCheetCounter, $sheetTrainData[$j]['U']);
                        $excelCheetCounter++;
                        break;
            }
        }
    }
}
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

$date = getdate();

$objWriter->save('TrainWithFullDataCredit' . $date['year'] . $date['mon'] . $date['mday'] . '.xlsx');
