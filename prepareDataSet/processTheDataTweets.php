<?php

include '../core/ini.php';

//ini_set('include_path', ini_get('include_path') . ':/Applications/MAMP/htdocs/tweitterPalPol/lib/PHPExcel_1.8.0_doc/Classes/'); //MAC
ini_set('include_path', ini_get('include_path') . ';' . MAIN_APP_PATH . 'lib/PHPExcel_1.8.0_doc/Classes/');

//$objWriter->save('tweets'.$date['year'].$date['mon'].$date['mday'].'.xlsx');
/** PHPExcel_IOFactory */
require 'PHPExcel.php';

include 'PHPExcel/IOFactory.php';
require 'PHPExcel/Writer/Excel2007.php';

$objPHPExcel = new PHPExcel();
$sheetChars[] = 'A';
$sheetChars[] = 'B';
$sheetChars[] = 'C';
$sheetChars[] = 'D';
$sheetChars[] = 'E';
$sheetChars[] = 'F';
$sheetChars[] = 'G';
$sheetChars[] = 'H';
$sheetChars[] = 'I';
$sheetChars[] = 'J';
$sheetChars[] = 'K';
$sheetChars[] = 'L';
$sheetChars[] = 'M';
$sheetChars[] = 'N';
$sheetChars[] = 'O';
$sheetChars[] = 'P';
$sheetChars[] = 'Q';
$sheetChars[] = 'R';
$sheetChars[] = 'S';
$sheetChars[] = 'T';
$sheetChars[] = 'U';
$sheetChars[] = 'V';

$excelCheetCounter = 2;
$charCounter = 0;
// Classify
$objPHPExcel->setActiveSheetIndex(0)
        //->setCellValue($sheetChars[$charCounter++].'1', 'Classify')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'id')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'text')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'hashtags')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'tweet_location_iso')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'tweet_location')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'location')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'citations')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'created_at')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'user_created_at')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'user_id')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'screen_name')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'verified')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'profile_location')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'description')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'followers_count')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'favourites_count')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'timestamp_ms')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'listed_count')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'tweets_count')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'time_zone')
        ->setCellValue($sheetChars[$charCounter++] . '1', 'Classify');


$inputFileName =  'TrainWithFullData2015718.xlsx';
//$inputFileName = 'allData.xlsx';
//echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory to identify the format<br />';
$objPHPExcelLoad = PHPExcel_IOFactory::load($inputFileName);


echo '<hr />';

$sheetData = $objPHPExcelLoad->getActiveSheet()->toArray(null, true, true, true);
for ($i = 0; $i < sizeof($sheetData); $i++) {
    $tweet = $sheetData[$i]['B'];
    $regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@";
    $tweet = preg_replace('/(^|[^a-z0-9_])@([a-z0-9_]+)/i', '', $tweet);
    $tweet = preg_replace($regex, '', $tweet);
    $tweet = (str_replace('RT', '', $tweet));
    if (($sheetData[$i]['A'] != null || $sheetData[$i]['A'] != ""))
       if(($sheetData[$i]['L']) || ($sheetData[$i]['O'] > 134) || ($sheetData[$i]['S'] > 3657)) //verified || follwers
        {
        $charCounter = 0;

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $excelCheetCounter, $sheetData[$i]['A'])
                ->setCellValue('B' . $excelCheetCounter, $tweet)
                ->setCellValue('C' . $excelCheetCounter, $sheetData[$i]['C'])
                ->setCellValue('D' . $excelCheetCounter, $sheetData[$i]['D'])
                ->setCellValue('E' . $excelCheetCounter, $sheetData[$i]['E'])
                ->setCellValue('F' . $excelCheetCounter, $sheetData[$i]['F'])
                ->setCellValue('G' . $excelCheetCounter, $sheetData[$i]['G'])
                ->setCellValue('H' . $excelCheetCounter, $sheetData[$i]['H'])
                ->setCellValue('I' . $excelCheetCounter, $sheetData[$i]['I'])
                ->setCellValue('J' . $excelCheetCounter, $sheetData[$i]['J'])
                ->setCellValue('K' . $excelCheetCounter, $sheetData[$i]['K'])
                ->setCellValue('L' . $excelCheetCounter, $sheetData[$i]['L'])
                ->setCellValue('M' . $excelCheetCounter, $sheetData[$i]['M'])
                ->setCellValue('N' . $excelCheetCounter, $sheetData[$i]['N'])
                ->setCellValue('O' . $excelCheetCounter, $sheetData[$i]['O'])
                ->setCellValue('P' . $excelCheetCounter, $sheetData[$i]['P'])
                //->setCellValue('I' . $excelCheetCounter, $tweetInfo['profile_location'])
                ->setCellValue('Q' . $excelCheetCounter, $sheetData[$i]['Q'])
                ->setCellValue('R' . $excelCheetCounter, $sheetData[$i]['R'])
                ->setCellValue('S' . $excelCheetCounter, $sheetData[$i]['S'])
                ->setCellValue('T' . $excelCheetCounter, $sheetData[$i]['T'])
                ->setCellValue('U' . $excelCheetCounter, $sheetData[$i]['U']);
        $excelCheetCounter++;
    }
}
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

$date = getdate();

$objWriter->save('prepareTrainingAndTestingDataSet'.DS.'trainingCreditDataSet' . $date['year'] . $date['mon'] . $date['mday'] . '.xlsx');
