<?php

/**
 * Storage container for the oauth credentials, both server and consumer side.
 * This is the factory to select the store you want to use
 * 
 * @version $Id: OAuthStore.php 67 2010-01-12 18:42:04Z brunobg@corollarium.com $
 * @author Marc Worrell <marcw@pobox.com>
 * @date  Nov 16, 2007 4:03:30 PM
 * 
 * 
 * The MIT License
 * 
 * Copyright (c) 2007-2008 Mediamatic Lab
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
// require_once dirname(__FILE__) . '/OAuthException.php';
class OAuthStore {

    private static $instance = false;

    /**
     * Maximum delta a timestamp may be off from a previous timestamp.
     * Allows multiple consumers with some clock skew to work with the same token.
     * Unit is seconds, default max skew is 10 minutes.
     */
    protected $max_timestamp_skew = 600;

    /**
     * Default ttl for request tokens
     */
    protected $max_request_token_ttl = 3600;

    public function checkServerNonce($consumer_key, $token, $timestamp, $nonce) { /*
     * $columns= 'MAX(Osn_Timestamp) ,MAX(Osn_Timestamp)>'.$timestamp.'+'. $this->max_timestamp_skew.' ,[Osr_Consumer_Secret]'; $whereCondition='[Osr_Consumer_Key] ='.$consumer_key.' AND [Osn_Token] = '.$token; $rs =$GLOBALS['registry']->OAuthDB->getRecord('[OAuth].[dbo].[Oauth_Server_Nonce]',$columns,$whereCondition,NULL,NULL);
     */
        $statment = ('
							SELECT MAX(Osn_Timestamp)
							FROM ' . OAUTH_NONCE . '
							WHERE Osn_Consumer_Key = \'' . $consumer_key . '\'
							  AND Osn_Token        = \'' . $token . '\' 
					HAVING MAX(Osn_Timestamp) > (' . $timestamp . ' + ' . $this->max_timestamp_skew . ')');
        $result = $GLOBALS ['registry']->OAuthDB->query($statment);
        $result = $this->getResultFromQuery($result);

        if (!empty($result) && $result [1]) {
            throw new OAuthException('Timestamp is out of sequence. Request rejected. Got ' . $timestamp . ' last max is ' . $result [0] . ' allowed skew is ' . $this->max_timestamp_skew);
        }

        // Insert the new combination
        $statment = '
				INSERT  INTO ' . OAUTH_NONCE . '
				( Osn_Consumer_Key,
					Osn_Token			 ,
					Osn_Timestamp		,
					Osn_Nonce		)
				values (\'' . $consumer_key . '\',\'' . $token . '\',\'' . $timestamp . '\',\'' . $nonce . '\')
				';

        $result = $GLOBALS ['registry']->OAuthDB->query($statment);

        if (!$result) {
            throw new OAuthException('Duplicate timestamp/nonce combination, possible replay attack.  Request rejected.');
        }

        // Clean up all timestamps older than the one we just received
        $statment = '
				DELETE FROM ' . OAUTH_NONCE . '
				WHERE [Osn_Consumer_Key]	=\'' . $consumer_key . '\'
				  AND [Osn_Token]			= \'' . $token . '\'
				  AND [Osn_Timestamp]     < ' . $timestamp . '-' . $this->max_timestamp_skew;

        $result = $GLOBALS ['registry']->OAuthDB->query($statment);
    }

    /*     * *
     * add consumer token
     */

    public function addConsumerRequestToken($consumer_key, $options = array()) {
        //$GLOBALS['registry']->OAuthDB->setDebug(TIDY_CONSTANTS::DEBUG_FULL_MODE);
        $resultStatment = $GLOBALS ['registry']->OAuthDB->query('
						SELECT Ost_Token,
							   Ost_Token_TTL
						FROM ' . OAUTH_TOKEN . '
						WHERE Ost_Consumer_Key = \'' . $consumer_key . '\'
					');

        if ($resultStatment)
            $result = $this->getResultFromQuery($resultStatment);

        if ($result !== null) {

            $Ost_Token = $result [0];
            $Ost_Token_TTL = $result [1];

            date_default_timezone_set('Asia/Jerusalem');
            if ($Ost_Token_TTL > date("Y-m-d H:i:s", time())) {

                if (isset($options ['token_ttl']) && is_numeric($options ['token_ttl'])) {
                    $ttl = intval($options ['token_ttl']);
                } else {
                    $ttl = $this->max_request_token_ttl;
                }
                $resultStatment = $GLOBALS ['registry']->OAuthDB->query('
						Update ' . OAUTH_TOKEN . '
						Set [Ost_Timestamp]=GETDATE(),[Ost_Token_TTL]= DATEADD(SECOND, ' . $ttl . ',\'' . $Ost_Token_TTL . '\') ' . ' WHERE Ost_Consumer_Key = \'' . $consumer_key . '\'
					');
                if ($resultStatment)
                    return array(
                        'token' => $Ost_Token
                    );
            }
            $this->deleteConsumerRequestToken($Ost_Token);
            $GLOBALS ['registry']->OAuthDB->beginTR();
            $statatment = '
			INSERT INTO ' . OAUTH_HESTORY . '
					(Oh_Consumer_Key,Oh_Token,Oh_Action,Oh_ActionDate)
					 VALUES (\'' . $consumer_key . '\',\'' . $Ost_Token . '\',' . EXPIRE . ',GETDATE());';

            $resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);
            $GLOBALS ['registry']->OAuthDB->endTR();
        }

        $token = $this->generateKey(true);

        if (isset($options ['token_ttl']) && is_numeric($options ['token_ttl'])) {
            $ttl = intval($options ['token_ttl']);
        } else {
            $ttl = $this->max_request_token_ttl;
        }

        if (!isset($options ['oauth_callback'])) {
            // 1.0a Compatibility : store callback url associated with request token
            $options ['oauth_callback'] = 'oob';
        }
        // print_r($Osr_id);
        $statatment = '
			INSERT INTO ' . OAUTH_TOKEN . '
					(Ost_Consumer_Key,Ost_Token,Ost_Token_TTL,Ost_Timestamp,Ost_Callback_URL)
					 VALUES (\'' . $consumer_key . '\',\'' . $token . '\', DATEADD( SECOND,' . $ttl . ',GETDATE() ),GETDATE(),\'' . $options ['oauth_callback'] . '\');';
        $resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);
        // var_dump($stat);

        if ($resultStatment)
            return array(
                'token' => $token
            );
    }

    /**
     * Fetch the consumer request token, by request token.
     *
     * @param
     *        	string token
     * @return array token and consumer details
     */
    public function getConsumerRequestToken($token) {
        //date_default_timezone_set ( 'Asia/Jerusalem' );
        define('TOKEN_ACTIVE', 1);
        define('TOKEN_EXPIRED', 2);
        define('TOKEN_NOT_FOUND', 3);     
        
        $resultStatment = $GLOBALS ['registry']->OAuthDB->query('
				SELECT	GETDATE()			as time,
						Ost_Token			as token,
						Ost_Consumer_Key	as consumer_key,
						Ost_Callback_URL    as callback_url,
						Ost_Token_TTL       as token_TTL
 					
				FROM ' . OAUTH_TOKEN . '
						
				WHERE  Ost_Token      = \'' . $token . '\'
				'); //AND Ost_Token_TTL >=  GETDATE()	
        $result = $this->getResultFromQuery($resultStatment);

        if (empty($result)) {

            $statatment = $GLOBALS ['registry']->OAuthDB->query('
				SELECT	Oh_Token			as token,
						Oh_Consumer_Key	as consumer_key
		
				FROM ' . OAUTH_HESTORY . '
		
				WHERE  Oh_Token      = \'' . $token . '\'
				  ');
            $tokenStatuse = $this->getResultFromQuery($statatment);
            if (empty($tokenStatuse)) {
                $responseArray['tokenStatus'] = TOKEN_NOT_FOUND;
            } else {
                $responseArray['tokenStatus'] = TOKEN_EXPIRED;
            }
        } else {

            if ($result['token_TTL'] >= $result['time']) {
                $resultStatment = $GLOBALS ['registry']->OAuthDB->query(
                        'Update ' . OAUTH_TOKEN . '
						Set [Ost_Token_TTL ]=DATEADD(second,10,\'' . $result['token_TTL'] . '\')
						WHERE  Ost_Token      = \'' . $token . '\' '
                );
                if ($resultStatment) {
                    $responseArray['result'] = $result;
                    $responseArray['tokenStatus'] = TOKEN_ACTIVE;
                }
            } else {
                $statatment = $GLOBALS ['registry']->OAuthDB->query('
				SELECT	Oh_Token			as token,
						Oh_Consumer_Key	as consumer_key
			
				FROM ' . OAUTH_HESTORY . '
			
				WHERE  Oh_Token      = \'' . $token . '\'
				  ');
                $tokenStatuse = $this->getResultFromQuery($statatment);
                if (empty($tokenStatuse)) {
                    $GLOBALS ['registry']->OAuthDB->beginTR();
                    $statatment = '
			INSERT INTO ' . OAUTH_HESTORY . '
					(Oh_Consumer_Key,Oh_Token,Oh_Action,Oh_ActionDate)
					 VALUES (\'' . $result['consumer_key'] . '\',\'' . $token . '\',' . EXPIRE . ',GETDATE());';

                    $resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);
                    $GLOBALS ['registry']->OAuthDB->endTR();
                }

                $responseArray['tokenStatus'] = TOKEN_EXPIRED;
            }
        }

        return $responseArray;
    }

    /**
     * Fetch the consumer request token, by request token.
     *
     * @param
     *        	string token
     * @return array token and consumer details
     */
    public function getRequestToken($token) {
        $resultStatment = $GLOBALS ['registry']->OAuthDB->query('
				SELECT	Ost_Token			as token,
						Ost_Consumer_Key	as consumer_key,
						Ost_Callback_URL    as callback_url
				FROM ' . OAUTH_TOKEN . '
				WHERE  Ost_Token      = \'' . $token . '\'
				  AND Ost_Token_TTL >=  GETDATE()');

        $result = $this->getResultFromQuery($resultStatment);
        return $result;
    }

    public function verifyConsumerToken($consumer_key, $token) {
        $resultStatment = $GLOBALS ['registry']->OAuthDB->query('
				SELECT	Ost_Token			as token,
						Ost_Consumer_Key	as consumer_key,
						Ost_Callback_URL    as callback_url
 						
				FROM ' . OAUTH_TOKEN . '
						
				WHERE  Ost_Token      = \'' . $token . '\'
				  AND Ost_Consumer_Key= \'' . $consumer_key . '\'
				  AND Ost_Token_TTL  >=  GETDATE()');
        $result = $this->getResultFromQuery($resultStatment);
        if (empty($result)) {
            return false;
        }

        return $result;
    }

    /**
     * Find the consumer request token, by request token.
     *
     * @param
     *        	string token
     * @return array token and consumer details
     */
    public function getConsumerToken($consumer_key) {
        $resultStatment = $GLOBALS ['registry']->OAuthDB->query('
				SELECT		Ost_Token			as token,
						Ost_Consumer_Key	as consumer_key,
						Ost_Callback_URL    as callback_url
 						
				FROM ' . OAUTH_TOKEN . '
						
				WHERE Ost_Consumer_Key= \'' . $consumer_key . '\'
				  AND Ost_Token_TTL  >=  GETDATE()');

        $result = $this->getResultFromQuery($resultStatment);
        return $result;
    }

    /**
     * Delete a consumer token.
     * Delete Token From database 
     *
     * @param
     *        	string token
     */
    public function deleteConsumerRequestToken($token) {
        $GLOBALS ['registry']->OAuthDB->query('
					DELETE FROM ' . OAUTH_TOKEN . '
					WHERE Ost_Token 	 = \'' . $token . '\'')

        ;
    }

    /**
     * Delete a consumer token process.
     * Delete the token from database table and add thats action to history.
     *
     * @param string token
     *        	string consumer key
     *      
     */
    public function deleteConsumerToken($token, $consumer_key) {

        $this->deleteConsumerRequestToken($token);
        $GLOBALS ['registry']->OAuthDB->beginTR();
        $statatment = '
			INSERT INTO ' . OAUTH_HESTORY . '
					(Oh_Consumer_Key,Oh_Token,Oh_Action,Oh_ActionDate)
					 VALUES (\'' . $consumer_key . '\',\'' . $token . '\',' . DELETED . ',GETDATE());';

        $resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);
        $GLOBALS ['registry']->OAuthDB->endTR();
        if ($resultStatment)
            return true;
        else
            return false;
    }

    /**
     * Add an entry to the log table
     *
     * @param array keys (Ost_consumer_key, Ost_token)
     * @param string received
     * @param string sent
     * @param string base_string
     * @param string notes
     * @param int (optional) user_id
     */
    public function addLog($keys, $received, $sent, $base_string, $notes) {
        $args = array();
        $ps = array();
        foreach ($keys as $key => $value) {
            $args[] = '\'' . $value . '\'';
            $ps[] = "Olg_$key ";
        }

        if (!empty($_SERVER['REMOTE_ADDR'])) {
            $remote_ip = $_SERVER['REMOTE_ADDR'];
        } else if (!empty($_SERVER['REMOTE_IP'])) {
            $remote_ip = $_SERVER['REMOTE_IP'];
        } else {
            $remote_ip = '0.0.0.0';
        }

        // Build the SQL
        $ps[] = "Olg_Received";
        $args[] = '\'' . $received . '\'';
        $ps[] = "Olg_Sent";
        $args[] = '\'' . $sent . '\'';
        $ps[] = "Olg_Base_String";
        $args[] = '\'' . $base_string . '\'';
        $ps[] = "Olg_Notes ";
        $args[] = '\'' . $notes . '\'';
        $ps[] = "Olg_Timestamp";
        $args[] = 'GETDATE()';
        $ps[] = "Olg_Remote_IP";
        $args[] = ip2long($remote_ip); //= IFNULL(INET_ATON('%s'),0)
        $statatment = '
			INSERT INTO ' . OAUTH_LOG . '
					(' . implode(',', $ps) . ')
					 VALUES (' . implode(',', $args) . ');';

        $resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);
        if ($resultStatment)
            return true;
        else
            return false;
    }

    /**
     * generate unique key
     *
     * @param string $unique        	
     * @return string
     */
    public function generateKey($unique = false) {
        $key = md5(uniqid(rand(), true));
        if ($unique) {
            list ( $usec, $sec ) = explode(' ', microtime());
            $key .= dechex($usec) . dechex($sec);
        }
        return $key;
    }

    /**
     *
     *
     * get result as array from result query
     * 
     * @param
     *        	$resultStatment
     * @return
     *
     */
    public function getResultFromQuery($resultStatment) {
        $i = 0;
        foreach ($resultStatment as $query) {
            $returnQuery [$i] = $query;
            $i ++;
        }
        if ($i > 1) {
            return $returnQuery;
        } else {
            return $returnQuery [0];
        }
    }

}

/* vi:set ts=4 sts=4 sw=4 binary noeol: */
?>