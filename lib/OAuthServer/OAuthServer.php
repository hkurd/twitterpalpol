<?php

/**
 * Server layer over the OAuthRequest handler
 *
 * @version $Id: OAuthServer.php 154 2010-08-31 18:04:41Z brunobg@corollarium.com $
 * @author Marc Worrell <marcw@pobox.com>
 * @date  Nov 27, 2007 12:36:38 PM
 *
 *
 * The MIT License
 *
 * Copyright (c) 2007-2008 Mediamatic Lab
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//require_once 'OAuthSession.php';

class OAuthServer extends OAuthRequestVerifier {

    //protected $session;

    protected $allowed_uri_schemes = array(
        'http',
        'https'
    );
    protected $disallowed_uri_schemes = array(
        'file',
        'callto',
        'mailto'
    );

    /**
     * Construct the request to be verified
     *
     * @param string request
     * @param string method
     * @param array params The request parameters
     * @param string store The session storage class.
     * @param array store_options The session storage class parameters.
     * @param array options Extra options:
     *   - allowed_uri_schemes: list of allowed uri schemes.
     *   - disallowed_uri_schemes: list of unallowed uri schemes.
     *
     * e.g. Allow only http and https
     * $options = array(
     *     'allowed_uri_schemes' => array('http', 'https'),
     *     'disallowed_uri_schemes' => array()
     * );
     *
     * e.g. Disallow callto, mailto and file, allow everything else
     * $options = array(
     *     'allowed_uri_schemes' => array(),
     *     'disallowed_uri_schemes' => array('callto', 'mailto', 'file')
     * );
     *
     * e.g. Allow everything
     * $options = array(
     *     'allowed_uri_schemes' => array(),
     *     'disallowed_uri_schemes' => array()
     * );
     *
     */
    function __construct($uri = null, $method = null, $params = null, $store = null, $store_options = array(), $options = array()) {
        parent::__construct($uri, $method, $params);
        //$this->session = OAuthSession::instance($store, $store_options);    
          
        if (array_key_exists('allowed_uri_schemes', $options) && is_array($options['allowed_uri_schemes'])) {
            $this->allowed_uri_schemes = $options['allowed_uri_schemes'];
        }
        if (array_key_exists('disallowed_uri_schemes', $options) && is_array($options['disallowed_uri_schemes'])) {
            $this->disallowed_uri_schemes = $options['disallowed_uri_schemes'];
        }
    }

    /**
     * Handle the request_token request.
     * Returns the new request token and request token secret.
     *
     * TODO: add correct result code to exception
     *
     * @return string 	returned request token, false on an error
     */
    public function requestToken() {
        //OAuthRequestLogger::start($this);
        try {
            //$this->verify(false);

            $options = array();
            $ttl = $this->getParam('xoauth_token_ttl', false);

            if ($ttl) {
                $options['token_ttl'] = $ttl;
            }

            // 1.0a Compatibility : associate callback url to the request token
            $cbUrl = $this->getParam('oauth_callback', true);
            if ($cbUrl) {
                $options['oauth_callback'] = $cbUrl;
            }

            // Create a request token
            $store = new OAuthStore();
            //	$r      =$store->getConsumerToken($this->getParam('oauth_consumer_key', true));
            //var_dump($r);
            //if (empty($r)){
            $token = $store->addConsumerRequestToken($this->getParam('oauth_consumer_key', true), $options);
            //var_dump($token);	
            $result = 'oauth_callback_confirmed=1&oauth_token=' . $this->urlencode($token['token']);
            //	.'&oauth_token_secret='.$this->urlencode($token['token_secret']);

            if (!empty($token['token_ttl'])) {
                $result .= '&xoauth_token_ttl=' . $this->urlencode($token['token_ttl']);
            }

            $request_token = $token['token'];

            header('HTTP/1.1 200 OK');
            header('Content-Length: ' . strlen($result));
            header('Content-Type: application/x-www-form-urlencoded');

            echo $result;
            //}
            /* 	else{
              $result = 'oauth_callback_confirmed=1&oauth_token='.$this->urlencode($r['token'])
              .'&oauth_token_secret='.$this->urlencode($r['token_secret']);

              if (!empty($r['token_ttl']))
              {
              $result .= '&xoauth_token_ttl='.$this->urlencode($r['token_ttl']);
              }
              $request_token=$r['token'];
              header('HTTP/1.1 200 OK');
              header('Content-Length: '.strlen($result));
              header('Content-Type: application/x-www-form-urlencoded');

              echo $result;
              } */
        } catch (OAuthException $e) {
            $request_token = false;

            header('HTTP/1.1 401 Unauthorized');
            header('Content-Type: text/plain');

            echo "OAuth Verification Failed: " . $e->getMessage();
        }

        OAuthRequestLogger::flush();
        //print_R($request_token);
        return $result;
    }

    /**
     * Handle the request_token request.
     * @param $consumerKey
     * @param $tokenTime optional
     * Returns the new request token .
     * @return string returned request token, false on an error.
     */
    public function getToken($consumerKey, $tokenTime = null) {
        $this->setParam('oauth_consumer_key', $consumerKey);
        $this->setParam('xoauth_token_ttl', $tokenTime);

        OAuthRequestLogger::start($this);
        try {

            $options = array();
            $options['token_ttl'] = $tokenTime;

            $ttl = $this->getParam('xoauth_token_ttl', false);
            // Create a request token
            $store = new OAuthStore();
            $token = $store->addConsumerRequestToken($this->getParam('oauth_consumer_key', true), $options);

            $request_token = $token['token'];
        } catch (OAuthException $e) {
            echo $e->getMessage();
            return false;
        }
        OAuthRequestLogger::flush();
        return $request_token;
    }

    /**
     * Handle the verify_token request.
     * @param $token
     * @param $nonce
     * @param $timestamp
     * @param $signatureMethod
     * @param $consumerKey
     * @param $oauthSign    
     * @return boolean 	returned true if token valid, false on an error or not valid
     */
    public function verifyToken($token, $nonce, $timestamp, $signatureMethod, $consumerKey, $oauthSign) {
    	$this->setParam('xoauth_token_ttl', "");
    	 
        $this->setParam('oauth_consumer_key', $consumerKey);
        $this->setParam('oauth_token', $token);
        $this->setParam('oauth_nonce', $nonce);
        $this->setParam('oauth_timestamp', $timestamp);
        $this->setParam('oauth_signature_method', $signatureMethod);
        $this->setParam('oauth_signature', $oauthSign);

        OAuthRequestLogger::start($this);
        try {
            $verify = $this->verify('verify');
            if ($verify == 'expaire') {
                return false;
            } else {
                $options = array();
                $ttl = $this->getParam('xoauth_token_ttl', false);

                if ($ttl) {
                    $options['token_ttl'] = $ttl;
                }

                // 1.0a Compatibility : associate callback url to the request token
                $cbUrl = $this->getParam('oauth_callback', true);
                if ($cbUrl) {
                    $options['oauth_callback'] = $cbUrl;
                }

                // verify a request token
                $store = new OAuthStore();
                $r = $store->verifyConsumerToken($consumerKey, $token);

                if (!$r) {
                    return false;
                } else
                    return true;
            }
        } catch (OAuthException $e) {
            echo "OAuth Verification Failed: " . $e->getMessage();
        }

        OAuthRequestLogger::flush();
        return ($r) ? true : false;
    }
    /**
     * Handle the deleteToken request.
     * @param $token
     * @param $nonce
     * @param $timestamp
     * @param $signatureMethod
     * @param $consumerKey
     * @param $oauthSign    
     * @return boolean 	returned true if token deleted, false on an error 
     */
    public function deleteToken($token, $nonce, $timestamp, $signatureMethod, $consumerKey, $oauthSign) {
    	$this->setParam('xoauth_token_ttl', "");
    	 
        $this->setParam('oauth_consumer_key', $consumerKey);
        $this->setParam('oauth_token', $token);
        $this->setParam('oauth_nonce', $nonce);
        $this->setParam('oauth_timestamp', $timestamp);
        $this->setParam('oauth_signature_method', $signatureMethod);
        $this->setParam('oauth_signature', $oauthSign);

        OAuthRequestLogger::start($this);

        $this->verify('delete');


        $options = array();
        // verify a request token
        $store = new OAuthStore();
        $r = $store->deleteConsumerToken($token, $consumerKey);


        OAuthRequestLogger::flush();
        return ($r) ? true : false;
    }

}

/* vi:set ts=4 sts=4 sw=4 binary noeol: */
