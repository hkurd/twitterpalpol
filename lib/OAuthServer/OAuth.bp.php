<?php // 2014.2.0 , Date: 2014-05-28
//namespace OAuth;
class OAuthDataStore {
function lookup_consumer($consumer_key) {
}function lookup_token($consumer, $token_type, $token) {
}function lookup_nonce($consumer, $token, $nonce, $timestamp) {
}function new_request_token($consumer, $callback = null) {
}function new_access_token($token, $consumer, $verifier = null) {
}}class OAuthDiscovery {
static function discover($uri) {
$xrds_file = self::discoverXRDS($uri);if (!empty($xrds_file)) {
$xrds = xrds_parse($xrds_file);if (empty($xrds)) {
throw new OAuthException('Could not discover OAuth information for ' . $uri);}} else {
throw new OAuthException('Could not discover XRDS file at ' . $uri);}$ps = parse_url($uri);$host = isset($ps['host']) ? $ps['host'] : 'localhost';$server_uri = $ps['scheme'] . '://' . $host . '/';
$p = array(
'user_id' => null,'consumer_key' => '','consumer_secret' => '','signature_methods' => '','server_uri' => $server_uri,'request_token_uri' => '','authorize_uri' => '','access_token_uri' => ''
);if (isset($xrds['consumer_identity'])) {
foreach ($xrds['consumer_identity'] as $ci) {
if ($ci['method'] == 'static' && !empty($ci['consumer_key'])) {
$p['consumer_key'] = $ci['consumer_key'];$p['consumer_secret'] = '';} else if ($ci['method'] == 'oob' && !empty($ci['uri'])) {
$p['consumer_oob_uri'] = $ci['uri'];}}}if (isset($xrds['request'][0]['uri'])) {
$p['request_token_uri'] = $xrds['request'][0]['uri'];if (!empty($xrds['request'][0]['signature_method'])) {
$p['signature_methods'] = $xrds['request'][0]['signature_method'];}}if (isset($xrds['authorize'][0]['uri'])) {
$p['authorize_uri'] = $xrds['authorize'][0]['uri'];if (!empty($xrds['authorize'][0]['signature_method'])) {
$p['signature_methods'] = $xrds['authorize'][0]['signature_method'];}}if (isset($xrds['access'][0]['uri'])) {
$p['access_token_uri'] = $xrds['access'][0]['uri'];if (!empty($xrds['access'][0]['signature_method'])) {
$p['signature_methods'] = $xrds['access'][0]['signature_method'];}}return $p;}static protected function discoverXRDS($uri, $recur = 0) {
if ($recur > 10) {
return false;}$data = self::curl($uri);if (is_string($data) && !empty($data)) {
list($head, $body) = explode("\r\n\r\n", $data);$body = trim($body);$m = false;if (preg_match('/^Content-Type:\s*application\/xrds+xml/im', $head) || preg_match('/^<\?xml[^>]*\\s*<xrds\s/i', $body) || preg_match('/^<xrds\s/i', $body)
) {
$xrds = $body;} else if (preg_match('/^X-XRDS-Location:\s*([^\r\n]*)/im', $head, $m) || preg_match('/^Location:\s*([^\r\n]*)/im', $head, $m)) {
if ($uri != $m[1]) {
$xrds = self::discoverXRDS($m[1], $recur + 1);} else {
$xrds = false;}} else {
$xrds = false;}} else {
$xrds = false;}return $xrds;} static protected function curl($uri) {$ch = curl_init(); curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/xrds+xml, */*;q=0.1'));
curl_setopt($ch, CURLOPT_USERAGENT, 'anyMeta/OAuth 1.0 - (OAuth Discovery $LastChangedRevision: 45 $)');curl_setopt($ch, CURLOPT_URL, $uri);curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 curl_setopt($ch, CURLOPT_HEADER, true);curl_setopt($ch, CURLOPT_TIMEOUT, 30);$txt = curl_exec($ch);curl_close($ch);$data = "GET $uri";OAuthRequestLogger::setSent($data, "");
OAuthRequestLogger::setReceived($txt); return $txt;
}}

class OAuthException extends Exception {
function __construct($message) {
Exception::__construct($message);OAuthRequestLogger::addNote('OAuthException: ' . $message);}}class OAuthRequest {
protected $parameters;protected $http_method;protected $http_url;public $base_string;public static $version = '1.0';public static $POST_INPUT = 'php://input';
protected $realm;protected $param = array();protected $uri_parts;protected $uri;protected $headers;protected $method = 'POST';protected $body;function __construct($uri = null, $method = null, $parameters = '', $headers = array(), $body = null) {
if (is_object($_SERVER)) {
if (!$method) {
$method = $_SERVER->REQUEST_METHOD->getRawUnsafe();}if (empty($uri)) {
$uri = $_SERVER->REQUEST_URI->getRawUnsafe();}} else {
if (!$method) {
if (isset($_SERVER['REQUEST_METHOD'])) {
$method = $_SERVER['REQUEST_METHOD'];} else {
$method = 'POST';}}$proto = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';if (empty($uri)) {
if (strpos($_SERVER['REQUEST_URI'], "://") !== false) {
$uri = $_SERVER['REQUEST_URI'];} else {
$uri = sprintf('%s://%s%s', $proto, $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI']);
}}}$headers = OAuthUtil::get_headers();$this->method = strtoupper($method);if (strcasecmp($method, 'POST') == 0) {
if ($this->getRequestContentType() == 'multipart/form-data') {
if (!isset($headers['X-OAuth-Test'])) {
$parameters .= $this->getRequestBodyOfMultipart();}}if ($this->getRequestContentType() == 'application/x-www-form-urlencoded') {
if (!isset($headers['X-OAuth-Test'])) {
$parameters .= $this->getRequestBody();}} else {
$body = $this->getRequestBody();}} else if (strcasecmp($method, 'PUT') == 0) {
$body = $this->getRequestBody();}//print_r($this->parseUri($parameters));
$this->method = strtoupper($method);$this->headers = $headers;$this->uri = $uri;$this->body = $body;if ($parameters != '')
$this->parseUri($parameters);$this->parseHeaders();$this->transcodeParams();}public static function from_request($uri = null, $method = null, $parameters = '', $headers = array(), $body = null) {
if (is_object($_SERVER)) {
if (!$method) {
$method = $_SERVER->REQUEST_METHOD->getRawUnsafe();}if (empty($uri)) {
$uri = $_SERVER->REQUEST_URI->getRawUnsafe();}} else {
if (!$method) {
if (isset($_SERVER['REQUEST_METHOD'])) {
$method = $_SERVER['REQUEST_METHOD'];} else {
$method = 'POST';}}$proto = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';if (empty($uri)) {
if (strpos($_SERVER['REQUEST_URI'], "://") !== false) {
$uri = $_SERVER['REQUEST_URI'];} else {
$uri = sprintf('%s://%s%s', $proto, $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI']);
}}}$headers = OAuthUtil::get_headers();$this->method = strtoupper($method);if (strcasecmp($method, 'POST') == 0) {
if ($this->getRequestContentType() == 'multipart/form-data') {
if (!isset($headers['X-OAuth-Test'])) {
$parameters .= $this->getRequestBodyOfMultipart();}}if ($this->getRequestContentType() == 'application/x-www-form-urlencoded') {
if (!isset($headers['X-OAuth-Test'])) {
$parameters .= $this->getRequestBody();}} else {
$body = $this->getRequestBody();}} else if (strcasecmp($method, 'PUT') == 0) {
$body = $this->getRequestBody();}$this->method = strtoupper($method);$this->headers = $headers;$this->uri = $uri;$this->body = $body;if (parameters != '')
$this->parseUri($parameters);$this->parseHeaders();$this->transcodeParams();}public static function from_consumer_and_token($consumer, $token, $http_method, $http_url, $parameters = NULL) {
$parameters = ($parameters) ? $parameters : array();$defaults = array("oauth_version" => OAuthRequest::$version,"oauth_nonce" => OAuthRequest::generate_nonce(),"oauth_timestamp" => OAuthRequest::generate_timestamp(),"oauth_consumer_key" => $consumer->key);if ($token) {
$defaults['oauth_token'] = $token->key;}$parameters = array_merge($defaults, $parameters);return new OAuthRequest($http_url, $http_method, $parameters);}public function set_parameter($name, $value, $allow_duplicates = true) {
if ($allow_duplicates && isset($this->parameters[$name])) {
if (is_scalar($this->parameters[$name])) {
$this->parameters[$name] = array($this->parameters[$name]);}$this->parameters[$name][] = $value;} else {
$this->parameters[$name] = $value;}}public function get_parameter($name) {
return isset($this->parameters[$name]) ? $this->parameters[$name] : null;}public function get_parameters() {
return $this->parameters;}public function unset_parameter($name) {
unset($this->parameters[$name]);}public function get_signable_parameters() {
$params = $this->parameters;if (isset($params['oauth_signature'])) {
unset($params['oauth_signature']);}return OAuthUtil::build_http_query($params);}public function get_signature_base_string() {
$parts = array(
$this->get_normalized_http_method(),$this->get_normalized_http_url(),$this->get_signable_parameters()
);$parts = OAuthUtil::urlencode_rfc3986($parts);return implode('&', $parts);}public function get_normalized_http_method() {
return strtoupper($this->http_method);}public function get_normalized_http_url() {
$parts = parse_url($this->http_url);$scheme = (isset($parts['scheme'])) ? $parts['scheme'] : 'http';$port = (isset($parts['port'])) ? $parts['port'] : (($scheme == 'https') ? '443' : '80');$host = (isset($parts['host'])) ? $parts['host'] : '';$path = (isset($parts['path'])) ? $parts['path'] : '';if (($scheme == 'https' && $port != '443') || ($scheme == 'http' && $port != '80')) {
$host = "$host:$port";}return "$scheme://$host$path";
}public function to_url() {
$post_data = $this->to_postdata();$out = $this->get_normalized_http_url();if ($post_data) {
$out .= '?' . $post_data;}return $out;}public function to_postdata() {
return OAuthUtil::build_http_query($this->parameters);}public function to_header($realm = null) {
$first = true;if ($realm) {
$out = 'Authorization: OAuth realm="' . OAuthUtil::urlencode_rfc3986($realm) . '"';$first = false;} else
$out = 'Authorization: OAuth';$total = array();foreach ($this->parameters as $k => $v) {
if (substr($k, 0, 5) != "oauth")
continue;if (is_array($v)) {
throw new OAuthException('Arrays not supported in headers');}$out .= ( $first) ? ' ' : ',';$out .= OAuthUtil::urlencode_rfc3986($k) .
'="' .
OAuthUtil::urlencode_rfc3986($v) .
'"';$first = false;}return $out;}public function __toString() {
return $this->to_url();}public function sign_request($signature_method, $consumer, $token) {
$this->set_parameter(
"oauth_signature_method", $signature_method->get_name(), false
);$signature = $this->build_signature($signature_method, $consumer, $token);$this->set_parameter("oauth_signature", $signature, false);}public function build_signature($signature_method, $consumer, $token) {
$signature = $signature_method->build_signature($this, $consumer, $token);return $signature;}private static function generate_timestamp() {
return time();}private static function generate_nonce() {
$mt = microtime();$rand = mt_rand();return md5($mt . $rand);
}public function parse_result($str) {
if (empty($str)) {
throw new OAuthException('error');}$parts = explode('&', $str);$result = array();foreach ($parts as $part) {
list($k, $v) = explode('=', $part, 2);$result[trim(urldecode($k))] = urldecode($v);}if (empty($result)) {
throw new OAuthException('error');}return $result;}function signatureBaseString() {
$sig = array();$sig[] = 'POST';//$sig[]= $this->getRequestUrl();
$sig[] = $this->getNormalizedParams();return implode('&', array_map(array($this, 'urlencode'), $sig));}function calculateSignature($consumer_key, $token_type = 'access') {
$required = array(
'oauth_signature_method','oauth_timestamp','oauth_nonce'
);if ($token_type != 'requestToken') {
$required[] = 'oauth_token';}foreach ($required as $req) {
if (!isset($this->param[$req])) {
throw new OAuthException('Can\'t sign request, missing parameter "' . $req . '"');}}$this->checks();$base = $this->signatureBaseString();$signature = $this->calculateDataSignature($base, $consumer_key, $this->param['oauth_signature_method']);return $signature;}function calculateDataSignature($data, $consumer_key, $signature_method) {
if (is_null($data)) {
$data = '';}$sig = $this->getSignatureMethod($signature_method);return $sig->signature($this, $data, $consumer_key);}public function selectSignatureMethod($methods) {
if (in_array('HMAC-SHA1', $methods)) {
$method = 'HMAC-SHA1';} else if (in_array('MD5', $methods)) {
$method = 'MD5';} else {
$method = false;foreach ($methods as $m) {
$m = strtoupper($m);$m2 = preg_replace('/[^A-Z0-9]/', '_', $m);if (file_exists(dirname(__FILE__) . '/signature_method/OAuthSignatureMethod_' . $m2 . '.php')) {
$method = $m;break;}}if (empty($method)) {
throw new OAuthException('None of the signing methods is supported.');}}return $method;}function getSignatureMethod($method) {
$m = strtoupper($method);$m = preg_replace('/[^A-Z0-9]/', '_', $m);$class = 'OAuthSignatureMethod_' . $m;if (file_exists(dirname(__FILE__) . '/signature_method/' . $class . '.php')) {
//require_once dirname(__FILE__) . '/signature_method/' . $class . '.php';
$sig = new $class();} else {
throw new OAuthException('Unsupported signature method "' . $m . '".');}return $sig;}function checks() {
if (isset($this->param['oauth_version'])) {
$version = $this->urldecode($this->param['oauth_version']);if ($version != '1.0') {
throw new OAuthException('Expected OAuth version 1.0, got "' . $this->param['oauth_version'] . '"');}}}function getMethod() {
return $this->method;}function getNormalizedParams() {
$params = $this->param;$normalized = array();ksort($params);foreach ($params as $key => $value) {
if ($key != 'oauth_signature') {
if (is_array($value)) {
$value_sort = $value;sort($value_sort);foreach ($value_sort as $v) {
if ($v != '')
$normalized[] = $key . '=' . $v;}}elseif ($value != '') {
$normalized[] = $key . '=' . $value;}}}return implode('&', $normalized);}function getRequestUrl() {
$url = $this->uri_parts['scheme'] . '://'
. $this->uri_parts['user'] . (!empty($this->uri_parts['pass']) ? ':' : '')
. $this->uri_parts['pass'] . (!empty($this->uri_parts['user']) ? '@' : '')
. $this->uri_parts['host'];if ($this->uri_parts['port'] && $this->uri_parts['port'] != $this->defaultPortForScheme($this->uri_parts['scheme'])) {
$url .= ':' . $this->uri_parts['port'];}if (!empty($this->uri_parts['path'])) {
$url .= $this->uri_parts['path'];}return $url;}function getParam($name, $urldecode = false) {
if (isset($this->param[$name])) {
$s = $this->param[$name];} else if (isset($this->param[$this->urlencode($name)])) {
$s = $this->param[$this->urlencode($name)];} else {
$s = false;}if (!empty($s) && $urldecode) {
if (is_array($s)) {
$s = array_map(array($this, 'urldecode'), $s);} else {
$s = $this->urldecode($s);}}return $s;}function setParam($name, $value, $encoded = false) {
if (!$encoded) {
$name_encoded = $this->urlencode($name);if (is_array($value)) {
foreach ($value as $v) {
$this->param[$name_encoded][] = $this->urlencode($v);}} else {
$this->param[$name_encoded] = $this->urlencode($value);}} else {
$this->param[$name] = $value;}}protected function transcodeParams() {
$params = $this->param;$this->param = array();foreach ($params as $name => $value) {
if (is_array($value)) {
$this->param[$this->urltranscode($name)] = array_map(array($this, 'urltranscode'), $value);} else {
$this->param[$this->urltranscode($name)] = $this->urltranscode($value);}}}function getBody() {
return $this->body;}function setBody($body) {
$this->body = $body;}protected function parseUri($parameters) {
$ps = @parse_url($this->uri);$ps['scheme'] = strtolower($ps['scheme']);if (function_exists('mb_strtolower'))
$ps['host'] = mb_strtolower($ps['host']);else
$ps['host'] = strtolower($ps['host']);if (!preg_match('/^[a-z0-9\.\-]+$/', $ps['host'])) {
throw new OAuthException('Unsupported characters in host name');}if (empty($ps['port'])) {
$ps['port'] = $this->defaultPortForScheme($ps['scheme']);}if (empty($ps['user'])) {
$ps['user'] = '';}if (empty($ps['pass'])) {
$ps['pass'] = '';}if (empty($ps['path'])) {
$ps['path'] = '/';}if (empty($ps['query'])) {
$ps['query'] = '';}if (empty($ps['fragment'])) {
$ps['fragment'] = '';}//print_r($ps['query']);
foreach (array($ps['query'], $parameters) as $params) {//print_r($params);
if (strlen($params) > 0) {
$params = explode('&', $params);foreach ($params as $p) {
@list($name, $value) = explode('=', $p, 2);if (!strlen($name)) {
continue;}if (array_key_exists($name, $this->param)) {
if (is_array($this->param[$name]))
$this->param[$name][] = $value;else
$this->param[$name] = array($this->param[$name], $value);}else {
$this->param[$name] = $value;}}}}$this->uri_parts = $ps;}protected function defaultPortForScheme($scheme) {
switch ($scheme) {
case 'http': return 80;case 'https': return 443;default:throw new OAuthException('Unsupported scheme type, expected http or https, got "' . $scheme . '"');break;}}function urlencode($s) {
if ($s === false) {
return $s;} else {
return str_replace('%7E', '~', rawurlencode($s));}}function urldecode($s) {
if ($s === false) {
return $s;} else {
return rawurldecode($s);}}function urltranscode($s) {
if ($s === false) {
return $s;} else {
return $this->urlencode(rawurldecode($s));}}private function parseHeaders() {
if (isset($this->headers['Authorization'])) {
$auth = trim($this->headers['Authorization']);if (strncasecmp($auth, 'OAuth', 4) == 0) {
$vs = explode(',', substr($auth, 6));foreach ($vs as $v) {
if (strpos($v, '=')) {
$v = trim($v);list($name, $value) = explode('=', $v, 2);if (!empty($value) && $value{0} == '"' && substr($value, -1) == '"') {
$value = substr(substr($value, 1), 0, -1);}if (strcasecmp($name, 'realm') == 0) {
$this->realm = $value;} else {
$this->param[$name] = $value;}}}}}}private function getRequestContentType() {
$content_type = 'application/octet-stream';if (!empty($_SERVER) && array_key_exists('CONTENT_TYPE', $_SERVER)) {
list($content_type) = explode(';', $_SERVER['CONTENT_TYPE']);}return trim($content_type);}private function getRequestBody() {
$body = null;if ($this->method == 'POST' || $this->method == 'PUT') {
$body = '';$fh = @fopen('php://input', 'r');
if ($fh) {
while (!feof($fh)) {
$s = fread($fh, 1024);if (is_string($s)) {
$body .= $s;}}fclose($fh);}}return $body;}private function getRequestBodyOfMultipart() {
$body = null;if ($this->method == 'POST') {
$body = '';if (is_array($_POST) && count($_POST) > 1) {
foreach ($_POST AS $k => $v) {
$body .= $k . '=' . $this->urlencode($v) . '&';} #end foreach
if (substr($body, -1) == '&') {
$body = substr($body, 0, strlen($body) - 1);} #end if
} #end if
} #end if
return $body;}public function redirect($uri, $params) {
if (!empty($params)) {
$q = array();foreach ($params as $name => $value) {
$q[] = $name . '=' . $value;}$q_s = implode('&', $q);if (strpos($uri, '?')) {
$uri .= '&' . $q_s;} else {
$uri .= '?' . $q_s;}}$uri = preg_replace('/\s/', '%20', $uri);if (strncasecmp($uri, 'http://', 7) && strncasecmp($uri, 'https://', 8)) {
if (strpos($uri, '://')) {
throw new OAuthException('Illegal protocol in redirect uri ' . $uri);}$uri = 'http://' . $uri;
}header('HTTP/1.1 302 Found');header('Location: ' . $uri);echo '';exit();}}class OAuthRequestLogger {
static private $logging = 0;static private $enable_logging = null;static private $store_log = null;static private $note = '';static private $request_object = null;static private $sent = null;static private $received = null;static private $log = array();static function start($request_object = null) {
if (defined('OAUTH_LOG_REQUEST')) {
if (is_null(OAuthRequestLogger::$enable_logging)) {
OAuthRequestLogger::$enable_logging = true;}if (is_null(OAuthRequestLogger::$store_log)) {
OAuthRequestLogger::$store_log = true;}}if (OAuthRequestLogger::$enable_logging && !OAuthRequestLogger::$logging) {
OAuthRequestLogger::$logging = true;OAuthRequestLogger::$request_object = $request_object;ob_start();register_shutdown_function(array('OAuthRequestLogger', 'flush'));}}static function enableLogging($store_log = null) {
OAuthRequestLogger::$enable_logging = true;if (!is_null($store_log)) {
OAuthRequestLogger::$store_log = $store_log;}}static function flush() {
if (OAuthRequestLogger::$logging) {
OAuthRequestLogger::$logging = false;if (is_null(OAuthRequestLogger::$sent)) {
$data = ob_get_contents();if (strlen($data) > 0) {
ob_end_flush();} elseif (ob_get_level()) {
ob_end_clean();}$hs = headers_list();$sent = implode("\n", $hs) . "\n\n" . $data;} else {
$sent = OAuthRequestLogger::$sent;}if (is_null(OAuthRequestLogger::$received)) {
$hs0 = self::getAllHeaders();$hs = array();foreach ($hs0 as $h => $v) {
$hs[] = "$h: $v";}$data = '';$fh = @fopen('php://input', 'r');
if ($fh) {
while (!feof($fh)) {
$s = fread($fh, 1024);if (is_string($s)) {
$data .= $s;}}fclose($fh);}$received = implode("\n", $hs) . "\n\n" . $data;} else {
$received = OAuthRequestLogger::$received;}if (OAuthRequestLogger::$request_object) {
$base_string = OAuthRequestLogger::$request_object->signatureBaseString();} else {
$base_string = '';}$keys = array();if (OAuthRequestLogger::$request_object) {
$consumer_key = OAuthRequestLogger::$request_object->getParam('oauth_consumer_key', true);$token = OAuthRequestLogger::$request_object->getParam('oauth_token', true);switch (get_class(OAuthRequestLogger::$request_object)) {
case 'OAuthServer':case 'OAuthRequestVerifier':$keys['Ost_Consumer_Key'] = $consumer_key;$keys['Ost_Token'] = $token;break;}}if (OAuthRequestLogger::$store_log) {
$store = new OAuthStore();$store->addLog($keys, $received, $sent, $base_string, OAuthRequestLogger::$note);}OAuthRequestLogger::$log[] = array(
'keys' => $keys,'received' => $received,'sent' => $sent,'base_string' => $base_string,'note' => OAuthRequestLogger::$note
);}}static function addNote($note) {
OAuthRequestLogger::$note .= $note . "\n\n";}static function setRequestObject($request_object) {
OAuthRequestLogger::$request_object = $request_object;}static function setSent($request) {
OAuthRequestLogger::$sent = $request;}static function setReceived($reply) {
OAuthRequestLogger::$received = $reply;}static function getLog() {
return OAuthRequestLogger::$log;}public static function getAllHeaders() {
$retarr = array();$headers = array();if (function_exists('apache_request_headers')) {
$headers = apache_request_headers();ksort($headers);return $headers;} else {
$headers = array_merge($_ENV, $_SERVER);foreach ($headers as $key => $val) {
//we need this header
if (strpos(strtolower($key), 'content-type') !== FALSE)
continue;if (strtoupper(substr($key, 0, 5)) != "HTTP_")
unset($headers[$key]);}}//Normalize this array to Cased-Like-This structure.
foreach ($headers AS $key => $value) {
$key = preg_replace('/^HTTP_/i', '', $key);$key = str_replace(
" ", "-", ucwords(strtolower(str_replace(array("-", "_"), " ", $key)))
);$retarr[$key] = $value;}ksort($retarr);return $retarr;}}
class OAuthRequestSigner extends OAuthRequest {
protected $request;protected $store;protected $usr_id = 0;private $signed = false;function __construct($request = null, $method = null, $params = null, $body = null) {
//$this->store = OAuthStore::instance();
if (is_string($params)) {
parent::__construct($request, $method, $params);} elseif ($request != null) {
parent::__construct($request, $method);if (is_array($params)) {
foreach ($params as $name => $value) {
$this->setParam($name, $value);}}}if (strcasecmp($method, 'PUT') == 0 || strcasecmp($method, 'POST') == 0) {
$this->setBody($body);}}function setUnsigned() {
$this->signed = false;}function sign($secrets = null, $name = '', $token_type = null) {
$url = $this->getRequestUrl();if (empty($secrets)) {
throw new OAuthException('No OAuth relation with the server for at "' . $url . '"');}$signature_method = $secrets['oauth_signature_method'];$token = isset($secrets['oauth_token']) ? $secrets['oauth_token'] : '';//$token_secret = isset($secrets['token_secret']) ? $secrets['token_secret'] : '';
if (!$token) {
$token = $this->getParam('oauth_token');}$this->setParam('oauth_token', $token);$this->setParam('oauth_signature_method', $signature_method);$this->setParam('oauth_signature', '');$this->setParam('oauth_nonce', !empty($secrets['oauth_nonce']) ? $secrets['oauth_nonce'] : uniqid(''));$this->setParam('oauth_timestamp', !empty($secrets['oauth_timestamp']) ? $secrets['oauth_timestamp'] : time());$this->setParam('oauth_consumer_key', '');//$this->setParam('oauth_version', '1.0');
$body = $this->getBody();if (!is_null($body)) {
$body_signature = $this->calculateDataSignature($body, $secrets['consumer_key'], $signature_method);$this->setParam('xoauth_body_signature', $body_signature, true);}$signature = $this->calculateSignature($secrets['oauth_consumer_key'], $token_type);$this->setParam('oauth_signature', $signature, true);$this->signed = true;return $signature;}function getAuthorizationHeader() {
if (!$this->signed) {
$this->sign($this->usr_id);}$h = array();$h[] = 'Authorization: OAuth realm=""';foreach ($this->param as $name => $value) {
if (strncmp($name, 'oauth_', 6) == 0 || strncmp($name, 'xoauth_', 7) == 0) {
$h[] = $name . '="' . $value . '"';}}$hs = implode(', ', $h);return $hs;}function getQueryString($oauth_as_header = true) {
$parms = array();foreach ($this->param as $name => $value) {
if (!$oauth_as_header || (strncmp($name, 'oauth_', 6) != 0 && strncmp($name, 'xoauth_', 7) != 0)) {
if (is_array($value)) {
foreach ($value as $v) {
$parms[] = $name . '=' . $v;}} else {
$parms[] = $name . '=' . $value;}}}return implode('&', $parms);}}
class OAuthRequestVerifier extends OAuthRequest {
private $request;private $store;private $accepted_signatures = null;function __construct($uri = null, $method = null, $params = null) {
if ($params) {
$encodedParams = array();foreach ($params as $key => $value) {
if (preg_match("/^oauth_/", $key)) {
continue;}$encodedParams[rawurlencode($key)] = rawurlencode($value);}$this->param = array_merge($this->param, $encodedParams);}$this->store = new OAuthStore();parent::__construct($uri, $method);OAuthRequestLogger::start($this);}static public function requestIsSigned() {
if (isset($_REQUEST['oauth_signature'])) {
$signed = true;} else {
$hs = OAuthRequestLogger::getAllHeaders();if (isset($hs['Authorization']) && strpos($hs['Authorization'], 'oauth_signature') !== false) {
$signed = true;} else {
$signed = false;}}return $signed;}public function verify($token_type = false) {
$retval = $this->verifyExtended($token_type);return $retval;}public function verifyExtended($token_type = false) {
$consumer_key = $this->getParam('oauth_consumer_key');$token = $this->getParam('oauth_token');$secrets = array();if ($consumer_key && ($token_type === false || $token)) {
$this->store->checkServerNonce($this->urldecode($consumer_key), $this->urldecode($token), $this->getParam('oauth_timestamp', true), $this->getParam('oauth_nonce', true));$oauth_sig = $this->getParam('oauth_signature');$this->setParam('oauth_consumer_key', '');if (empty($oauth_sig)) {
throw new OAuthException('Verification of signature failed (no oauth_signature in request).');}try {
$this->verifySignature($consumer_key, $token_type);} catch (OAuthException $e) {
throw new OAuthException('Verification of signature failed (signature base string was "' . $this->signatureBaseString() . '").'
. " with " . print_r(array($consumer_key), true));}if ($this->getParam('xoauth_body_signature')) {
$method = $this->getParam('xoauth_body_signature_method');if (empty($method)) {
$method = $this->getParam('oauth_signature_method');}try {
$this->verifyDataSignature($this->getBody(), $consumer_key, $method, $this->getParam('xoauth_body_signature'));} catch (OAuthException $e) {
throw new OAuthException('Verification of body signature failed.');}}//$ttl = $this->getParam('xoauth_token_ttl', true);
} else {
throw new OAuthException('Can\'t verify request, missing oauth_consumer_key or oauth_token');}return array('consumer_key' => $consumer_key);}public function verifySignature($consumer_key, $token_type = false) {
$required = array(
'oauth_consumer_key','oauth_signature_method','oauth_timestamp','oauth_nonce','oauth_signature'
);foreach ($required as $req) {
if (!isset($this->param[$req])) {
throw new OAuthException('Can\'t verify request signature, missing parameter "' . $req . '"');}}//$this->checks();
$base = $this->signatureBaseString();$this->verifyDataSignature($base, $consumer_key, $this->param['oauth_signature_method'], $this->param['oauth_signature']);}public function verifyDataSignature($data, $consumer_key, $signature_method, $signature) {
if (is_null($data)) {
$data = '';}$sig = $this->getSignatureMethod($signature_method);if (!$sig->verify($this, $data, $consumer_key, $signature)) {
throw new OAuthException('Signature verification failed (' . $signature_method . ')');}}public function setAcceptedSignatureMethods($accepted = null) {
if (is_array($accepted))
$this->accepted_signatures = $accepted;else if ($accepted == null)
$this->accepted_signatures = null;}}
class OAuthRequester extends OAuthRequestSigner {
protected $files;function __construct($request, $method = null, $params = null, $body = null, $files = null) {
parent::__construct($request, $method, $params, $body);if (!empty($files)) {
$empty = true;foreach ($files as $f) {
$empty = $empty && empty($f['file']) && !isset($f['data']);}if (!$empty) {
if (!is_null($body)) {
throw new OAuthException('When sending files, you can\'t send a body as well.');}$this->files = $files;}}}function doRequest($curl_options = array(), $options = array()) {
$name = isset($options['name']) ? $options['name'] : '';if (isset($options['token_ttl'])) {
$this->setParam('xoauth_token_ttl', intval($options['token_ttl']));}if (!empty($this->files)) {
list($extra_headers, $body) = OAuthUtil::encodeBody($this->files);$this->setBody($body);$curl_options = $this->prepareCurlOptions($curl_options, $extra_headers);}$this->sign(null, $name);$text = $this->curl_raw($curl_options);$result = $this->curl_parse($text);if ($result['code'] >= 400) {
throw new OAuthException('Request failed with code ' . $result['code'] . ': ' . $result['body']);}$token_ttl = $this->getParam('xoauth_token_ttl', false);if (is_numeric($token_ttl)) {
$this->store->setServerTokenTtl($this->getParam('oauth_consumer_key', true), $this->getParam('oauth_token', true), $token_ttl);}return $result;}static function requestRequestToken($consumer_key, $params = null, $method = 'POST', $options = array(), $curl_options = array()) {
OAuthRequestLogger::start();if (isset($options['token_ttl']) && is_numeric($options['token_ttl'])) {
$params['xoauth_token_ttl'] = intval($options['token_ttl']);}$store = OAuthStore::instance();$r = $store->getServer($consumer_key);$uri = $r['request_token_uri'];$oauth = new OAuthRequester($uri, $method, $params);OAuthRequestLogger::setRequestObject($oauth);$oauth->sign($r, '', 'requestToken');$text = $oauth->curl_raw($curl_options);if (empty($text)) {
throw new OAuthException('No answer from the server "' . $uri . '" while requesting a request token');}$data = $oauth->curl_parse($text);if ($data['code'] != 200) {
throw new OAuthException('Unexpected result from the server "' . $uri . '" (' . $data['code'] . ') while requesting a request token');}$token = array();$params = explode('&', $data['body']);foreach ($params as $p) {
@list($name, $value) = explode('=', $p, 2);$token[$name] = $oauth->urldecode($value);}if (!empty($token['oauth_token']) && !empty($token['oauth_token_secret'])) {
$opts = array();if (isset($options['name'])) {
$opts['name'] = $options['name'];}if (isset($token['xoauth_token_ttl'])) {
$opts['token_ttl'] = $token['xoauth_token_ttl'];}print_r($token);$store->addServerToken($consumer_key, $token['oauth_token'], $token['oauth_token_secret'], $opts);} else {
throw new OAuthException('The server "' . $uri . '" did not return the oauth_token or the oauth_token_secret');}OAuthRequestLogger::flush();return array(
'token_secret' => $token['oauth_token_secret'],'token' => $token['oauth_token']
);}static function verifyToken($consumer_key, $token, $method = 'POST', $options = array(), $curl_options = array()) {
OAuthRequestLogger::start();if (isset($options['token_ttl']) && is_numeric($options['token_ttl'])) {
$params['xoauth_token_ttl'] = intval($options['token_ttl']);}$store = OAuthStore::instance();$r = $store->getServerToken($consumer_key, $token);if ($r == false) {
echo 'false';}OAuthRequestLogger::flush();return true;}protected function curl_raw($opts = array()) {
if (isset($opts[CURLOPT_HTTPHEADER])) {
$header = $opts[CURLOPT_HTTPHEADER];} else {
$header = array();}$ch = curl_init();$method = $this->getMethod();$url = $this->getRequestUrl();$header[] = $this->getAuthorizationHeader();$query = $this->getQueryString();$body = $this->getBody();$has_content_type = false;foreach ($header as $h) {
if (strncasecmp($h, 'Content-Type:', 13) == 0) {
$has_content_type = true;}}if (!is_null($body)) {
if ($method == 'TRACE') {
throw new OAuthException('A body can not be sent with a TRACE operation');}if (!empty($query)) {
$url .= '?' . $query;}if (!$has_content_type) {
$header[] = 'Content-Type: application/octet-stream';$has_content_type = true;}if ($method == 'PUT') {
$put_file = @tmpfile();if (!$put_file) {
throw new OAuthException('Could not create tmpfile for PUT operation');}fwrite($put_file, $body);fseek($put_file, 0);curl_setopt($ch, CURLOPT_PUT, true);curl_setopt($ch, CURLOPT_INFILE, $put_file);curl_setopt($ch, CURLOPT_INFILESIZE, strlen($body));} else {
curl_setopt($ch, CURLOPT_POST, true);curl_setopt($ch, CURLOPT_POSTFIELDS, $body);}} else {
if ($method == 'POST') {
if (!$has_content_type) {
$header[] = 'Content-Type: application/x-www-form-urlencoded';$has_content_type = true;}curl_setopt($ch, CURLOPT_POST, true);curl_setopt($ch, CURLOPT_POSTFIELDS, $query);} else {
if (!empty($query)) {
$url .= '?' . $query;}if ($method != 'GET') {
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);}}}curl_setopt($ch, CURLOPT_HTTPHEADER, $header);curl_setopt($ch, CURLOPT_USERAGENT, 'anyMeta/OAuth 1.0 - ($LastChangedRevision: 174 $)');curl_setopt($ch, CURLOPT_URL, $url);curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);curl_setopt($ch, CURLOPT_HEADER, true);curl_setopt($ch, CURLOPT_TIMEOUT, 30);foreach ($opts as $k => $v) {
if ($k != CURLOPT_HTTPHEADER) {
curl_setopt($ch, $k, $v);}}$txt = curl_exec($ch);if ($txt === false) {
$error = curl_error($ch);curl_close($ch);throw new OAuthException('CURL error: ' . $error);}curl_close($ch);if (!empty($put_file)) {
fclose($put_file);}$data = $method . " $url\n" . implode("\n", $header);if (is_string($body)) {
$data .= "\n\n" . $body;} else if ($method == 'POST') {
$data .= "\n\n" . $query;}OAuthRequestLogger::setSent($data, $body);OAuthRequestLogger::setReceived($txt);return $txt;}protected function curl_parse($response) {
if (empty($response)) {
return array();}@list($headers, $body) = explode("\r\n\r\n", $response, 2);$lines = explode("\r\n", $headers);if (preg_match('@^HTTP/[0-9]\.[0-9] +100@', $lines[0])) {
@list($headers, $body) = explode("\r\n\r\n", $body, 2);$lines = explode("\r\n", $headers);}$http_line = array_shift($lines);if (preg_match('@^HTTP/[0-9]\.[0-9] +([0-9]{3})@', $http_line, $matches)) {
$code = $matches[1];}$headers = array();foreach ($lines as $l) {
list($k, $v) = explode(': ', $l, 2);$headers[strtolower($k)] = $v;}return array('code' => $code, 'headers' => $headers, 'body' => $body);}protected function prepareCurlOptions($curl_options, $extra_headers) {
$hs = array();if (!empty($curl_options[CURLOPT_HTTPHEADER]) && is_array($curl_options[CURLOPT_HTTPHEADER])) {
foreach ($curl_options[CURLOPT_HTTPHEADER] as $h) {
list($opt, $val) = explode(':', $h, 2);$opt = str_replace(' ', '-', ucwords(str_replace('-', ' ', $opt)));$hs[$opt] = $val;}}$curl_options[CURLOPT_HTTPHEADER] = array();$hs = array_merge($hs, $extra_headers);foreach ($hs as $h => $v) {
$curl_options[CURLOPT_HTTPHEADER][] = "$h: $v";}return $curl_options;}}
//require_once 'OAuthSession.php';
class OAuthServer extends OAuthRequestVerifier {
//protected $session;
protected $allowed_uri_schemes = array(
'http','https'
);protected $disallowed_uri_schemes = array(
'file','callto','mailto'
);function __construct($uri = null, $method = null, $params = null, $store = null, $store_options = array(), $options = array()) {
parent::__construct($uri, $method, $params);//$this->session = OAuthSession::instance($store, $store_options);  
if (array_key_exists('allowed_uri_schemes', $options) && is_array($options['allowed_uri_schemes'])) {
$this->allowed_uri_schemes = $options['allowed_uri_schemes'];}if (array_key_exists('disallowed_uri_schemes', $options) && is_array($options['disallowed_uri_schemes'])) {
$this->disallowed_uri_schemes = $options['disallowed_uri_schemes'];}}public function requestToken() {
//OAuthRequestLogger::start($this);
try {
//$this->verify(false);
$options = array();$ttl = $this->getParam('xoauth_token_ttl', false);if ($ttl) {
$options['token_ttl'] = $ttl;}$cbUrl = $this->getParam('oauth_callback', true);if ($cbUrl) {
$options['oauth_callback'] = $cbUrl;}$store = new OAuthStore();//$r   =$store->getConsumerToken($this->getParam('oauth_consumer_key', true));
//var_dump($r);
//if (empty($r)){
$token = $store->addConsumerRequestToken($this->getParam('oauth_consumer_key', true), $options);//var_dump($token);
$result = 'oauth_callback_confirmed=1&oauth_token=' . $this->urlencode($token['token']);//.'&oauth_token_secret='.$this->urlencode($token['token_secret']);
if (!empty($token['token_ttl'])) {
$result .= '&xoauth_token_ttl=' . $this->urlencode($token['token_ttl']);}$request_token = $token['token'];header('HTTP/1.1 200 OK');header('Content-Length: ' . strlen($result));header('Content-Type: application/x-www-form-urlencoded');echo $result;//}
} catch (OAuthException $e) {
$request_token = false;header('HTTP/1.1 401 Unauthorized');header('Content-Type: text/plain');echo "OAuth Verification Failed: " . $e->getMessage();}OAuthRequestLogger::flush();//print_R($request_token);
return $result;}public function getToken($consumerKey, $tokenTime = null) {
$this->setParam('oauth_consumer_key', $consumerKey);$this->setParam('xoauth_token_ttl', $tokenTime);OAuthRequestLogger::start($this);try {
$options = array();$options['token_ttl'] = $tokenTime;$ttl = $this->getParam('xoauth_token_ttl', false);$store = new OAuthStore();$token = $store->addConsumerRequestToken($this->getParam('oauth_consumer_key', true), $options);$request_token = $token['token'];} catch (OAuthException $e) {
echo $e->getMessage();return false;}OAuthRequestLogger::flush();return $request_token;}public function verifyToken($token, $nonce, $timestamp, $signatureMethod, $consumerKey, $oauthSign) {
$this->setParam('xoauth_token_ttl', "");$this->setParam('oauth_consumer_key', $consumerKey);$this->setParam('oauth_token', $token);$this->setParam('oauth_nonce', $nonce);$this->setParam('oauth_timestamp', $timestamp);$this->setParam('oauth_signature_method', $signatureMethod);$this->setParam('oauth_signature', $oauthSign);OAuthRequestLogger::start($this);try {
$verify = $this->verify('verify');if ($verify == 'expaire') {
return false;} else {
$options = array();$ttl = $this->getParam('xoauth_token_ttl', false);if ($ttl) {
$options['token_ttl'] = $ttl;}$cbUrl = $this->getParam('oauth_callback', true);if ($cbUrl) {
$options['oauth_callback'] = $cbUrl;}$store = new OAuthStore();$r = $store->verifyConsumerToken($consumerKey, $token);if (!$r) {
return false;} else
return true;}} catch (OAuthException $e) {
echo "OAuth Verification Failed: " . $e->getMessage();}OAuthRequestLogger::flush();return ($r) ? true : false;}public function deleteToken($token, $nonce, $timestamp, $signatureMethod, $consumerKey, $oauthSign) {
$this->setParam('xoauth_token_ttl', "");$this->setParam('oauth_consumer_key', $consumerKey);$this->setParam('oauth_token', $token);$this->setParam('oauth_nonce', $nonce);$this->setParam('oauth_timestamp', $timestamp);$this->setParam('oauth_signature_method', $signatureMethod);$this->setParam('oauth_signature', $oauthSign);OAuthRequestLogger::start($this);$this->verify('delete');$options = array();$store = new OAuthStore();$r = $store->deleteConsumerToken($token, $consumerKey);OAuthRequestLogger::flush();return ($r) ? true : false;}}class OAuthStore {
private static $instance = false;protected $max_timestamp_skew = 600;protected $max_request_token_ttl = 3600;public function checkServerNonce($consumer_key, $token, $timestamp, $nonce) { 
$statment = ('
SELECT MAX(Osn_Timestamp)
FROM ' . OAUTH_NONCE . '
WHERE Osn_Consumer_Key = \'' . $consumer_key . '\'
AND Osn_Token = \'' . $token . '\' 
HAVING MAX(Osn_Timestamp) > (' . $timestamp . ' + ' . $this->max_timestamp_skew . ')');$result = $GLOBALS ['registry']->OAuthDB->query($statment);$result = $this->getResultFromQuery($result);if (!empty($result) && $result [1]) {
throw new OAuthException('Timestamp is out of sequence. Request rejected. Got ' . $timestamp . ' last max is ' . $result [0] . ' allowed skew is ' . $this->max_timestamp_skew);}$statment = '
INSERT INTO ' . OAUTH_NONCE . '
( Osn_Consumer_Key,Osn_Token ,Osn_Timestamp,Osn_Nonce)
values (\'' . $consumer_key . '\',\'' . $token . '\',\'' . $timestamp . '\',\'' . $nonce . '\')
';$result = $GLOBALS ['registry']->OAuthDB->query($statment);if (!$result) {
throw new OAuthException('Duplicate timestamp/nonce combination, possible replay attack. Request rejected.');}$statment = '
DELETE FROM ' . OAUTH_NONCE . '
WHERE [Osn_Consumer_Key]=\'' . $consumer_key . '\'
AND [Osn_Token]= \'' . $token . '\'
AND [Osn_Timestamp]   < ' . $timestamp . '-' . $this->max_timestamp_skew;$result = $GLOBALS ['registry']->OAuthDB->query($statment);}public function addConsumerRequestToken($consumer_key, $options = array()) {
//$GLOBALS['registry']->OAuthDB->setDebug(TIDY_CONSTANTS::DEBUG_FULL_MODE);
$resultStatment = $GLOBALS ['registry']->OAuthDB->query('
SELECT Ost_Token,Ost_Token_TTL
FROM ' . OAUTH_TOKEN . '
WHERE Ost_Consumer_Key = \'' . $consumer_key . '\'
');if ($resultStatment)
$result = $this->getResultFromQuery($resultStatment);if ($result !== null) {
$Ost_Token = $result [0];$Ost_Token_TTL = $result [1];date_default_timezone_set('Asia/Jerusalem');if ($Ost_Token_TTL > date("Y-m-d H:i:s", time())) {
if (isset($options ['token_ttl']) && is_numeric($options ['token_ttl'])) {
$ttl = intval($options ['token_ttl']);} else {
$ttl = $this->max_request_token_ttl;}$resultStatment = $GLOBALS ['registry']->OAuthDB->query('
Update ' . OAUTH_TOKEN . '
Set [Ost_Timestamp]=GETDATE(),[Ost_Token_TTL]= DATEADD(SECOND, ' . $ttl . ',\'' . $Ost_Token_TTL . '\') ' . ' WHERE Ost_Consumer_Key = \'' . $consumer_key . '\'
');if ($resultStatment)
return array(
'token' => $Ost_Token
);}$this->deleteConsumerRequestToken($Ost_Token);$GLOBALS ['registry']->OAuthDB->beginTR();$statatment = '
INSERT INTO ' . OAUTH_HESTORY . '
(Oh_Consumer_Key,Oh_Token,Oh_Action,Oh_ActionDate)
VALUES (\'' . $consumer_key . '\',\'' . $Ost_Token . '\',' . EXPIRE . ',GETDATE());';$resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);$GLOBALS ['registry']->OAuthDB->endTR();}$token = $this->generateKey(true);if (isset($options ['token_ttl']) && is_numeric($options ['token_ttl'])) {
$ttl = intval($options ['token_ttl']);} else {
$ttl = $this->max_request_token_ttl;}if (!isset($options ['oauth_callback'])) {
$options ['oauth_callback'] = 'oob';}$statatment = '
INSERT INTO ' . OAUTH_TOKEN . '
(Ost_Consumer_Key,Ost_Token,Ost_Token_TTL,Ost_Timestamp,Ost_Callback_URL)
VALUES (\'' . $consumer_key . '\',\'' . $token . '\', DATEADD( SECOND,' . $ttl . ',GETDATE() ),GETDATE(),\'' . $options ['oauth_callback'] . '\');';$resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);if ($resultStatment)
return array(
'token' => $token
);}public function getConsumerRequestToken($token) {
//date_default_timezone_set ( 'Asia/Jerusalem' );
define('TOKEN_ACTIVE', 1);define('TOKEN_EXPIRED', 2);define('TOKEN_NOT_FOUND', 3);   
$resultStatment = $GLOBALS ['registry']->OAuthDB->query('
SELECT GETDATE() as time,Ost_Token as token,Ost_Consumer_Key as consumer_key,Ost_Callback_URL  as callback_url,Ost_Token_TTL as token_TTL
FROM ' . OAUTH_TOKEN . '
WHERE Ost_Token   = \'' . $token . '\'
'); //AND Ost_Token_TTL >= GETDATE()
$result = $this->getResultFromQuery($resultStatment);if (empty($result)) {
$statatment = $GLOBALS ['registry']->OAuthDB->query('
SELECT Oh_Token as token,Oh_Consumer_Key as consumer_key
FROM ' . OAUTH_HESTORY . '
WHERE Oh_Token   = \'' . $token . '\'
');$tokenStatuse = $this->getResultFromQuery($statatment);if (empty($tokenStatuse)) {
$responseArray['tokenStatus'] = TOKEN_NOT_FOUND;} else {
$responseArray['tokenStatus'] = TOKEN_EXPIRED;}} else {
if ($result['token_TTL'] >= $result['time']) {
$resultStatment = $GLOBALS ['registry']->OAuthDB->query(
'Update ' . OAUTH_TOKEN . '
Set [Ost_Token_TTL ]=DATEADD(second,10,\'' . $result['token_TTL'] . '\')
WHERE Ost_Token   = \'' . $token . '\' '
);if ($resultStatment) {
$responseArray['result'] = $result;$responseArray['tokenStatus'] = TOKEN_ACTIVE;}} else {
$statatment = $GLOBALS ['registry']->OAuthDB->query('
SELECT Oh_Token as token,Oh_Consumer_Key as consumer_key
FROM ' . OAUTH_HESTORY . '
WHERE Oh_Token   = \'' . $token . '\'
');$tokenStatuse = $this->getResultFromQuery($statatment);if (empty($tokenStatuse)) {
$GLOBALS ['registry']->OAuthDB->beginTR();$statatment = '
INSERT INTO ' . OAUTH_HESTORY . '
(Oh_Consumer_Key,Oh_Token,Oh_Action,Oh_ActionDate)
VALUES (\'' . $result['consumer_key'] . '\',\'' . $token . '\',' . EXPIRE . ',GETDATE());';$resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);$GLOBALS ['registry']->OAuthDB->endTR();}$responseArray['tokenStatus'] = TOKEN_EXPIRED;}}return $responseArray;}public function getRequestToken($token) {
$resultStatment = $GLOBALS ['registry']->OAuthDB->query('
SELECT Ost_Token as token,Ost_Consumer_Key as consumer_key,Ost_Callback_URL  as callback_url
FROM ' . OAUTH_TOKEN . '
WHERE Ost_Token   = \'' . $token . '\'
AND Ost_Token_TTL >= GETDATE()');$result = $this->getResultFromQuery($resultStatment);return $result;}public function verifyConsumerToken($consumer_key, $token) {
$resultStatment = $GLOBALS ['registry']->OAuthDB->query('
SELECT Ost_Token as token,Ost_Consumer_Key as consumer_key,Ost_Callback_URL  as callback_url
FROM ' . OAUTH_TOKEN . '
WHERE Ost_Token   = \'' . $token . '\'
AND Ost_Consumer_Key= \'' . $consumer_key . '\'
AND Ost_Token_TTL >= GETDATE()');$result = $this->getResultFromQuery($resultStatment);if (empty($result)) {
return false;}return $result;}public function getConsumerToken($consumer_key) {
$resultStatment = $GLOBALS ['registry']->OAuthDB->query('
SELECT Ost_Token as token,Ost_Consumer_Key as consumer_key,Ost_Callback_URL  as callback_url
FROM ' . OAUTH_TOKEN . '
WHERE Ost_Consumer_Key= \'' . $consumer_key . '\'
AND Ost_Token_TTL >= GETDATE()');$result = $this->getResultFromQuery($resultStatment);return $result;}public function deleteConsumerRequestToken($token) {
$GLOBALS ['registry']->OAuthDB->query('
DELETE FROM ' . OAUTH_TOKEN . '
WHERE Ost_Token = \'' . $token . '\'')
;}public function deleteConsumerToken($token, $consumer_key) {
$this->deleteConsumerRequestToken($token);$GLOBALS ['registry']->OAuthDB->beginTR();$statatment = '
INSERT INTO ' . OAUTH_HESTORY . '
(Oh_Consumer_Key,Oh_Token,Oh_Action,Oh_ActionDate)
VALUES (\'' . $consumer_key . '\',\'' . $token . '\',' . DELETED . ',GETDATE());';$resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);$GLOBALS ['registry']->OAuthDB->endTR();if ($resultStatment)
return true;else
return false;}public function addLog($keys, $received, $sent, $base_string, $notes) {
$args = array();$ps = array();foreach ($keys as $key => $value) {
$args[] = '\'' . $value . '\'';$ps[] = "Olg_$key ";}if (!empty($_SERVER['REMOTE_ADDR'])) {
$remote_ip = $_SERVER['REMOTE_ADDR'];} else if (!empty($_SERVER['REMOTE_IP'])) {
$remote_ip = $_SERVER['REMOTE_IP'];} else {
$remote_ip = '0.0.0.0';}$ps[] = "Olg_Received";$args[] = '\'' . $received . '\'';$ps[] = "Olg_Sent";$args[] = '\'' . $sent . '\'';$ps[] = "Olg_Base_String";$args[] = '\'' . $base_string . '\'';$ps[] = "Olg_Notes ";$args[] = '\'' . $notes . '\'';$ps[] = "Olg_Timestamp";$args[] = 'GETDATE()';$ps[] = "Olg_Remote_IP";$args[] = ip2long($remote_ip); //= IFNULL(INET_ATON('%s'),0)
$statatment = '
INSERT INTO ' . OAUTH_LOG . '
(' . implode(',', $ps) . ')
VALUES (' . implode(',', $args) . ');';$resultStatment = $GLOBALS ['registry']->OAuthDB->query($statatment);if ($resultStatment)
return true;else
return false;}public function generateKey($unique = false) {
$key = md5(uniqid(rand(), true));if ($unique) {
list ( $usec, $sec ) = explode(' ', microtime());$key .= dechex($usec) . dechex($sec);}return $key;}public function getResultFromQuery($resultStatment) {
$i = 0;foreach ($resultStatment as $query) {
$returnQuery [$i] = $query;$i ++;}if ($i > 1) {
return $returnQuery;} else {
return $returnQuery [0];}}}
class OAuthUtil {
public static function urlencode_rfc3986($input) {
if (is_array($input)) {
return array_map(array('OAuthUtil', 'urlencode_rfc3986'), $input);} else if (is_scalar($input)) {
return str_replace(
'+', ' ', str_replace('%7E', '~', rawurlencode($input))
);} else {
return '';}}public static function urldecode_rfc3986($string) {
return urldecode($string);}public static function split_header($header, $only_allow_oauth_parameters = true) {
$params = array();if (preg_match_all('/(' . ($only_allow_oauth_parameters ? 'oauth_' : '') . '[a-z_-]*)=(:?"([^"]*)"|([^,]*))/', $header, $matches)) {
foreach ($matches[1] as $i => $h) {
$params[$h] = OAuthUtil::urldecode_rfc3986(empty($matches[3][$i]) ? $matches[4][$i] : $matches[3][$i]);}if (isset($params['realm'])) {
unset($params['realm']);}}return $params;}public static function get_headers() {
if (function_exists('apache_request_headers')) {
$headers = apache_request_headers();$out = array();foreach ($headers AS $key => $value) {
$key = str_replace(
" ", "-", ucwords(strtolower(str_replace("-", " ", $key)))
);$out[$key] = $value;}} else {
$out = array();if (isset($_SERVER['CONTENT_TYPE']))
$out['Content-Type'] = $_SERVER['CONTENT_TYPE'];if (isset($_ENV['CONTENT_TYPE']))
$out['Content-Type'] = $_ENV['CONTENT_TYPE'];foreach ($_SERVER as $key => $value) {
if (substr($key, 0, 5) == "HTTP_") {
$key = str_replace(
" ", "-", ucwords(strtolower(str_replace("_", " ", substr($key, 5))))
);$out[$key] = $value;}}}return $out;}public static function parse_parameters($input) {
if (!isset($input) || !$input)
return array();$pairs = explode('&', $input);$parsed_parameters = array();foreach ($pairs as $pair) {
$split = explode('=', $pair, 2);$parameter = OAuthUtil::urldecode_rfc3986($split[0]);$value = isset($split[1]) ? OAuthUtil::urldecode_rfc3986($split[1]) : '';if (isset($parsed_parameters[$parameter])) {
if (is_scalar($parsed_parameters[$parameter])) {
$parsed_parameters[$parameter] = array($parsed_parameters[$parameter]);}$parsed_parameters[$parameter][] = $value;} else {
$parsed_parameters[$parameter] = $value;}}return $parsed_parameters;}public static function build_http_query($params) {
if (!$params)
return '';$keys = OAuthUtil::urlencode_rfc3986(array_keys($params));$values = OAuthUtil::urlencode_rfc3986(array_values($params));$params = array_combine($keys, $values);uksort($params, 'strcmp');$pairs = array();foreach ($params as $parameter => $value) {
if (is_array($value)) {
sort($value, SORT_STRING);foreach ($value as $duplicate_value) {
$pairs[] = $parameter . '=' . $duplicate_value;}} else {
$pairs[] = $parameter . '=' . $value;}}return implode('&', $pairs);}static function encodeBody($files) {
$headers = array();$body = null;if (!empty($files)) {
foreach ($files as $name => $f) {
$data = false;$filename = false;if (isset($f['filename'])) {
$filename = $f['filename'];}if (!empty($f['file'])) {
$data = @file_get_contents($f['file']);if ($data === false) {
throw new OAuthException(sprintf('Could not read the file "%s" for request body', $f['file']));}if (empty($filename)) {
$filename = basename($f['file']);}} else if (isset($f['data'])) {
$data = $f['data'];}if ($data !== false) {
if (isset($headers['Content-Disposition'])) {
throw new OAuthException('Only a single file (or data) allowed in a signed PUT/POST request body.');}if (empty($filename)) {
$filename = 'untitled';}$mime = !empty($f['mime']) ? $f['mime'] : 'application/octet-stream';$headers['Content-Disposition'] = 'attachment; filename="' . OAuthUtil::encodeParameterName($filename) . '"';$headers['Content-Type'] = $mime;$body = $data;}}if (!is_null($body)) {
$headers['Content-Length'] = strlen($body);}}return array($headers, $body);}static function encodeParameterName($name) {
return preg_replace('/[^\x20-\x7f]|"/', '-', $name);}}
//version no
define('OAUTH_VERSION', '2014.2.0');abstract class OAuthSignatureMethod
{
abstract public function name();abstract public function signature ( $request, $base_string, $consumer_secret, $token_secret );abstract public function verify ( $request, $base_string, $consumer_secret, $token_secret, $signature );}
class OAuthSignatureMethod_HMAC_SHA1 //extends OAuthSignatureMethod
{
public function name ()
{
return 'HMAC-SHA1';}function signature ( $request, $base_string,$consumer_key )
{
$key = $request->urlencode($consumer_key);if (function_exists('hash_hmac'))
{
$signature = base64_encode(hash_hmac("sha1", $base_string, $key, true));}else
{
$blocksize= 64;$hashfunc= 'sha1';if (strlen($key) > $blocksize)
{
$key = pack('H*', $hashfunc($key));}$key= str_pad($key,$blocksize,chr(0x00));$ipad= str_repeat(chr(0x36),$blocksize);$opad= str_repeat(chr(0x5c),$blocksize);$hmac = pack(
'H*',$hashfunc(
($key^$opad).pack(
'H*',$hashfunc(
($key^$ipad).$base_string
)
)
)
);$signature = base64_encode($hmac);}return $request->urlencode($signature);}public function verify ( $request, $base_string,$consumer_key, $signature )
{
$a = $request->urldecode($request->urldecode($signature));$b = $request->urldecode($this->signature($request, $base_string,$consumer_key));$valA = base64_decode($a);$valB = base64_decode($b);return rawurlencode($valA) == rawurlencode($valB);}}
class OAuthSignatureMethod_MD5 //extends OAuthSignatureMethod
{
public function name ()
{
return 'MD5';}function signature ( $request, $base_string,$consumer_key )
{
$s .= '&'.$request->urlencode($consumer_key);$md5 = md5($base_string);$bin = '';for ($i = 0; $i < strlen($md5); $i += 2)
{
$bin .= chr(hexdec($md5{$i+1}) + hexdec($md5{$i}) * 16);}return $request->urlencode(base64_encode($bin));}public function verify ( $request, $base_string, $consumer_key, $signature )
{
$a = $request->urldecode($request->urldecode($signature));$b = $request->urldecode($this->signature($request, $base_string, $consumer_key));$valA = base64_decode($a);$valB = base64_decode($b);return rawurlencode($valA) == rawurlencode($valB);}}
class OAuthSignatureMethod_PLAINTEXT //extends OAuthSignatureMethod
{
public function name ()
{
return 'PLAINTEXT';}function signature ( $request, $base_string, $consumer_key)
{
return $request->urlencode($consumer_key);}public function verify ( $request, $base_string, $consumer_key, $signature )
{
$a = $request->urldecode($signature);$b = $request->urldecode($this->signature($request, $base_string, $consumer_key));return $request->urldecode($a) == $request->urldecode($b);}}
class OAuthSignatureMethod_RSA_SHA1 //extends OAuthSignatureMethod
{
public function name() 
{
return 'RSA-SHA1';}protected function fetch_public_cert ( $request )
{
//
throw OAuthException("OAuthSignatureMethod_RSA_SHA1::fetch_public_cert not implemented");}protected function fetch_private_cert ( $request )
{
//
throw OAuthException("OAuthSignatureMethod_RSA_SHA1::fetch_private_cert not implemented");}public function signature ( $request, $base_string, $consumer_key )
{
$cert = $this->fetch_private_cert($request);$privatekeyid = openssl_get_privatekey($cert);$sig = false;$ok = openssl_sign($base_string, $sig, $privatekeyid);  
openssl_free_key($privatekeyid);return $request->urlencode(base64_encode($sig));}public function verify ( $request, $base_string, $consumer_key, $signature )
{
$decoded_sig = base64_decode($request->urldecode($signature));$cert = $this->fetch_public_cert($request);$publickeyid = openssl_get_publickey($cert);$ok = openssl_verify($base_string, $decoded_sig, $publickeyid);  
openssl_free_key($publickeyid);return $ok == 1;}}