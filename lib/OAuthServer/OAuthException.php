<?php

require_once dirname(__FILE__) . '/OAuthRequestLogger.php';

class OAuthException extends Exception {

    function __construct($message) {
        Exception::__construct($message);
        OAuthRequestLogger::addNote('OAuthException: ' . $message);
    }

}
