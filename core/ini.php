<?php
/**
 * initiator, Tidy PHP 
 * iniate the system according to request
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @author Hussam El-Kurd <smartx86@gmail.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage Setting
 * @filesource
 */
//ob_start("ob_gzhandler");

error_reporting(/*E_ALL & ~ E_NOTICE & ~ E_WARNING*/E_ALL & ~ E_NOTICE & ~ E_WARNING);
define('DS', strstr(PHP_OS, 'WIN') ? '\\' : '/');
//define('MAIN_APP_PATH','/Applications/MAMP/htdocs/tweitterPalPol/');//MAC ,,,,
define('MAIN_APP_PATH',realpath(dirname(dirname(__FILE__))).DS);//Windows 
define('CASH_PATH', MAIN_APP_PATH.'cash'.DS);
define('CASH_VAR_INTERVAL', 7200);


set_include_path(
          MAIN_APP_PATH.'tidy'.DS.'tidy2014.3'.DS. PATH_SEPARATOR 
         .MAIN_APP_PATH.'lib'.DS.PATH_SEPARATOR 
        . get_include_path());

include_once ('tidy.compress.php');


/**
 * @var object registry
 * @see AppRegistry
 */
$GLOBALS ['registry'] = Registry::createRegistry();


/*
 * set the loader configuration for zend library
 */
include_once ( 'Zend' . DS . 'Loader.php' );
/**
 * Load Zend Lib Fsiles
 */
Zend_Loader::loadClass('Zend_Acl');
Zend_Loader::loadClass('Zend_Exception');
Zend_Loader::loadClass('Zend_Cache');

/**
 * @var messages
 * @see message
 */
$GLOBALS ['registry']->msg = new Message ();

/**
 * @var object cleaner for cleaning string and request
 * @GLOBAL 
 * @see Cleaner
 */
$GLOBALS ['registry']->cleaner = new Cleaner ();

/**
 * @var object request
 * @see Request
 */
$requestReg = Request::createRequestRegistry();

/**
 * @var object restRequest
 * @see restRequest
 */
$restRequest = new restRequest();
/**
 * @var object response
 * @see Response
 */
$responseReg = Response::createResponseRegistry();

/**
 * @var object validation
 * @GLOBAL 
 * @see Validate
 * define the validation ob in the registry
 */
$GLOBALS ['registry']->validate = new Validate($requestReg);

/**
 * @var object Request
 * @GLOBAL
 * @see Request
 * define request in registry
 */
$GLOBALS ['registry']->request = $requestReg;
 /* *
 * @var object Request
 * @GLOBAL
 * @see Request
 * define response in registry
 */
$GLOBALS ['registry']->response = $responseReg;
/**
 * load up the router
 */
$GLOBALS ['registry']->router = new RouterApi ();



$GLOBALS ['registry']->cache = App::getCash(CASH_VAR_INTERVAL, CASH_PATH );

/**
 * @var object todayDateTime
 * @GLOBAL
 * current Date time
 */
$GLOBALS ['registry']->todayDateTime = date(DATE_DB, time());

