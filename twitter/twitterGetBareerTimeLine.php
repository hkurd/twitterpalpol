<?php
$consumer_key = ($_GET['consumer_key']) ? $_GET['consumer_key'] : 'iDty1ZuesU2N3UltjelGp3bgD';
$consumer_secret = ($_GET['consumer_secret']) ? $_GET['consumer_secret'] : 'rkQn8b39aZMXI9ROwmbxQQF5PUUBH2CKoGpwuQEnb2ZHuJYtPX';
$screen_name = ($_GET['screen_name']) ? $_GET['screen_name'] : 'hussamkurd';
$count = ($_GET['count']) ? $_GET['count'] : 5;
$bearer_token = '';

function requestString($op) {
    $headers = array();

    // custom headers?   
    if (is_array($op['headers'])) {
        foreach ($op['headers'] as &$value)
            $headers[count($headers)] = $value;
    };

    $ch = curl_init();

    // post?
    if (!empty($op['post'])) {
        $headers[count($headers)] = 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8';
        $headers[count($headers)] = 'Content-Length: ' . (strlen($op['post']));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $op['post']); // post vars
    };

    curl_setopt($ch, CURLOPT_URL, $op['url']); // request URL
    curl_setopt($ch, CURLOPT_HTTPHEADER, $op['headers']); // custom headers
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip'); // accept gzip to speedup request
    // if you are testing this script locally on windows server, you might need to set CURLOPT_SSL_VERIFYPEER to 'false' to skip errors with SSL verification
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true); // verifying the peer's certificate

    $result = curl_exec($ch); // execute
    curl_close($ch);

    return $result;
}

function get_bearer_token() {
    global $bearer_token, $consumer_key, $consumer_secret;

    $token_cred = urlencode($consumer_key) . ':' . urlencode($consumer_secret);
    $base64_token_cred = base64_encode($token_cred);

    $result = requestString(array(
        'url' => 'https://api.twitter.com/oauth2/token'
        , 'post' => 'grant_type=client_credentials'
        , 'headers' => array('Authorization: Basic ' . $base64_token_cred)
            ));
    $json = json_decode($result);

    $bearer_token = $json->{'access_token'};
    return $bearer_token;
}

function users_show() {
    global $_GET, $bearer_token, $screen_name, $count;
    $bearer_token = ($_GET['bearer_token']) ? $_GET['bearer_token'] : get_bearer_token();

    return requestString(array(
        'url' => 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=' . urlencode($screen_name) . '&count=' . $count
        , 'headers' => array('Authorization: Bearer ' . $bearer_token)
            ));
}

function cache_start() {
    global $cache_file, $cache_time;

    // Serve from the cache if it is less than $cache_time
    if (file_exists($cache_file) && ( time() - $cache_time < filemtime($cache_file) )) {
        // custom header for testing purpose
        header('x-cached: ' . date('Y-m-d H:i:s', filemtime($cache_file)));

        include($cache_file);

        cache_end();

        exit;
    }

    ob_start(); // start the output buffer
}

function cache_end() {
    global $cache_file;

    // open the cache file for writing
    $fp = fopen($cache_file, 'w');
    // save the contents of output buffer to the file
    fwrite($fp, ob_get_contents());
    // close the file
    fclose($fp);
    // Send the output to the browser
    ob_end_flush();
}
//cache_start();
//print '[';
print users_show(); // get user data

//print ',';
//print user_timeline(); // Get user timeline
//print ']';