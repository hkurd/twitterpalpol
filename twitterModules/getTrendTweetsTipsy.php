<?php

include '../core/ini.php';
$date = getdate();
$tweetsHead = md5('tweetsHeadInMac' . $date['year'] . $date['mon'] . $date['mday'] . $date['seconds']. '_bulk');
$tweetsHeadBack = md5('tweetsHeadInMac' . $date['year'] . $date['mon'] . $date['mday']. $date['seconds'] . '_bulkBack');


//$GLOBALS['registry']->cache->save(array(), $tweetsHead);
$max_id = null;
$extraParam = '';

include 'prepareTwitterOAuth.php';
echo '<pre>';



//error_reporting(E_ALL);
$todayDate = getdate();

function checkTweetAlreadyCashed($id) {
    global $tweetsHead;
    $oldCashedTweets = $GLOBALS['registry']->cache->load($tweetsHead);
    if (is_array($oldCashedTweets) && sizeof($oldCashedTweets) > 0) {

        foreach ($oldCashedTweets as $key => $value) {
            if ($id == $value['id'])
                return true;
        }
        return false;
    }
}

$arrayOFAPIWords[] = 'فلسطين';
$arrayOFAPIWords[] = 'غزة';
$arrayOFAPIWords[] = 'الصهانية';
$arrayOFAPIWords[] = 'الضفة الغربية';
$arrayOFAPIWords[] = 'فلسطين المحتلة';
$arrayOFAPIWords[] = 'يهود';
$arrayOFAPIWords[] = 'القدس';
$arrayOFAPIWords[] = 'إسرائيل';
$arrayOFAPIWords[] = 'قطاع غزة';
$arrayOFAPIWords[] = 'عملية طعن';
$arrayOFAPIWords[] = 'كتائب القسام';
$arrayOFAPIWords[] = 'كتائب الأقصى';
$arrayOFAPIWords[] = 'سرايا القدس';
$arrayOFAPIWords[] = 'الرئيس أبو مازن';
$arrayOFAPIWords[] = 'هنية';
$arrayOFAPIWords[] = 'نتنياهو';
$arrayOFAPIWords[] = 'دحلان';
//$arrayOFAPIWords[] = 'الحرب';
$arrayOFAPIWords[] = 'معبر رفح';
$arrayOFAPIWords[] = 'السلطة الفلسطينية';
$arrayOFAPIWords[] = 'حماس';
$arrayOFAPIWords[] = 'محمد الضيف';
$arrayOFAPIWords[] = 'الأسرى';
$arrayOFAPIWords[] = 'قناة الأقصى';
$arrayOFAPIWords[] = 'الجهاد الإسلامي';
$arrayOFAPIWords[] = 'المفاوضات';
$arrayOFAPIWords[] = 'ميناء غزة';
$arrayOFAPIWords[] = 'محمد عساف';
$arrayOFAPIWords[] = 'العصف المأكول';
$arrayOFAPIWords[] = 'حرب الفرقان';
$arrayOFAPIWords[] = 'الرصاص المصبوب';
$arrayOFAPIWords[] = 'الجرف الصامد';

//&mintime=1370217600&maxtime=1370893329
$preparedQWords = 'q=';
$sizeOfWords = sizeof($arrayOFAPIWords);
$sizeOfWordsCounter = 0;

foreach ($arrayOFAPIWords as $word) {

    $word = str_replace(' ', '%20', $word);
    if (strstr($word, '%20')) {

        $word = '%22' . $word . '%22';
    }
    $word = $word . '%20';
    $preparedQWords.= $word . (($sizeOfWordsCounter == ($sizeOfWords - 1)) ? '' : 'OR%20');
    $sizeOfWordsCounter ++;
}
$dateQuery = '';
//for($distanceTweet=1000 ; $distanceTweet<5000 ; $distanceTweet = $distanceTweet*2)geocode=31.603726,34.911230,'.$distanceTweet.'km&
//$q = 'q=فلسطين%20OR%20غزة%20OR%20الصهاينة%20OR%20%22الضفة%20الغربية%22%20OR%20%22فلسطين%20المحتلة%22%20OR%20يهود%20OR%20القدس%20OR%20إسرائيل%20OR%20%22قطاع%20غزة%22%20until%3A' . $todayDate['year'] . '-' . $todayDate['mon'] . '-' . ($todayDate['mday'] - $tweetDateCounter);


$q = $preparedQWords . $dateQuery;

for($year =0; $year < 8; $year++){
$maxDate = (mktime(date("H"), date("i"), date("s"), date("n"), date("j"), 2015-$year));

if($maxDate != '')
    $extraParam .= '&maxtime='.$maxDate;
// 'http://api.topsy.com/v2/content/bulktweets.json?' . $q . '&limit=5&apikey=09C43A9B270A470B8EB8F2946A9369F3&include_enrichment_all=1' . $extraParam;
$tweets = $GLOBALS['twitterOauth']->http('http://api.topsy.com/v2/content/bulktweets.json?' . $q . '&limit=80&apikey=09C43A9B270A470B8EB8F2946A9369F3&include_enrichment_all=1' . $extraParam, 'GET');
//$GLOBALS['registry']->cache->save($tweets, $tweetsHeadBack);
//var_dump($tweets);
$tweetsArray = explode("\n", $tweets);
$tweetsBulkArray = [];
$tweetsCount = sizeof($tweetsArray);
$i = 0;

if (is_array($tweetsArray) && $tweetsArray) {
    foreach ($tweetsArray as $tweet) {
        $tweetDecoded = json_decode($tweet, true);
        //var_dump($tweetDecoded);

        if (is_array($tweetDecoded) && $tweetDecoded && !checkTweetAlreadyCashed($tweetDecoded['id']) ) {
            // var_dump(array_keys($tweetDecoded));
            $tweetsBulkArray[$i]['text'] = (array_key_exists('text', $tweetDecoded) ? $tweetDecoded['text'] : '');
            $tweetsBulkArray[$i]['id'] = (array_key_exists('id', $tweetDecoded) ? $tweetDecoded['id'] : 0);
            $tweetsBulkArray[$i]['id_str'] = (array_key_exists('id_str', $tweetDecoded) ? $tweetDecoded['id_str'] : '');

            $tweetsBulkArray[$i]['timestamp_ms'] = (array_key_exists('timestamp_ms', $tweetDecoded) ? $tweetDecoded['timestamp_ms'] : 0);
            $tweetsBulkArray[$i]['favorite_count'] = (array_key_exists('favorite_count', $tweetDecoded) ? $tweetDecoded['favorite_count'] : 0);
            $tweetsBulkArray[$i]['created_at'] = (array_key_exists('created_at', $tweetDecoded) ? $tweetDecoded['created_at'] : '');

            //var_dump($tweetDecoded['topsy']);

            if (array_key_exists('user', $tweetDecoded)) {
                $tweetsBulkArray[$i]['screen_name'] = (array_key_exists('screen_name', $tweetDecoded['user']) ? $tweetDecoded['user']['screen_name'] : '');
                $tweetsBulkArray[$i]['profile_location'] = (array_key_exists('location', $tweetDecoded['user']) ? $tweetDecoded['user']['location'] : '');
                $tweetsBulkArray[$i]['description'] = (array_key_exists('description', $tweetDecoded['user']) ? $tweetDecoded['user']['description'] : '');
                $tweetsBulkArray[$i]['followers_count'] = (array_key_exists('followers_count', $tweetDecoded['user']) ? $tweetDecoded['user']['followers_count'] : 0);
                $tweetsBulkArray[$i]['favourites_count'] = (array_key_exists('favourites_count', $tweetDecoded['user']) ? $tweetDecoded['user']['favourites_count'] : 0);
                $tweetsBulkArray[$i]['verified'] = (array_key_exists('verified', $tweetDecoded['user']) ? $tweetDecoded['user']['verified'] : false);
                $tweetsBulkArray[$i]['user_id'] = (array_key_exists('id', $tweetDecoded['user']) ? $tweetDecoded['user']['id'] : 0);
                $tweetsBulkArray[$i]['tweets_count'] = (array_key_exists('statuses_count', $tweetDecoded['user']) ? $tweetDecoded['user']['statuses_count'] : 0);
                $tweetsBulkArray[$i]['time_zone'] = (array_key_exists('time_zone', $tweetDecoded['user']) ? $tweetDecoded['user']['time_zone'] : 0);
                $tweetsBulkArray[$i]['listed_count'] = (array_key_exists('listed_count', $tweetDecoded['user']) ? $tweetDecoded['user']['listed_count'] : 0);
                $tweetsBulkArray[$i]['user_name'] = (array_key_exists('name', $tweetDecoded['user']) ? $tweetDecoded['user']['name'] : '');
                $tweetsBulkArray[$i]['user_created_at'] = (array_key_exists('created_at', $tweetDecoded['user']) ? $tweetDecoded['user']['created_at'] : '');
            }
            if (array_key_exists('entities', $tweetDecoded)) {
                if (array_key_exists('hashtags', $tweetDecoded['entities'])) {
                    if (is_array($tweetDecoded['entities']['hashtags'])) {
                        $j = 0;
                        $tweetsBulkArray[$i]['hashtags'] = '';
                        foreach ($tweetDecoded['entities']['hashtags'] as $keyHash => $valueHash) {
                            if (array_key_exists('text', $valueHash)) {
                                $tweetsBulkArray[$i]['hashtags'].= $valueHash['text'] . (($j == (sizeof($tweetDecoded['entities']['hashtags']) - 1)) ? "" : ',');
                                $tweetsBulkArray[$i]['hashtagsArr'][$j] = $valueHash['text'];
                                $j++;
                            }
                        }
                    }
                }
            }
            if (array_key_exists('topsy', $tweetDecoded)) {
                if (array_key_exists('original_tweet_location', $tweetDecoded['topsy'])) {
                    if (array_key_exists('tags', $tweetDecoded['topsy']['original_tweet_location'])) {
                        if (is_array($tweetDecoded['topsy']['original_tweet_location']['tags'])) {
                            $tweetsBulkArray[$i]['tweet_location_iso'] = (array_key_exists('iso2', $tweetDecoded['topsy']['original_tweet_location']['tags']) ? $tweetDecoded['topsy']['original_tweet_location']['tags']['iso2'] : '');
                            $tweetsBulkArray[$i]['tweet_location'] = (array_key_exists('name', $tweetDecoded['topsy']['original_tweet_location']['tags']) ? $tweetDecoded['topsy']['original_tweet_location']['tags']['name'] : '');
                        }
                    }
                    $tweetsBulkArray[$i]['terms'] = (array_key_exists('terms', $tweetDecoded['topsy']) ? implode(' , ', $tweetDecoded['topsy']['terms']) : '');
                    if (array_key_exists('metrics', $tweetDecoded['topsy'])) {
                        if (array_key_exists('citations', $tweetDecoded['topsy']['metrics'])) {

                            $tweetsBulkArray[$i]['citations'] = (array_key_exists('total', $tweetDecoded['topsy']['metrics']['citations']) ? $tweetDecoded['topsy']['metrics']['citations']['total'] : 0);
                        }
                    }
                }
            }
            //var_dump($tweetsBulkArray);
            //var_dump($tweetDecoded);
            //$GLOBALS['registry']->cache->save($tweetsBulkArray, $tweetsHead . $i);

            $i++;
        }
    }
    //var_dump($tweetsBulkArray);
    $oldTweetArray = $GLOBALS['registry']->cache->load($tweetsHead);
    if (is_array($oldTweetArray))
        $tweetsBulkArray = array_merge($tweetsBulkArray, $oldTweetArray);
    if(sizeof($tweetsBulkArray)>0)
        $GLOBALS['registry']->cache->save($tweetsBulkArray, $tweetsHead);
}
}
    $cashedTweets = $GLOBALS['registry']->cache->load($tweetsHead);

echo '<pre>';
print_r($cashedTweets);
exit;
