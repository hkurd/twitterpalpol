<?php
/**
 * configuration api, related with configuration business processes
 * @author Hussam El-Kurd <me@hussamkurd.com>
 * @copyright Copyright (c) 2011, Hussam El-Kurd 
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @package Tidy
 * @subpackage controller 
 */
class configurationApiController extends abstractConfigurationApi {

    /**
     * define validation action that require a validation
     * @access public
     * @var array
     */
    public $validateActions = array('addEditCurrency');

    /**
     * first method called before any thing executed
     * */
    public function ini() {
        $this->enableAPIOauth = true;
        parent::ini();
        $this->_configurationProcessesModel = new configurationProcessesModel();
    }

    /**
     * before action load called directly
     * */
    public function beforeLoad() {
        parent::beforeLoad();
    }

    /**
     * validate before AddEditCurrency 
     * */
    public function beforeAddEditCurrency() {
        $this->validateRules = array(
            // currency Valdation (add | edit)
            'Currency_Code' => array('' => _tr('please enter currency code')),
            'Currency_Name' => array('MaxChar=40' => _tr('please enter currency name within 40 char max'), '' => _tr('please enter currency name')),
            'CUEX_Value' => array('' => _tr('please enter exchange rate value'))
        );
    }

    /*
     * add curreny using post data , if Currency_ID exist it will be update
     * all params via post data
     * @return json array (messages , updatedID)
     * */

    public function addEditCurrency() {

        if ($this->validate) {
            if (sizeof($GLOBALS['registry']->data) > 0) {
                if ($this->_configurationProcessesModel->addEditCurrency(NULL, $GLOBALS['registry']->data)) {
                    ($GLOBALS['registry']->data['Currency_ID'] != NULL) ? _tr('Currency was successfully updated', SUCC_MSG) : _tr('Currency was successfully added', SUCC_MSG);
                } else {
                    _tr('Sorry ! Malfunction occurs when try to update currency , Please verify inputs', ERROR_MSG);
                }
            } else
                _tr('please verify inputs', ERROR_MSG);
        } else
            _tr('validation error , please verify inputs', ERROR_MSG);

        $msgs = @array_merge($GLOBALS['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS['registry']->msg->getAllMessages(ERROR_MSG));

        return $this->sendResponse(array('messages' => $msgs,
                    'updatedID' => $GLOBALS ['registry']->configDB->getInsertID()));
    }

    /**
     * delete a set of currencies by array of currencies id
     * all params via post data
     * @return json array (messages,opStatus) and operation status
     * */
    public function deleteCurrencyModelSet() {
        $error = false;
        if (sizeof($GLOBALS['registry']->data['currenciesModel']) > 0) {
            foreach ($GLOBALS['registry']->data['currenciesModel'] as $currencyModel)
                if (!$this->_configurationProcessesModel->deleteCurrency($currencyModel['Currency_ID']))
                    $error = true;

            if (!$error)
                _tr('Currency was successfully deleted', SUCC_MSG);
            else
                _tr('Sorry ! Malfunction occurs when try to delete currency , Please verify inputs', ERROR_MSG);
        }
        else {
            _tr('please verify inputs', ERROR_MSG);
            $error = true;
        }
        $msgs = @array_merge($GLOBALS['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS['registry']->msg->getAllMessages(ERROR_MSG)
        );

        return $this->sendResponse(array('messages' => $msgs, 'opStatus' => (!$error)));
    }

    /**
     * update currencies status set by array of currency id
     * all params via post data
     * @return json array (messages,opStatus) and operation status
     * */
    public function updateCurrenciesStatus() {
        $error = false;
        if (sizeof($GLOBALS['registry']->data['currenciesModel']) > 0) {
            foreach ($GLOBALS['registry']->data['currenciesModel'] as $currencyModel)
                if (!$this->_configurationProcessesModel->updateCurrecnyStatus($currencyModel['Currency_ID'], $GLOBALS['registry']->data['Currency_Status']))
                    $error = true;

            if (!$error)
                _tr('Currency status successfully updated', SUCC_MSG);
            else
                _tr('Sorry ! Malfunction occurs while try to update currency status , Please verify inputs', ERROR_MSG);
        }
        else {
            _tr('please verify inputs', ERROR_MSG);
            $error = true;
        }
        $msgs = @array_merge($GLOBALS['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS['registry']->msg->getAllMessages(ERROR_MSG)
        );

        return $this->sendResponse(array('messages' => $msgs, 'opStatus' => (!$error)));
    }

    /**
     * get definitions with languages , if @param type id is set it will return the record only , if not it will return definitions set
     * @param int $typeId
     * @return json array (record | records) contain  definitions set
     * */
    public function getDefinitions($typeId = NULL) {
        $typesModel = new typesModel();
        if ($typeId != NULL) {
            $typesModel = $typesModel->getTypesModel($typeId);
            $trTypesModel = $typesModel->modelTranslation->getTrTypesModel($typeId);
            $responseArray['typesModel'] = is_object($typesModel) ?
                    $typesModel->modelTranslation->getUpdateTypesModelWithLanguages($typesModel->getData()) : NULL;
        } else {
            $responseArray['typesModel'] = $typesModel->modelTranslation->getUpdateTypesModelsWithLanguages($typesModel->getTypesModels());
        }
        $responseArray['definitionClasses'] = $GLOBALS ['registry']->constantResources['typeClassesConstants'];

        return $this->sendResponse($responseArray);
    }

    /**
     * Validate Verification
     * @access public
     * @return
     */
    public function beforeAddEditDefinition() {
        $this->validateData = $GLOBALS ['registry']->data;

        $this->validateRules = array(
            'Type_Title' => array(
                '' => _tr('missing Title! , wrong in verification')
            ),
            'Type_Class' => array(
                '' => _tr('missing Class! , wrong in verification')
            ),
            'Type_Description' => array(
                '' => _tr('missing Description! , wrong in verification')
            )
        );
    }

    /**
     * add definition using post data , if Type_ID exist it will be update
     * all params via post data
     * @return json array (messages , updatedID)
     * */
    public function addEditDefinition() {
        // $GLOBALS['registry']->configDB->setDebug(\TIDY_CONSTANTS::DEBUG_FULL_MODE);
        if ($GLOBALS['registry']->data['Type_Class'] != '' and $GLOBALS['registry']->data['Type_Title'] != '' and $GLOBALS['registry']->data['Type_Description'] != '') {
            if (sizeof($GLOBALS['registry']->data) > 0) {
                if ($this->_configurationProcessesModel->addEditType($GLOBALS['registry']->data)) {
                    ($GLOBALS['registry']->data['Type_ID'] != NULL) ? _tr('Definition was successfully updated', SUCC_MSG) : _tr('Definition was successfully added', SUCC_MSG);
                } else {
                    _tr('Sorry ! Malfunction occurs while try to update  definition , Please verify inputs', ERROR_MSG);
                }
            } else
                _tr('please verify inputs', ERROR_MSG);
        } else
            _tr('validation error , please verify inputs', ERROR_MSG);
        $msgs = @array_merge($GLOBALS['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS['registry']->msg->getAllMessages(ERROR_MSG));
        return $this->sendResponse(array('messages' => $msgs,
                    'updatedID' => ($GLOBALS['registry']->data['Type_ID'] != NULL) ? $GLOBALS['registry']->data['Type_ID'] : $GLOBALS ['registry']->configDB->getInsertID()));
    }

    /**
     * delete a set of difinitions by array of type id
     * all params via post data
     * @return json array (messages ,opStatus) contains the operation status
     * */
    public function deleteDefinationsModelSet() {

        $error = false;

        if (sizeof($GLOBALS['registry']->data['definitionsModel']) > 0) {
            @$GLOBALS['registry']->configDB->beginTR();
            foreach ($GLOBALS['registry']->data['definitionsModel'] as $definationModel)
                if (!$this->_configurationProcessesModel->deleteType($definationModel['Type_ID']))
                    $error = true;

            if (!$error) {
                _tr('Definition was successfully deleted', SUCC_MSG);
                @$GLOBALS['registry']->configDB->endTR();
            } else {
                _tr('Sorry ! Malfunction occurs while try to delete definition , Please verify inputs', ERROR_MSG);
                @$GLOBALS['registry']->configDB->rollBack();
                $error = true;
            }
        } else {
            _tr('please verify inputs', ERROR_MSG);
            $error = true;
        }
        $msgs = @array_merge($GLOBALS['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS['registry']->msg->getAllMessages(ERROR_MSG)
        );

        return $this->sendResponse(array('messages' => $msgs, 'opStatus' => (!$error)));
    }

    /**
     * delete country by countryid
     * @param int Country id
     * @return json array (messages,opStatus) and contains the operation status
     * */
    public function deleteCountry($countryID) {
        $error = false;
        if ($countryID != '') {
            if ($this->_configurationProcessesModel->deleteCountry($countryID)) {
                _tr('country was successfully deleted', SUCC_MSG);
            } else {
                _tr('Sorry ! Malfunction occurs while try to delete country , Please verify inputs', ERROR_MSG);
                $error = true;
            }
        } else {
            _tr('the requested country is not exist , please verify inputs', INTER_ERROR_MSG);
            $error = true;
        }
        $msgs = @array_merge($GLOBALS ['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS ['registry']->msg->getAllMessages(ERROR_MSG));

        return $this->sendResponse(array('messages' => $msgs, 'opStatus' => (!$error)));
    }

    /**
     * delete a set of countries by array of country id
     * all params via post data
     * @return json array (messages,opStatus) and contains the operation status
     * */
    public function deleteCountriesModelSet() {

        $error = false;

        if (sizeof($GLOBALS['registry']->data['countriesModel']) > 0) {
            @$GLOBALS['registry']->configDB->beginTR();
            foreach ($GLOBALS['registry']->data['countriesModel'] as $countriesModel)
                if (!$this->_configurationProcessesModel->deleteCountry($countriesModel['country_ID']))
                    $error = true;

            if (!$error) {
                _tr('country was successfully deleted', SUCC_MSG);
                @$GLOBALS['registry']->configDB->endTR();
            } else {
                _tr('Sorry ! Malfunction occurs while try to delete country , Please verify inputs', ERROR_MSG);
                $error = true;
                //	@$GLOBALS['registry']->configDB->rollBack();
            }
        } else {
            _tr('there is no selected countries  , please verify inputs', ERROR_MSG);
            $error = true;
        }
        $msgs = @array_merge($GLOBALS['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS['registry']->msg->getAllMessages(ERROR_MSG)
        );
        return $this->sendResponse(array('messages' => $msgs, 'opStatus' => (!$error)));
    }

    /**
     * delete city by city_id
     * @return json array (messages,opStatus) and contains the operation status 
     * */
    public function deleteCity($cityID) {
        $error = false;
        if ($cityID != '') {
            if ($this->_configurationProcessesModel->deleteCity($cityID)) {
                _tr('city was successfully deleted', SUCC_MSG);
            } else {
                _tr('Sorry ! Malfunction occurs while try to delete city , Please verify inputs', ERROR_MSG);
                $error = true;
            }
        } else {
            _tr('the requested city is not exist , please verify inputs', INTER_ERROR_MSG);
            $error = true;
        }
        $msgs = @array_merge($GLOBALS ['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS ['registry']->msg->getAllMessages(ERROR_MSG));
        return $this->sendResponse(array(
                    'messages' => $msgs, 'opStatus' => (!$error)
        ));
    }

    /**
     * get countries with languages , if @param country id is set it will return the record only , if not it will return countries set
     * @param int $countryID
     * @return json array (record | records) contains countries set
     * */
    public function getCountries($countryID = null) {
        $countriesModel = new countriesModel();
        $citiesModel = new citiesModel();
        if ($countryID == NULL) {
            $countries = $countriesModel->modelTranslation->getUpdateCountriesModelsWithLanguages($countriesModel->getCountriesModelsView());
            $responseArray['countriesModel'] = $citiesModel->modelTranslation->getUpdateCountryCitiesModelsWithLanguages($countries);
        } else {
            $countriesModel = $countriesModel->getCountriesModel($countryID);
            if (is_object($countriesModel)) {
                $trCurrenciesModel = $countriesModel->modelTranslation->getTrCountriesModel($countryID);
                $responseArray['countriesModel'] = is_object($countriesModel) ?
                        $countriesModel->modelTranslation->getUpdateCountriesModelWithLanguages($countriesModel->getData()) : NULL;
            } else
                _tr('the requested city is not exist , please verify inputs', ERROR_MSG);
        }
        $msgs = @array_merge($GLOBALS ['registry']->msg->getAllMessages(SUCC_MSG), $GLOBALS ['registry']->msg->getAllMessages(ERROR_MSG));
        $responseArray['messages'] = $msgs;
        return $this->sendResponse($responseArray);
    }

}
